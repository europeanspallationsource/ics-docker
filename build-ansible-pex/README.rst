build-ansible-pex
=================

Script to build an ansible-playbook PEX_ file.

PEX_ files are self-contained executable Python virtual environments
that make deployment very easy.

The script uses a CentOS 7.1 Docker_ image to build a pex file compatible with linux-x86_64
and Python 2.7.

::

    $ ./build-ansible-pex.sh -h
    Usage : build-ansible-pex.sh [-d output_dir] [-v ansible_version]

        Create a self-contained ansible-playbook file that can easily be installed.

        Options:

          -h                  : show this help and exit

          -d output_dir       : directory where the ansible-playbook PEX file will be created [default: .]

          -v ansible_version  : ansible version to build [default: 2.2.1.0]

        Examples:

          # Build the default ansible version
          $ ./build-ansible-pex.sh

          # Build ansible 1.9.4 in the output directory
          $ mkdir output; ./build-ansible-pex.sh -d output -v 1.9.4


.. _PEX: https://pex.readthedocs.io/en/stable/index.html

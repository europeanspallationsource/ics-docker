#!/bin/bash

# Default values
output_dir=$(pwd)
version="2.3.0.0"

short_usage() {
  cat <<EOF
Usage : build-ansible-pex.sh [-d output_dir] [-v ansible_version]

EOF
}


usage() {
  short_usage
  cat <<EOF
    Create self-contained ansible-playbook and ansible-galaxy PEX files that can easily be deployed.

    Options:

      -h                  : show this help and exit

      -d output_dir       : directory where the PEX files will be created [default: .]

      -v ansible_version  : ansible version to build [default: ${version}]

    Examples:

      # Build the default ansible version
      $ ./build-ansible-pex.sh

      # Build ansible 1.9.4 in the output directory
      $ mkdir output; ./build-ansible-pex.sh -d output -v 1.9.4
EOF
}


while getopts :hd:v: arg
do
  case $arg in
  (h)
    usage; exit;;
  (d)
    output_dir=$OPTARG;;
  (v)
    version=$OPTARG;;
  (?)
    echo "Invalid argument -$OPTARG .";
    short_usage; exit 2;;
  esac
done

[ ! -d "${output_dir}" ] && { echo "${output_dir} not found. Abort."; exit 1; }

# Get output_dir full path
output_dir=$(cd ${output_dir} && pwd -P)

# Refresh pex image
docker pull europeanspallationsource/pex:latest

for exe in ansible-playbook ansible-galaxy
do
  docker run --rm -v ${output_dir}:/build europeanspallationsource/pex:latest ansible==${version} -c ${exe} -o ${exe}
  if [[ "$?" == "0" ]]
  then
    echo "${output_dir}/${exe} PEX file (version ${version}) created successfully"
  fi
done

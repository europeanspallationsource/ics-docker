#!/bin/bash
JBOSS_HOME=/opt/jboss

$JBOSS_HOME/bin/standalone.sh &

sleep 20

echo "started jboss"

$JBOSS_HOME/bin/jboss-cli.sh --connect --command="module add --name=org.postgresql --resources=/opt/jboss/standalone/deployments/postgresql-${POSTGRESQL_VERSION}.jar --dependencies=javax.api,javax.transaction.api"

echo "set path"

$JBOSS_HOME/bin/jboss-cli.sh --connect --command="/subsystem=datasources/jdbc-driver=postgresql:add(driver-name=postgresql,driver-module-name=org.postgresql,driver-xa-datasource-class"

echo "added datasource"

rm -rf $JBOSS_HOME/standalone/configuration/standalone_xml_history/ $JBOSS_HOME/standalone/log/*

$JBOSS_HOME/bin/jboss-cli.sh --connect --command="shutdown"

echo "turned off jboss"


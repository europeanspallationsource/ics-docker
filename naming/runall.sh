#!/bin/bash
docker run --name postgresql -e POSTGRES_DB=discs_names -e POSTGRES_USER=discs_names -e POSTGRES_PASSWORD=discs_names -d psgdb
#docker exec -it postgresql psql –U cabledb cabledb < /home/hakanhagenrud/Documents/ics_proj/ics-docker/cabledb/cabledb.sql
docker run --name naming --link postgresql -d naming
#docker run --name cable --link postgresql -P cable
#docker run --name proxy --link naming -P -d proxy
docker run --name proxy --link naming -p 9001:80 -d proxy
echo "firefox http://localhost:9001"

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: artifact; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE artifact (
    id bigint NOT NULL,
    description character varying(255),
    modified_at timestamp without time zone,
    modified_by character varying(255),
    name character varying(255),
    uri text,
    rooturi text,
    modifiedat timestamp without time zone,
    modifiedby character varying(255)
);


ALTER TABLE public.artifact OWNER TO cabledb_dev;

--
-- Name: artifact_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE artifact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.artifact_id_seq OWNER TO cabledb_dev;

--
-- Name: artifact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE artifact_id_seq OWNED BY artifact.id;


--
-- Name: cable; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE cable (
    id bigint NOT NULL,
    cableclass character varying(255),
    created timestamp without time zone,
    installationby timestamp without time zone,
    modified timestamp without time zone,
    owner character varying(255),
    routing character varying(255),
    seqnumber integer,
    status character varying(255),
    subsystem character varying(255),
    system character varying(255),
    terminationby timestamp without time zone,
    validity character varying(255),
    cabletype_id bigint,
    endpointa_id bigint,
    endpointb_id bigint,
    length real,
    container character varying(255),
    qualityreport_id bigint
);


ALTER TABLE public.cable OWNER TO cabledb_dev;

--
-- Name: cable_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE cable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cable_id_seq OWNER TO cabledb_dev;

--
-- Name: cable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE cable_id_seq OWNED BY cable.id;


--
-- Name: cabletype; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE cabletype (
    id bigint NOT NULL,
    active boolean NOT NULL,
    description character varying(65545),
    comments character varying(255),
    flammability character varying(255),
    insulation character varying(255),
    jacket character varying(255),
    model character varying(255),
    name character varying(255),
    installationtype character varying(255),
    service character varying(255),
    tid real,
    weight real,
    diameter real,
    voltage real,
    manufacturer_id bigint,
    datasheet_id bigint
);


ALTER TABLE public.cabletype OWNER TO cabledb_dev;

--
-- Name: cabletype_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE cabletype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cabletype_id_seq OWNER TO cabledb_dev;

--
-- Name: cabletype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE cabletype_id_seq OWNED BY cabletype.id;


--
-- Name: connector_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE connector_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.connector_id_seq OWNER TO cabledb_dev;

--
-- Name: connector; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE connector (
    id bigint DEFAULT nextval('connector_id_seq'::regclass) NOT NULL,
    description character varying(255),
    created timestamp without time zone,
    connectortype character varying(255),
    modified timestamp without time zone,
    name character varying(255) NOT NULL,
    drawing character varying(255),
    active boolean NOT NULL
);


ALTER TABLE public.connector OWNER TO cabledb_dev;

--
-- Name: displayview; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE displayview (
    id bigint NOT NULL,
    created timestamp without time zone,
    description character varying(255),
    entitytype character varying(255),
    executed timestamp without time zone,
    owner character varying(255)
);


ALTER TABLE public.displayview OWNER TO cabledb_dev;

--
-- Name: displayview_displayviewcolumn; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE displayview_displayviewcolumn (
    displayview_id bigint NOT NULL,
    columns_id bigint NOT NULL
);


ALTER TABLE public.displayview_displayviewcolumn OWNER TO cabledb_dev;

--
-- Name: displayview_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE displayview_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.displayview_id_seq OWNER TO cabledb_dev;

--
-- Name: displayview_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE displayview_id_seq OWNED BY displayview.id;


--
-- Name: displayviewcolumn; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE displayviewcolumn (
    id bigint NOT NULL,
    columnname character varying(255),
    "position" integer,
    displayview bigint
);


ALTER TABLE public.displayviewcolumn OWNER TO cabledb_dev;

--
-- Name: displayviewcolumn_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE displayviewcolumn_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.displayviewcolumn_id_seq OWNER TO cabledb_dev;

--
-- Name: displayviewcolumn_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE displayviewcolumn_id_seq OWNED BY displayviewcolumn.id;


--
-- Name: endpoint; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE endpoint (
    id bigint NOT NULL,
    building character varying(255),
    device character varying(255),
    drawing character varying(255),
    label character varying(255),
    rack character varying(255),
    validity character varying(255),
    connector_id bigint
);


ALTER TABLE public.endpoint OWNER TO cabledb_dev;

--
-- Name: endpoint_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE endpoint_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.endpoint_id_seq OWNER TO cabledb_dev;

--
-- Name: endpoint_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE endpoint_id_seq OWNED BY endpoint.id;


--
-- Name: history; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE history (
    id bigint NOT NULL,
    entityid bigint,
    entitytype character varying(255),
    entry character varying(65535),
    operation character varying(255),
    "timestamp" timestamp without time zone,
    userid character varying(255),
    entityname character varying(255)
);


ALTER TABLE public.history OWNER TO cabledb_dev;

--
-- Name: history_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.history_id_seq OWNER TO cabledb_dev;

--
-- Name: history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE history_id_seq OWNED BY history.id;


--
-- Name: manufacturer_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE manufacturer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.manufacturer_id_seq OWNER TO cabledb_dev;

--
-- Name: manufacturer; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE manufacturer (
    id bigint DEFAULT nextval('manufacturer_id_seq'::regclass) NOT NULL,
    address character varying(255),
    country character varying(255),
    created timestamp without time zone,
    email character varying(255),
    modified timestamp without time zone,
    name character varying(255),
    phonenumber character varying(255),
    status character varying(255)
);


ALTER TABLE public.manufacturer OWNER TO cabledb_dev;

--
-- Name: query; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE query (
    id bigint NOT NULL,
    created timestamp without time zone,
    description character varying(255),
    executed timestamp without time zone,
    owner character varying(255),
    entitytype character varying(255)
);


ALTER TABLE public.query OWNER TO cabledb_dev;

--
-- Name: query_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE query_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.query_id_seq OWNER TO cabledb_dev;

--
-- Name: query_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE query_id_seq OWNED BY query.id;


--
-- Name: query_querycondition; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE query_querycondition (
    query_id bigint NOT NULL,
    conditions_id bigint NOT NULL
);


ALTER TABLE public.query_querycondition OWNER TO cabledb_dev;

--
-- Name: querycondition; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE querycondition (
    id bigint NOT NULL,
    booleanoperator character varying(255),
    comparisonoperator character varying(255),
    field character varying(255),
    parenthesisclose character varying(255),
    parenthesisopen character varying(255),
    "position" integer,
    value character varying(255),
    query bigint
);


ALTER TABLE public.querycondition OWNER TO cabledb_dev;

--
-- Name: querycondition_id_seq; Type: SEQUENCE; Schema: public; Owner: cabledb_dev
--

CREATE SEQUENCE querycondition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.querycondition_id_seq OWNER TO cabledb_dev;

--
-- Name: querycondition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cabledb_dev
--

ALTER SEQUENCE querycondition_id_seq OWNED BY querycondition.id;


--
-- Name: schema_version; Type: TABLE; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE TABLE schema_version (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.schema_version OWNER TO cabledb_dev;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY artifact ALTER COLUMN id SET DEFAULT nextval('artifact_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY cable ALTER COLUMN id SET DEFAULT nextval('cable_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY cabletype ALTER COLUMN id SET DEFAULT nextval('cabletype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY displayview ALTER COLUMN id SET DEFAULT nextval('displayview_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY displayviewcolumn ALTER COLUMN id SET DEFAULT nextval('displayviewcolumn_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY endpoint ALTER COLUMN id SET DEFAULT nextval('endpoint_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY history ALTER COLUMN id SET DEFAULT nextval('history_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY query ALTER COLUMN id SET DEFAULT nextval('query_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY querycondition ALTER COLUMN id SET DEFAULT nextval('querycondition_id_seq'::regclass);


--
-- Data for Name: artifact; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY artifact (id, description, modified_at, modified_by, name, uri, rooturi, modifiedat, modifiedby) FROM stdin;
1	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
2	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
3	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
4	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
5	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
6	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
7	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
8	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
9	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
10	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
11	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
12	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
13	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
14	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
15	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
16	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
17	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
18	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
19	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
20	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
21	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
22	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
23	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
205	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
206	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
207	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
208	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
209	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
210	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
211	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
212	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
213	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
214	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
215	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
216	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
217	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
218	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
219	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
220	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
221	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
222	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
223	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
224	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
225	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
226	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
46	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
47	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
48	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
49	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
50	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
51	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
52	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
53	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
54	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
55	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
56	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
57	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
58	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
59	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
60	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
61	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
62	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
63	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
64	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
65	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
66	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
67	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
68	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
69	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
70	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
71	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
72	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
73	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
74	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
75	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
76	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
77	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
78	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
79	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
80	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
81	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
82	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
83	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
84	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
85	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
86	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
87	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
88	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
89	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
90	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
91	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
92	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
93	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
94	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
95	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
96	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
97	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
98	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
99	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
100	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
101	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
102	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
103	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
104	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
105	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
106	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
107	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
108	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
109	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
110	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
111	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
112	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
113	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
114	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
115	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
116	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
117	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
118	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
119	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
120	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
121	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
122	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
123	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
124	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
125	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
227	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
228	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
229	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
230	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
231	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
232	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
233	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
234	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
235	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
236	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
237	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
238	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
239	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
240	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
241	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
242	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
243	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
244	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
245	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
246	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
247	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
248	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
249	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
250	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
251	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
252	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
253	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
254	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
255	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
256	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
257	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
258	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
259	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
260	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
261	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
262	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
263	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
264	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
265	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
266	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
267	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
268	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
269	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
270	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
271	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
272	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
273	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
274	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
275	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
276	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
277	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
278	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
279	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
280	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
281	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
282	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
283	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
859	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
860	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
861	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
862	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
863	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
864	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
865	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
866	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
867	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
868	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
869	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
870	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
871	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
872	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
873	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
874	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
875	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
876	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
877	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
878	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
879	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
880	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
881	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
882	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
883	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
884	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
885	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
886	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
887	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
888	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
889	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
890	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
891	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
892	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
893	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
894	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
895	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
896	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
897	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
898	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
899	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
900	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
901	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
902	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
903	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
904	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
905	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
906	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
907	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
908	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
909	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
910	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
911	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
912	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
913	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
484	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
485	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
486	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
487	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
488	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
489	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
490	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
491	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
492	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
493	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
494	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
495	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
496	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
497	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
498	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
499	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
500	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
501	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
502	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
503	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
504	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
505	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
506	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
507	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
508	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
509	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
510	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
511	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
512	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
513	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
514	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
515	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
516	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
517	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
518	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
519	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
520	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
521	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
522	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
523	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
524	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
525	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
526	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
527	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
528	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
529	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
530	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
531	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
532	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
533	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
534	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
535	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
536	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
537	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
538	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
539	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
540	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
541	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
542	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
543	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
544	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
545	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
546	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
547	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
548	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
549	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
550	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
551	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
552	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
553	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
554	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
555	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
556	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
557	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
558	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
559	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
560	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
561	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
562	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
711	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
712	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
713	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
714	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
715	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
716	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
717	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
718	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
719	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
720	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
721	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
722	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
723	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
724	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
725	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
726	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
727	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
728	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
729	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
730	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
731	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
732	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
733	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
734	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
735	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
736	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
737	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
738	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
739	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
740	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
741	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
742	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
743	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
744	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
745	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
746	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
747	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
748	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
749	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
750	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
751	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
752	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
753	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
754	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
755	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
756	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
757	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
758	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
759	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
760	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
761	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
762	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
763	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
764	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
765	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
766	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
767	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
768	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
769	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
770	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
771	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
772	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
773	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
774	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
775	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
776	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
777	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
778	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
779	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
780	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
781	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
782	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
783	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
784	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
785	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
786	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
787	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
788	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
789	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
914	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
915	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
916	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
917	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
918	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
919	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
920	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
921	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
922	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
923	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
924	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
925	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
926	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
927	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
928	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
929	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
930	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
931	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
932	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
933	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
934	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
935	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
936	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
937	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
938	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
939	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
940	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
941	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
942	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
943	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
944	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
945	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
946	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
947	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
948	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
949	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
950	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
951	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
952	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
953	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
954	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
955	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
956	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
957	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
958	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
959	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
960	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
961	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
962	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
963	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
964	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
965	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
966	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
967	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
968	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
969	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
970	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
971	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
972	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
973	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
974	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
975	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
976	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
977	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
978	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
979	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
980	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
981	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
982	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
983	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
984	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
985	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
986	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
987	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
988	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
989	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
990	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
991	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
992	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
993	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
994	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
995	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
996	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
997	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
998	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
999	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1000	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1001	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1002	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1003	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1004	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1005	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1006	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1007	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1008	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1009	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1010	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1011	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1012	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1013	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1014	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1015	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1016	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1017	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1018	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1019	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1020	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1021	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1022	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1023	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1024	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1025	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1026	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1027	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1028	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1029	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1030	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1031	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1032	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1033	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1034	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1035	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1036	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1037	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1038	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1039	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1040	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1041	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1042	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1043	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1044	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1045	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1046	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1047	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1048	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1049	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1050	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1051	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1052	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1053	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1054	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1055	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1056	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1057	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1058	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1059	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1060	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1061	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1062	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1063	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1064	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1065	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1066	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1067	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1068	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1069	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1070	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1071	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1072	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1073	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1074	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1075	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1076	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1077	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1078	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1079	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1080	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1081	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1082	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1083	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1084	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1085	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1086	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1087	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1088	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1089	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1090	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1091	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1092	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1093	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1094	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1095	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1096	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1097	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1098	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1099	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1100	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1101	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1102	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1103	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1104	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1105	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1106	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1107	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1108	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1109	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1110	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1111	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1112	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1113	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1114	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1115	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1116	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1117	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1118	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1119	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1120	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1121	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1122	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1123	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1124	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1125	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1126	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1127	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1128	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1129	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1130	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1131	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1132	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1133	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1134	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1135	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1136	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1137	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1138	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1139	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1140	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1141	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1142	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1143	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1144	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1145	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1146	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1147	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1148	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1149	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1150	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1151	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1152	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1153	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1154	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1155	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1156	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1157	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1158	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1159	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1160	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1161	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1162	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1163	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1164	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1165	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1166	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1167	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1168	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1169	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1170	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1171	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1172	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1173	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1174	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1175	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1176	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1177	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1178	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1179	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1180	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1181	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1182	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1183	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1184	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1185	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1186	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1187	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1188	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1189	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1190	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1191	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1192	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1193	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1194	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1195	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1196	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1197	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1198	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1199	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1200	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1201	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1202	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1203	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1204	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1205	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1206	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1207	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1208	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1209	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1210	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1211	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1212	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1213	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1214	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1215	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1216	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1217	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1218	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1219	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1220	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1221	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1222	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1223	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1224	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1225	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1226	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1227	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1228	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1229	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1230	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1231	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1232	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1233	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1234	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1235	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1236	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1237	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1238	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1239	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1240	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1241	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1242	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1243	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1244	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1245	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1246	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1247	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1248	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1249	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1250	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1251	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1252	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1253	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1254	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1274	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1275	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1276	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1277	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1278	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1279	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1280	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1281	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1282	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1283	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1284	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1285	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1286	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1287	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1288	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1289	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1290	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1291	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1292	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1293	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1294	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1295	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1296	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1297	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1298	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1299	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1300	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1301	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1302	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1303	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1304	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1305	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1306	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1307	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1308	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1309	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1310	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1311	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1312	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1313	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1314	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1315	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1316	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1317	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1318	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1319	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1320	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1321	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1322	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1323	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1324	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1325	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1326	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1327	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1328	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1329	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1330	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1331	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1332	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1333	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1334	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1335	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1336	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1337	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1338	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1339	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1340	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1341	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
1342	\N	\N	\N	\N	\N	\N	1970-01-01 01:00:00	\N
\.


--
-- Name: artifact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('artifact_id_seq', 1342, true);


--
-- Data for Name: cable; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY cable (id, cableclass, created, installationby, modified, owner, routing, seqnumber, status, subsystem, system, terminationby, validity, cabletype_id, endpointa_id, endpointb_id, length, container, qualityreport_id) FROM stdin;
5	A	2016-08-26 10:03:29.971	\N	2016-08-26 10:04:41.087	evangeliavaena		1	DELETED	0	3	\N	VALID	225	9	10	\N	\N	\N
6	A	2016-08-26 10:13:38.931	\N	2016-08-26 10:13:47.686	evangeliavaena		2	DELETED	0	1	\N	VALID	\N	11	12	\N	\N	\N
7	B	2016-09-05 16:28:16.616	\N	2016-09-05 16:30:15.603	francoisbellorini		3	DELETED	3	8	\N	VALID	312	13	14	10	\N	\N
9	A	2016-09-29 15:20:27.719	\N	2016-10-10 11:13:26.911	johannorin		5	APPROVED	2	3	\N	DANGLING	223	17	18	\N		1274
8	B	2016-09-07 14:50:43.003	\N	2016-09-07 15:43:26.845	johannorin		4	APPROVED	2	2	\N	DANGLING	\N	15	16	\N	\N	\N
10	A	2016-09-29 15:20:27.743	\N	2016-10-10 11:13:26.968	johannorin		6	APPROVED	2	3	\N	DANGLING	223	19	20	\N		1275
11	A	2016-09-29 15:20:27.751	\N	2016-10-10 11:13:27.032	johannorin		7	APPROVED	2	3	\N	DANGLING	223	21	22	\N		1276
12	A	2016-09-29 15:20:27.757	\N	2016-10-10 11:13:27.077	johannorin		8	APPROVED	2	3	\N	DANGLING	223	23	24	\N		1277
13	A	2016-09-29 15:20:27.767	\N	2016-10-10 11:13:27.128	johannorin		9	APPROVED	2	3	\N	DANGLING	223	25	26	\N		1278
14	A	2016-09-29 15:20:27.774	\N	2016-10-10 11:13:27.167	johannorin		10	APPROVED	2	3	\N	DANGLING	223	27	28	\N		1279
15	A	2016-09-29 15:20:27.782	\N	2016-10-10 11:13:27.197	johannorin		11	APPROVED	2	3	\N	DANGLING	223	29	30	\N		1280
16	A	2016-09-29 15:20:27.788	\N	2016-10-10 11:13:27.228	johannorin		12	APPROVED	2	3	\N	DANGLING	223	31	32	\N		1281
18	A	2016-09-29 15:20:27.801	\N	2016-10-10 11:13:27.259	johannorin		14	APPROVED	2	3	\N	DANGLING	223	35	36	\N		1282
19	A	2016-09-29 15:20:27.808	\N	2016-10-10 11:13:27.299	johannorin		15	APPROVED	2	3	\N	DANGLING	223	37	38	\N		1283
20	A	2016-09-29 15:20:27.814	\N	2016-10-10 11:13:27.329	johannorin		16	APPROVED	2	3	\N	DANGLING	223	39	40	\N		1284
21	A	2016-09-29 15:20:27.821	\N	2016-10-10 11:13:27.358	johannorin		17	APPROVED	2	3	\N	DANGLING	223	41	42	\N		1285
22	A	2016-09-29 15:20:27.828	\N	2016-10-10 11:13:27.401	johannorin		18	APPROVED	2	3	\N	DANGLING	223	43	44	\N		1286
23	A	2016-09-29 15:20:27.834	\N	2016-10-10 11:13:27.432	johannorin		19	APPROVED	2	3	\N	DANGLING	223	45	46	\N		1287
24	A	2016-09-29 15:20:27.842	\N	2016-10-10 11:13:27.463	johannorin		20	APPROVED	2	3	\N	DANGLING	223	47	48	\N		1288
25	A	2016-09-29 15:20:27.851	\N	2016-10-10 11:13:27.497	johannorin		21	APPROVED	2	3	\N	DANGLING	223	49	50	\N		1289
27	A	2016-09-29 15:20:27.865	\N	2016-10-10 11:13:27.529	johannorin		23	APPROVED	2	3	\N	DANGLING	223	53	54	\N		1290
28	A	2016-09-29 15:20:27.872	\N	2016-10-10 11:13:27.561	johannorin		24	APPROVED	2	3	\N	DANGLING	223	55	56	\N		1291
29	A	2016-09-29 15:20:27.882	\N	2016-10-10 11:13:27.594	johannorin		25	APPROVED	2	3	\N	DANGLING	223	57	58	\N		1292
30	A	2016-09-29 15:20:27.891	\N	2016-10-10 11:13:27.624	johannorin		26	APPROVED	2	3	\N	DANGLING	223	59	60	\N		1293
462	A	2016-10-10 15:15:29.236	\N	2016-10-10 15:15:29.236	johannorin		458	APPROVED	2	4	\N	VALID	223	923	924	\N		1294
463	A	2016-10-10 15:15:29.246	\N	2016-10-10 15:15:29.246	johannorin		459	APPROVED	2	4	\N	VALID	223	925	926	\N		1295
464	A	2016-10-10 15:15:29.253	\N	2016-10-10 15:15:29.253	johannorin		460	APPROVED	2	4	\N	VALID	223	927	928	\N		1296
465	A	2016-10-10 15:15:29.259	\N	2016-10-10 15:15:29.259	johannorin		461	APPROVED	2	4	\N	VALID	223	929	930	\N		1297
466	A	2016-10-10 15:15:29.266	\N	2016-10-10 15:15:29.266	johannorin		462	APPROVED	2	4	\N	VALID	223	931	932	\N		1298
78	E	2016-10-04 15:39:40	\N	2016-10-05 18:11:38.541	johannorin		74	APPROVED	2	3	\N	DANGLING	234	155	156	\N		780
79	D	2016-10-04 15:39:40.008	\N	2016-10-05 18:11:38.575	johannorin		75	APPROVED	2	3	\N	DANGLING	324	157	158	\N		781
467	A	2016-10-10 15:15:29.272	\N	2016-10-10 15:15:29.272	johannorin		463	APPROVED	2	4	\N	VALID	223	933	934	\N		1299
468	A	2016-10-10 15:15:29.28	\N	2016-10-10 15:15:29.28	johannorin		464	APPROVED	2	4	\N	VALID	223	935	936	\N		1300
469	A	2016-10-10 15:15:29.287	\N	2016-10-10 15:15:29.287	johannorin		465	APPROVED	2	4	\N	VALID	223	937	938	\N		1301
470	A	2016-10-10 15:15:29.294	\N	2016-10-10 15:15:29.294	johannorin		466	APPROVED	2	4	\N	VALID	223	939	940	\N		1302
471	A	2016-10-10 15:15:29.301	\N	2016-10-10 15:15:29.301	johannorin		467	APPROVED	2	4	\N	VALID	223	941	942	\N		1303
472	A	2016-10-10 15:15:29.311	\N	2016-10-10 15:15:29.311	johannorin		468	APPROVED	2	4	\N	VALID	223	943	944	\N		1304
473	A	2016-10-10 15:15:29.323	\N	2016-10-10 15:15:29.323	johannorin		469	APPROVED	2	4	\N	VALID	223	945	946	\N		1305
474	A	2016-10-10 15:15:29.33	\N	2016-10-10 15:15:29.33	johannorin		470	APPROVED	2	4	\N	VALID	223	947	948	\N		1306
475	A	2016-10-10 15:15:29.338	\N	2016-10-10 15:15:29.338	johannorin		471	APPROVED	2	4	\N	VALID	223	949	950	\N		1307
476	A	2016-10-10 15:15:29.345	\N	2016-10-10 15:15:29.345	johannorin		472	APPROVED	2	4	\N	VALID	223	951	952	\N		1308
477	A	2016-10-10 15:15:29.356	\N	2016-10-10 15:15:29.356	johannorin		473	APPROVED	2	4	\N	VALID	223	953	954	\N		1309
478	A	2016-10-10 15:15:29.369	\N	2016-10-10 15:15:29.369	johannorin		474	APPROVED	2	4	\N	VALID	223	955	956	\N		1310
479	A	2016-10-10 15:15:29.379	\N	2016-10-10 15:15:29.379	johannorin		475	APPROVED	2	4	\N	VALID	223	957	958	\N		1311
480	A	2016-10-10 15:15:29.389	\N	2016-10-10 15:15:29.389	johannorin		476	APPROVED	2	4	\N	VALID	223	959	960	\N		1312
481	A	2016-10-10 15:15:29.4	\N	2016-10-10 15:15:29.4	johannorin		477	APPROVED	2	4	\N	VALID	223	961	962	\N		1313
482	A	2016-10-10 15:15:29.409	\N	2016-10-10 15:15:29.409	johannorin		478	APPROVED	2	4	\N	VALID	223	963	964	\N		1314
483	A	2016-10-10 15:15:29.417	\N	2016-10-10 15:15:29.417	johannorin		479	APPROVED	2	4	\N	VALID	223	965	966	\N		1315
484	A	2016-10-10 15:15:29.426	\N	2016-10-10 15:15:29.426	johannorin		480	APPROVED	2	4	\N	VALID	223	967	968	\N		1316
17	A	2016-09-29 15:20:27.794	\N	2016-10-10 11:04:20.759	johannorin		13	APPROVED	2	3	\N	DANGLING	223	33	34	\N		1222
26	A	2016-09-29 15:20:27.858	\N	2016-10-10 11:04:20.822	johannorin		22	APPROVED	2	3	\N	DANGLING	223	51	52	\N		1223
31	A	2016-09-29 15:20:27.9	\N	2016-10-10 11:04:20.886	johannorin		27	APPROVED	2	3	\N	DANGLING	223	61	62	\N		1224
81	G	2016-10-04 15:39:40.025	\N	2016-10-05 18:11:38.597	johannorin		77	APPROVED	2	3	\N	DANGLING	\N	161	162	\N		783
82	B	2016-10-04 15:39:40.033	\N	2016-10-05 18:11:38.611	johannorin		78	APPROVED	2	3	\N	DANGLING	307	163	164	\N		784
485	A	2016-10-10 15:15:29.434	\N	2016-10-10 15:15:29.434	johannorin		481	APPROVED	2	4	\N	VALID	223	969	970	\N		1317
486	A	2016-10-10 15:15:29.442	\N	2016-10-10 15:15:29.442	johannorin		482	APPROVED	2	4	\N	VALID	223	971	972	\N		1318
487	A	2016-10-10 15:15:29.452	\N	2016-10-10 15:15:29.452	johannorin		483	APPROVED	2	4	\N	VALID	223	973	974	\N		1319
488	A	2016-10-10 15:15:29.462	\N	2016-10-10 15:15:29.462	johannorin		484	APPROVED	2	4	\N	VALID	223	975	976	\N		1320
37	A	2016-10-04 13:16:53.042	\N	2016-10-05 18:11:37.749	johannorin		33	APPROVED	2	3	\N	DANGLING	287	73	74	\N		739
38	G	2016-10-04 13:16:53.052	\N	2016-10-05 18:11:37.761	johannorin		34	APPROVED	2	3	\N	DANGLING	283	75	76	\N		740
39	A	2016-10-04 13:16:53.062	\N	2016-10-05 18:11:37.773	johannorin		35	APPROVED	2	3	\N	DANGLING	287	77	78	\N		741
40	A	2016-10-04 13:16:53.071	\N	2016-10-05 18:11:37.786	johannorin		36	APPROVED	2	3	\N	DANGLING	287	79	80	\N		742
41	A	2016-10-04 13:16:53.08	\N	2016-10-05 18:11:37.798	johannorin		37	APPROVED	2	3	\N	DANGLING	287	81	82	\N		743
54	A	2016-10-04 13:16:53.316	\N	2016-10-05 18:11:37.952	johannorin		50	APPROVED	2	3	\N	DANGLING	287	107	108	\N		756
55	A	2016-10-04 13:16:53.329	\N	2016-10-05 18:11:37.964	johannorin		51	APPROVED	2	3	\N	DANGLING	287	109	110	\N		757
56	A	2016-10-04 13:16:53.341	\N	2016-10-05 18:11:37.975	johannorin		52	APPROVED	2	3	\N	DANGLING	287	111	112	\N		758
57	A	2016-10-04 13:16:53.354	\N	2016-10-05 18:11:37.986	johannorin		53	APPROVED	2	3	\N	DANGLING	287	113	114	\N		759
58	A	2016-10-04 13:16:53.366	\N	2016-10-05 18:11:37.998	johannorin		54	APPROVED	2	3	\N	DANGLING	287	115	116	\N		760
59	G	2016-10-04 13:16:53.379	\N	2016-10-05 18:11:38.009	johannorin		55	APPROVED	2	3	\N	DANGLING	283	117	118	\N		761
60	G	2016-10-04 13:16:53.391	\N	2016-10-05 18:11:38.021	johannorin		56	APPROVED	2	3	\N	DANGLING	283	119	120	\N		762
61	G	2016-10-04 13:16:53.401	\N	2016-10-05 18:11:38.034	johannorin		57	APPROVED	2	3	\N	DANGLING	283	121	122	\N		763
62	G	2016-10-04 13:16:53.414	\N	2016-10-05 18:11:38.046	johannorin		58	APPROVED	2	3	\N	DANGLING	283	123	124	\N		764
63	G	2016-10-04 13:16:53.425	\N	2016-10-05 18:11:38.06	johannorin		59	APPROVED	2	3	\N	DANGLING	283	125	126	\N		765
64	G	2016-10-04 13:16:53.436	\N	2016-10-05 18:11:38.072	johannorin		60	APPROVED	2	3	\N	DANGLING	283	127	128	\N		766
489	A	2016-10-10 15:15:29.472	\N	2016-10-10 15:15:29.472	johannorin		485	APPROVED	2	4	\N	VALID	223	977	978	\N		1321
490	A	2016-10-10 15:15:29.484	\N	2016-10-10 15:15:29.484	johannorin		486	APPROVED	2	4	\N	VALID	223	979	980	\N		1322
491	A	2016-10-10 15:15:29.495	\N	2016-10-10 15:15:29.495	johannorin		487	APPROVED	2	4	\N	VALID	223	981	982	\N		1323
492	A	2016-10-10 15:15:29.504	\N	2016-10-10 15:15:29.504	johannorin		488	APPROVED	2	4	\N	VALID	223	983	984	\N		1324
493	A	2016-10-10 15:15:29.513	\N	2016-10-10 15:15:29.513	johannorin		489	APPROVED	2	4	\N	VALID	223	985	986	\N		1325
494	A	2016-10-10 15:15:29.522	\N	2016-10-10 15:15:29.522	johannorin		490	APPROVED	2	4	\N	VALID	223	987	988	\N		1326
495	A	2016-10-10 15:15:29.531	\N	2016-10-10 15:15:29.531	johannorin		491	APPROVED	2	4	\N	VALID	223	989	990	\N		1327
496	A	2016-10-10 15:15:29.54	\N	2016-10-10 15:15:29.54	johannorin		492	APPROVED	2	4	\N	VALID	223	991	992	\N		1328
497	A	2016-10-10 15:15:29.549	\N	2016-10-10 15:15:29.549	johannorin		493	APPROVED	2	4	\N	VALID	223	993	994	\N		1329
498	A	2016-10-10 15:15:29.557	\N	2016-10-10 15:15:29.557	johannorin		494	APPROVED	2	4	\N	VALID	223	995	996	\N		1330
499	A	2016-10-10 15:15:29.566	\N	2016-10-10 15:15:29.566	johannorin		495	APPROVED	2	4	\N	VALID	223	997	998	\N		1331
500	A	2016-10-10 15:15:29.574	\N	2016-10-10 15:15:29.574	johannorin		496	APPROVED	2	4	\N	VALID	223	999	1000	\N		1332
501	A	2016-10-10 15:15:29.584	\N	2016-10-10 15:15:29.584	johannorin		497	APPROVED	2	4	\N	VALID	223	1001	1002	\N		1333
502	A	2016-10-10 15:15:29.593	\N	2016-10-10 15:15:29.593	johannorin		498	APPROVED	2	4	\N	VALID	223	1003	1004	\N		1334
503	A	2016-10-10 15:15:29.602	\N	2016-10-10 15:15:29.602	johannorin		499	APPROVED	2	4	\N	VALID	223	1005	1006	\N		1335
504	A	2016-10-10 15:15:29.61	\N	2016-10-10 15:15:29.61	johannorin		500	APPROVED	2	4	\N	VALID	223	1007	1008	\N		1336
505	A	2016-10-10 15:15:29.619	\N	2016-10-10 15:15:29.619	johannorin		501	APPROVED	2	4	\N	VALID	223	1009	1010	\N		1337
506	A	2016-10-10 15:15:29.629	\N	2016-10-10 15:15:29.629	johannorin		502	APPROVED	2	4	\N	VALID	223	1011	1012	\N		1338
507	A	2016-10-10 15:15:29.638	\N	2016-10-10 15:15:29.638	johannorin		503	APPROVED	2	4	\N	VALID	223	1013	1014	\N		1339
508	A	2016-10-10 15:15:29.647	\N	2016-10-10 15:15:29.647	johannorin		504	APPROVED	2	4	\N	VALID	223	1015	1016	\N		1340
509	A	2016-10-10 15:15:29.656	\N	2016-10-10 15:15:29.656	johannorin		505	APPROVED	2	4	\N	VALID	223	1017	1018	\N		1341
510	A	2016-10-10 15:15:29.666	\N	2016-10-10 15:15:29.666	johannorin		506	APPROVED	2	4	\N	VALID	223	1019	1020	\N		1342
32	A	2016-10-04 13:16:52.976	\N	2016-10-05 18:11:37.685	johannorin		28	APPROVED	2	3	\N	DANGLING	287	63	64	\N		734
33	G	2016-10-04 13:16:52.998	\N	2016-10-05 18:11:37.695	johannorin		29	APPROVED	2	3	\N	DANGLING	283	65	66	\N		735
34	A	2016-10-04 13:16:53.008	\N	2016-10-05 18:11:37.713	johannorin		30	APPROVED	2	3	\N	DANGLING	287	67	68	\N		736
35	A	2016-10-04 13:16:53.018	\N	2016-10-05 18:11:37.725	johannorin		31	APPROVED	2	3	\N	DANGLING	287	69	70	\N		737
36	A	2016-10-04 13:16:53.032	\N	2016-10-05 18:11:37.737	johannorin		32	APPROVED	2	3	\N	DANGLING	287	71	72	\N		738
50	A	2016-10-04 13:16:53.247	\N	2016-10-05 18:11:37.906	johannorin		46	APPROVED	2	3	\N	DANGLING	287	99	100	\N		752
51	A	2016-10-04 13:16:53.282	\N	2016-10-05 18:11:37.918	johannorin		47	APPROVED	2	3	\N	DANGLING	287	101	102	\N		753
52	A	2016-10-04 13:16:53.293	\N	2016-10-05 18:11:37.93	johannorin		48	APPROVED	2	3	\N	DANGLING	287	103	104	\N		754
53	A	2016-10-04 13:16:53.304	\N	2016-10-05 18:11:37.941	johannorin		49	APPROVED	2	3	\N	DANGLING	287	105	106	\N		755
65	A	2016-10-04 13:16:53.442	\N	2016-10-05 18:11:38.104	johannorin		61	APPROVED	2	3	\N	DANGLING	315	129	130	\N		767
66	C	2016-10-04 15:39:39.878	\N	2016-10-05 18:11:38.136	johannorin		62	APPROVED	2	3	\N	DANGLING	315	131	132	\N		768
67	E	2016-10-04 15:39:39.891	\N	2016-10-05 18:11:38.169	johannorin		63	APPROVED	2	3	\N	DANGLING	234	133	134	\N		769
68	A	2016-10-04 15:39:39.9	\N	2016-10-05 18:11:38.203	johannorin		64	APPROVED	2	3	\N	DANGLING	254	135	136	\N		770
69	A	2016-10-04 15:39:39.909	\N	2016-10-05 18:11:38.241	johannorin		65	APPROVED	2	3	\N	DANGLING	242	137	138	\N		771
70	E	2016-10-04 15:39:39.918	\N	2016-10-05 18:11:38.275	johannorin		66	APPROVED	2	3	\N	DANGLING	234	139	140	\N		772
71	D	2016-10-04 15:39:39.927	\N	2016-10-05 18:11:38.31	johannorin		67	APPROVED	2	3	\N	DANGLING	324	141	142	\N		773
72	E	2016-10-04 15:39:39.937	\N	2016-10-05 18:11:38.343	johannorin		68	APPROVED	2	3	\N	DANGLING	234	143	144	\N		774
73	D	2016-10-04 15:39:39.945	\N	2016-10-05 18:11:38.377	johannorin		69	APPROVED	2	3	\N	DANGLING	324	145	146	\N		775
74	A	2016-10-04 15:39:39.953	\N	2016-10-05 18:11:38.409	johannorin		70	APPROVED	2	3	\N	DANGLING	254	147	148	\N		776
75	A	2016-10-04 15:39:39.964	\N	2016-10-05 18:11:38.443	johannorin		71	APPROVED	2	3	\N	DANGLING	242	149	150	\N		777
76	E	2016-10-04 15:39:39.973	\N	2016-10-05 18:11:38.475	johannorin		72	APPROVED	2	3	\N	DANGLING	234	151	152	\N		778
77	D	2016-10-04 15:39:39.987	\N	2016-10-05 18:11:38.508	johannorin		73	APPROVED	2	3	\N	DANGLING	324	153	154	\N		779
80	G	2016-10-04 15:39:40.016	\N	2016-10-05 18:11:38.586	johannorin		76	APPROVED	2	3	\N	DANGLING	\N	159	160	\N		782
42	A	2016-10-04 13:16:53.09	\N	2016-10-05 18:11:37.811	johannorin		38	APPROVED	2	3	\N	DANGLING	287	83	84	\N		744
43	G	2016-10-04 13:16:53.101	\N	2016-10-05 18:11:37.823	johannorin		39	APPROVED	2	3	\N	DANGLING	283	85	86	\N		745
44	A	2016-10-04 13:16:53.112	\N	2016-10-05 18:11:37.835	johannorin		40	APPROVED	2	3	\N	DANGLING	287	87	88	\N		746
45	A	2016-10-04 13:16:53.124	\N	2016-10-05 18:11:37.847	johannorin		41	APPROVED	2	3	\N	DANGLING	287	89	90	\N		747
46	A	2016-10-04 13:16:53.134	\N	2016-10-05 18:11:37.86	johannorin		42	APPROVED	2	3	\N	DANGLING	287	91	92	\N		748
47	A	2016-10-04 13:16:53.147	\N	2016-10-05 18:11:37.872	johannorin		43	APPROVED	2	3	\N	DANGLING	287	93	94	\N		749
48	G	2016-10-04 13:16:53.158	\N	2016-10-05 18:11:37.884	johannorin		44	APPROVED	2	3	\N	DANGLING	283	95	96	\N		750
49	A	2016-10-04 13:16:53.171	\N	2016-10-05 18:11:37.895	johannorin		45	APPROVED	2	3	\N	DANGLING	287	97	98	\N		751
83	B	2016-10-04 15:39:40.043	\N	2016-10-05 18:11:38.625	johannorin		79	APPROVED	2	3	\N	DANGLING	307	165	166	\N		785
84	G	2016-10-04 15:39:40.052	\N	2016-10-05 18:11:38.639	johannorin		80	APPROVED	2	3	\N	DANGLING	223	167	168	\N		786
85	G	2016-10-04 15:39:40.06	\N	2016-10-05 18:11:38.653	johannorin		81	APPROVED	2	3	\N	DANGLING	223	169	170	\N		787
86	B	2016-10-04 15:39:40.068	\N	2016-10-05 18:11:38.678	johannorin		82	APPROVED	2	3	\N	DANGLING	324	171	172	\N		788
87	B	2016-10-04 15:39:40.077	\N	2016-10-05 18:11:38.703	johannorin		83	APPROVED	2	3	\N	DANGLING	324	173	174	\N		789
88	L	2016-10-04 15:39:40.087	\N	2016-10-05 17:39:26.091	johannorin		84	DELETED	2	3	\N	VALID	\N	175	176	\N		125
89	A	2016-10-10 11:04:05.251	\N	2016-10-10 11:04:05.251	johannorin		85	APPROVED	2	2	\N	DANGLING	223	177	178	\N		859
90	A	2016-10-10 11:04:05.304	\N	2016-10-10 11:04:05.304	johannorin		86	APPROVED	2	2	\N	DANGLING	223	179	180	\N		860
91	A	2016-10-10 11:04:05.353	\N	2016-10-10 11:04:05.353	johannorin		87	APPROVED	2	2	\N	DANGLING	223	181	182	\N		861
92	A	2016-10-10 11:04:05.389	\N	2016-10-10 11:04:05.389	johannorin		88	APPROVED	2	2	\N	DANGLING	223	183	184	\N		862
93	A	2016-10-10 11:04:05.416	\N	2016-10-10 11:04:05.416	johannorin		89	APPROVED	2	2	\N	DANGLING	223	185	186	\N		863
94	A	2016-10-10 11:04:05.444	\N	2016-10-10 11:04:05.444	johannorin		90	APPROVED	2	2	\N	DANGLING	223	187	188	\N		864
95	A	2016-10-10 11:04:05.472	\N	2016-10-10 11:04:05.472	johannorin		91	APPROVED	2	2	\N	DANGLING	223	189	190	\N		865
96	A	2016-10-10 11:04:05.503	\N	2016-10-10 11:04:05.503	johannorin		92	APPROVED	2	2	\N	DANGLING	223	191	192	\N		866
106	A	2016-10-10 11:04:05.78	\N	2016-10-10 11:04:05.78	johannorin		102	APPROVED	2	2	\N	DANGLING	223	211	212	\N		876
107	A	2016-10-10 11:04:05.815	\N	2016-10-10 11:04:05.815	johannorin		103	APPROVED	2	2	\N	DANGLING	223	213	214	\N		877
108	A	2016-10-10 11:04:05.85	\N	2016-10-10 11:04:05.85	johannorin		104	APPROVED	2	2	\N	DANGLING	223	215	216	\N		878
109	A	2016-10-10 11:04:05.879	\N	2016-10-10 11:04:05.879	johannorin		105	APPROVED	2	2	\N	DANGLING	223	217	218	\N		879
110	A	2016-10-10 11:04:05.907	\N	2016-10-10 11:04:05.907	johannorin		106	APPROVED	2	2	\N	DANGLING	223	219	220	\N		880
111	A	2016-10-10 11:04:05.933	\N	2016-10-10 11:04:05.933	johannorin		107	APPROVED	2	2	\N	DANGLING	223	221	222	\N		881
112	A	2016-10-10 11:04:05.962	\N	2016-10-10 11:04:05.962	johannorin		108	APPROVED	2	2	\N	DANGLING	223	223	224	\N		882
113	A	2016-10-10 11:04:05.989	\N	2016-10-10 11:04:05.989	johannorin		109	APPROVED	2	2	\N	DANGLING	223	225	226	\N		883
114	A	2016-10-10 11:04:06.017	\N	2016-10-10 11:04:06.017	johannorin		110	APPROVED	2	2	\N	DANGLING	223	227	228	\N		884
115	A	2016-10-10 11:04:06.043	\N	2016-10-10 11:04:06.043	johannorin		111	APPROVED	2	2	\N	DANGLING	223	229	230	\N		885
116	A	2016-10-10 11:04:06.068	\N	2016-10-10 11:04:06.068	johannorin		112	APPROVED	2	2	\N	DANGLING	223	231	232	\N		886
117	A	2016-10-10 11:04:06.099	\N	2016-10-10 11:04:06.099	johannorin		113	APPROVED	2	2	\N	DANGLING	223	233	234	\N		887
118	A	2016-10-10 11:04:06.126	\N	2016-10-10 11:04:06.126	johannorin		114	APPROVED	2	2	\N	DANGLING	223	235	236	\N		888
119	A	2016-10-10 11:04:06.158	\N	2016-10-10 11:04:06.158	johannorin		115	APPROVED	2	2	\N	DANGLING	223	237	238	\N		889
120	A	2016-10-10 11:04:06.183	\N	2016-10-10 11:04:06.183	johannorin		116	APPROVED	2	2	\N	DANGLING	223	239	240	\N		890
121	A	2016-10-10 11:04:06.213	\N	2016-10-10 11:04:06.213	johannorin		117	APPROVED	2	2	\N	DANGLING	223	241	242	\N		891
97	A	2016-10-10 11:04:05.531	\N	2016-10-10 11:04:05.531	johannorin		93	APPROVED	2	2	\N	DANGLING	223	193	194	\N		867
98	A	2016-10-10 11:04:05.559	\N	2016-10-10 11:04:05.559	johannorin		94	APPROVED	2	2	\N	DANGLING	223	195	196	\N		868
99	A	2016-10-10 11:04:05.59	\N	2016-10-10 11:04:05.59	johannorin		95	APPROVED	2	2	\N	DANGLING	223	197	198	\N		869
100	A	2016-10-10 11:04:05.621	\N	2016-10-10 11:04:05.621	johannorin		96	APPROVED	2	2	\N	DANGLING	223	199	200	\N		870
101	A	2016-10-10 11:04:05.647	\N	2016-10-10 11:04:05.647	johannorin		97	APPROVED	2	2	\N	DANGLING	223	201	202	\N		871
102	A	2016-10-10 11:04:05.674	\N	2016-10-10 11:04:05.674	johannorin		98	APPROVED	2	2	\N	DANGLING	223	203	204	\N		872
103	A	2016-10-10 11:04:05.699	\N	2016-10-10 11:04:05.699	johannorin		99	APPROVED	2	2	\N	DANGLING	223	205	206	\N		873
104	A	2016-10-10 11:04:05.724	\N	2016-10-10 11:04:05.724	johannorin		100	APPROVED	2	2	\N	DANGLING	223	207	208	\N		874
105	A	2016-10-10 11:04:05.751	\N	2016-10-10 11:04:05.751	johannorin		101	APPROVED	2	2	\N	DANGLING	223	209	210	\N		875
122	A	2016-10-10 11:04:06.241	\N	2016-10-10 11:04:06.241	johannorin		118	APPROVED	2	2	\N	DANGLING	223	243	244	\N		892
123	A	2016-10-10 11:04:06.267	\N	2016-10-10 11:04:06.267	johannorin		119	APPROVED	2	2	\N	DANGLING	223	245	246	\N		893
124	A	2016-10-10 11:04:06.294	\N	2016-10-10 11:04:06.294	johannorin		120	APPROVED	2	2	\N	DANGLING	223	247	248	\N		894
125	A	2016-10-10 11:04:06.32	\N	2016-10-10 11:04:06.32	johannorin		121	APPROVED	2	2	\N	DANGLING	223	249	250	\N		895
126	A	2016-10-10 11:04:06.347	\N	2016-10-10 11:04:06.347	johannorin		122	APPROVED	2	2	\N	DANGLING	223	251	252	\N		896
139	A	2016-10-10 11:04:06.803	\N	2016-10-10 11:04:06.803	johannorin		135	APPROVED	2	2	\N	DANGLING	223	277	278	\N		909
140	A	2016-10-10 11:04:06.83	\N	2016-10-10 11:04:06.83	johannorin		136	APPROVED	2	2	\N	DANGLING	223	279	280	\N		910
141	A	2016-10-10 11:04:06.859	\N	2016-10-10 11:04:06.859	johannorin		137	APPROVED	2	2	\N	DANGLING	223	281	282	\N		911
142	A	2016-10-10 11:04:06.892	\N	2016-10-10 11:04:06.892	johannorin		138	APPROVED	2	2	\N	DANGLING	223	283	284	\N		912
143	A	2016-10-10 11:04:06.921	\N	2016-10-10 11:04:06.921	johannorin		139	APPROVED	2	2	\N	DANGLING	223	285	286	\N		913
144	A	2016-10-10 11:04:06.953	\N	2016-10-10 11:04:06.953	johannorin		140	APPROVED	2	2	\N	DANGLING	223	287	288	\N		914
145	A	2016-10-10 11:04:06.98	\N	2016-10-10 11:04:06.98	johannorin		141	APPROVED	2	2	\N	DANGLING	223	289	290	\N		915
146	A	2016-10-10 11:04:07.009	\N	2016-10-10 11:04:07.009	johannorin		142	APPROVED	2	2	\N	DANGLING	223	291	292	\N		916
147	A	2016-10-10 11:04:07.039	\N	2016-10-10 11:04:07.039	johannorin		143	APPROVED	2	2	\N	DANGLING	223	293	294	\N		917
148	A	2016-10-10 11:04:07.067	\N	2016-10-10 11:04:07.067	johannorin		144	APPROVED	2	2	\N	DANGLING	223	295	296	\N		918
149	A	2016-10-10 11:04:07.097	\N	2016-10-10 11:04:07.097	johannorin		145	APPROVED	2	2	\N	DANGLING	223	297	298	\N		919
150	A	2016-10-10 11:04:07.129	\N	2016-10-10 11:04:07.129	johannorin		146	APPROVED	2	2	\N	DANGLING	223	299	300	\N		920
151	A	2016-10-10 11:04:07.16	\N	2016-10-10 11:04:07.16	johannorin		147	APPROVED	2	2	\N	DANGLING	223	301	302	\N		921
152	A	2016-10-10 11:04:07.188	\N	2016-10-10 11:04:07.188	johannorin		148	APPROVED	2	2	\N	DANGLING	223	303	304	\N		922
153	A	2016-10-10 11:04:07.266	\N	2016-10-10 11:04:07.266	johannorin		149	APPROVED	2	2	\N	DANGLING	223	305	306	\N		923
154	A	2016-10-10 11:04:07.296	\N	2016-10-10 11:04:07.296	johannorin		150	APPROVED	2	2	\N	DANGLING	223	307	308	\N		924
127	A	2016-10-10 11:04:06.376	\N	2016-10-10 11:04:06.376	johannorin		123	APPROVED	2	2	\N	DANGLING	223	253	254	\N		897
128	A	2016-10-10 11:04:06.404	\N	2016-10-10 11:04:06.404	johannorin		124	APPROVED	2	2	\N	DANGLING	223	255	256	\N		898
129	A	2016-10-10 11:04:06.432	\N	2016-10-10 11:04:06.432	johannorin		125	APPROVED	2	2	\N	DANGLING	223	257	258	\N		899
130	A	2016-10-10 11:04:06.46	\N	2016-10-10 11:04:06.46	johannorin		126	APPROVED	2	2	\N	DANGLING	223	259	260	\N		900
131	A	2016-10-10 11:04:06.494	\N	2016-10-10 11:04:06.494	johannorin		127	APPROVED	2	2	\N	DANGLING	223	261	262	\N		901
132	A	2016-10-10 11:04:06.521	\N	2016-10-10 11:04:06.521	johannorin		128	APPROVED	2	2	\N	DANGLING	223	263	264	\N		902
133	A	2016-10-10 11:04:06.576	\N	2016-10-10 11:04:06.576	johannorin		129	APPROVED	2	2	\N	DANGLING	223	265	266	\N		903
134	A	2016-10-10 11:04:06.623	\N	2016-10-10 11:04:06.623	johannorin		130	APPROVED	2	2	\N	DANGLING	223	267	268	\N		904
135	A	2016-10-10 11:04:06.673	\N	2016-10-10 11:04:06.673	johannorin		131	APPROVED	2	2	\N	DANGLING	223	269	270	\N		905
136	A	2016-10-10 11:04:06.717	\N	2016-10-10 11:04:06.717	johannorin		132	APPROVED	2	2	\N	DANGLING	223	271	272	\N		906
137	A	2016-10-10 11:04:06.747	\N	2016-10-10 11:04:06.747	johannorin		133	APPROVED	2	2	\N	DANGLING	223	273	274	\N		907
138	A	2016-10-10 11:04:06.775	\N	2016-10-10 11:04:06.775	johannorin		134	APPROVED	2	2	\N	DANGLING	223	275	276	\N		908
155	A	2016-10-10 11:04:07.325	\N	2016-10-10 11:04:07.325	johannorin		151	APPROVED	2	2	\N	DANGLING	223	309	310	\N		925
156	A	2016-10-10 11:04:07.354	\N	2016-10-10 11:04:07.354	johannorin		152	APPROVED	2	2	\N	DANGLING	223	311	312	\N		926
157	A	2016-10-10 11:04:07.387	\N	2016-10-10 11:04:07.387	johannorin		153	APPROVED	2	2	\N	DANGLING	223	313	314	\N		927
158	A	2016-10-10 11:04:07.418	\N	2016-10-10 11:04:07.418	johannorin		154	APPROVED	2	2	\N	DANGLING	223	315	316	\N		928
159	A	2016-10-10 11:04:07.457	\N	2016-10-10 11:04:07.457	johannorin		155	APPROVED	2	2	\N	DANGLING	223	317	318	\N		929
160	A	2016-10-10 11:04:07.535	\N	2016-10-10 11:04:07.535	johannorin		156	APPROVED	2	2	\N	DANGLING	223	319	320	\N		930
161	A	2016-10-10 11:04:07.583	\N	2016-10-10 11:04:07.583	johannorin		157	APPROVED	2	2	\N	DANGLING	223	321	322	\N		931
162	A	2016-10-10 11:04:07.614	\N	2016-10-10 11:04:07.614	johannorin		158	APPROVED	2	2	\N	DANGLING	223	323	324	\N		932
163	A	2016-10-10 11:04:07.645	\N	2016-10-10 11:04:07.645	johannorin		159	APPROVED	2	2	\N	DANGLING	223	325	326	\N		933
164	A	2016-10-10 11:04:07.676	\N	2016-10-10 11:04:07.676	johannorin		160	APPROVED	2	2	\N	DANGLING	223	327	328	\N		934
165	A	2016-10-10 11:04:07.707	\N	2016-10-10 11:04:07.707	johannorin		161	APPROVED	2	2	\N	DANGLING	223	329	330	\N		935
166	A	2016-10-10 11:04:07.738	\N	2016-10-10 11:04:07.738	johannorin		162	APPROVED	2	2	\N	DANGLING	223	331	332	\N		936
167	A	2016-10-10 11:04:07.769	\N	2016-10-10 11:04:07.769	johannorin		163	APPROVED	2	2	\N	DANGLING	223	333	334	\N		937
168	A	2016-10-10 11:04:07.808	\N	2016-10-10 11:04:07.808	johannorin		164	APPROVED	2	2	\N	DANGLING	223	335	336	\N		938
169	A	2016-10-10 11:04:07.843	\N	2016-10-10 11:04:07.843	johannorin		165	APPROVED	2	2	\N	DANGLING	223	337	338	\N		939
170	A	2016-10-10 11:04:07.877	\N	2016-10-10 11:04:07.877	johannorin		166	APPROVED	2	2	\N	DANGLING	223	339	340	\N		940
171	A	2016-10-10 11:04:07.909	\N	2016-10-10 11:04:07.909	johannorin		167	APPROVED	2	2	\N	DANGLING	223	341	342	\N		941
172	A	2016-10-10 11:04:07.941	\N	2016-10-10 11:04:07.941	johannorin		168	APPROVED	2	2	\N	DANGLING	223	343	344	\N		942
173	A	2016-10-10 11:04:07.974	\N	2016-10-10 11:04:07.974	johannorin		169	APPROVED	2	2	\N	DANGLING	223	345	346	\N		943
174	A	2016-10-10 11:04:08.006	\N	2016-10-10 11:04:08.006	johannorin		170	APPROVED	2	2	\N	DANGLING	223	347	348	\N		944
175	A	2016-10-10 11:04:08.037	\N	2016-10-10 11:04:08.037	johannorin		171	APPROVED	2	2	\N	DANGLING	223	349	350	\N		945
176	A	2016-10-10 11:04:08.071	\N	2016-10-10 11:04:08.071	johannorin		172	APPROVED	2	2	\N	DANGLING	223	351	352	\N		946
177	A	2016-10-10 11:04:08.965	\N	2016-10-10 11:04:08.965	johannorin		173	APPROVED	2	3	\N	DANGLING	223	353	354	\N		967
178	A	2016-10-10 11:04:08.998	\N	2016-10-10 11:04:08.998	johannorin		174	APPROVED	2	3	\N	DANGLING	223	355	356	\N		968
179	A	2016-10-10 11:04:09.033	\N	2016-10-10 11:04:09.033	johannorin		175	APPROVED	2	3	\N	DANGLING	223	357	358	\N		969
180	A	2016-10-10 11:04:09.083	\N	2016-10-10 11:04:09.083	johannorin		176	APPROVED	2	3	\N	DANGLING	223	359	360	\N		970
181	A	2016-10-10 11:04:09.141	\N	2016-10-10 11:04:09.141	johannorin		177	APPROVED	2	3	\N	DANGLING	223	361	362	\N		971
182	A	2016-10-10 11:04:09.174	\N	2016-10-10 11:04:09.174	johannorin		178	APPROVED	2	3	\N	DANGLING	223	363	364	\N		972
183	A	2016-10-10 11:04:09.208	\N	2016-10-10 11:04:09.208	johannorin		179	APPROVED	2	3	\N	DANGLING	223	365	366	\N		973
184	A	2016-10-10 11:04:09.246	\N	2016-10-10 11:04:09.246	johannorin		180	APPROVED	2	3	\N	DANGLING	223	367	368	\N		974
185	A	2016-10-10 11:04:09.302	\N	2016-10-10 11:04:09.302	johannorin		181	APPROVED	2	3	\N	DANGLING	223	369	370	\N		975
186	A	2016-10-10 11:04:09.344	\N	2016-10-10 11:04:09.344	johannorin		182	APPROVED	2	3	\N	DANGLING	223	371	372	\N		976
187	A	2016-10-10 11:04:09.411	\N	2016-10-10 11:04:09.411	johannorin		183	APPROVED	2	3	\N	DANGLING	223	373	374	\N		977
188	A	2016-10-10 11:04:09.456	\N	2016-10-10 11:04:09.456	johannorin		184	APPROVED	2	3	\N	DANGLING	223	375	376	\N		978
189	A	2016-10-10 11:04:09.517	\N	2016-10-10 11:04:09.517	johannorin		185	APPROVED	2	3	\N	DANGLING	223	377	378	\N		979
190	A	2016-10-10 11:04:09.563	\N	2016-10-10 11:04:09.563	johannorin		186	APPROVED	2	3	\N	DANGLING	223	379	380	\N		980
191	A	2016-10-10 11:04:09.597	\N	2016-10-10 11:04:09.597	johannorin		187	APPROVED	2	3	\N	DANGLING	223	381	382	\N		981
192	A	2016-10-10 11:04:09.635	\N	2016-10-10 11:04:09.635	johannorin		188	APPROVED	2	3	\N	DANGLING	223	383	384	\N		982
193	A	2016-10-10 11:04:09.669	\N	2016-10-10 11:04:09.669	johannorin		189	APPROVED	2	3	\N	DANGLING	223	385	386	\N		983
194	A	2016-10-10 11:04:09.705	\N	2016-10-10 11:04:09.705	johannorin		190	APPROVED	2	3	\N	DANGLING	223	387	388	\N		984
195	A	2016-10-10 11:04:09.739	\N	2016-10-10 11:04:09.739	johannorin		191	APPROVED	2	3	\N	DANGLING	223	389	390	\N		985
196	A	2016-10-10 11:04:09.773	\N	2016-10-10 11:04:09.773	johannorin		192	APPROVED	2	3	\N	DANGLING	223	391	392	\N		986
197	A	2016-10-10 11:04:09.811	\N	2016-10-10 11:04:09.811	johannorin		193	APPROVED	2	3	\N	DANGLING	223	393	394	\N		987
198	A	2016-10-10 11:04:09.852	\N	2016-10-10 11:04:09.852	johannorin		194	APPROVED	2	3	\N	DANGLING	223	395	396	\N		988
199	A	2016-10-10 11:04:09.888	\N	2016-10-10 11:04:09.888	johannorin		195	APPROVED	2	3	\N	DANGLING	223	397	398	\N		989
200	A	2016-10-10 11:04:09.929	\N	2016-10-10 11:04:09.929	johannorin		196	APPROVED	2	3	\N	DANGLING	223	399	400	\N		990
201	A	2016-10-10 11:04:09.963	\N	2016-10-10 11:04:09.963	johannorin		197	APPROVED	2	3	\N	DANGLING	223	401	402	\N		991
202	A	2016-10-10 11:04:09.998	\N	2016-10-10 11:04:09.998	johannorin		198	APPROVED	2	3	\N	DANGLING	223	403	404	\N		992
203	A	2016-10-10 11:04:10.036	\N	2016-10-10 11:04:10.036	johannorin		199	APPROVED	2	3	\N	DANGLING	223	405	406	\N		993
204	A	2016-10-10 11:04:10.082	\N	2016-10-10 11:04:10.082	johannorin		200	APPROVED	2	3	\N	DANGLING	223	407	408	\N		994
205	A	2016-10-10 11:04:10.121	\N	2016-10-10 11:04:10.121	johannorin		201	APPROVED	2	3	\N	DANGLING	223	409	410	\N		995
206	A	2016-10-10 11:04:10.158	\N	2016-10-10 11:04:10.158	johannorin		202	APPROVED	2	3	\N	DANGLING	223	411	412	\N		996
207	A	2016-10-10 11:04:10.193	\N	2016-10-10 11:04:10.193	johannorin		203	APPROVED	2	3	\N	DANGLING	223	413	414	\N		997
208	A	2016-10-10 11:04:10.229	\N	2016-10-10 11:04:10.229	johannorin		204	APPROVED	2	3	\N	DANGLING	223	415	416	\N		998
209	A	2016-10-10 11:04:10.267	\N	2016-10-10 11:04:10.267	johannorin		205	APPROVED	2	3	\N	DANGLING	223	417	418	\N		999
210	A	2016-10-10 11:04:10.304	\N	2016-10-10 11:04:10.304	johannorin		206	APPROVED	2	3	\N	DANGLING	223	419	420	\N		1000
211	A	2016-10-10 11:04:10.344	\N	2016-10-10 11:04:10.344	johannorin		207	APPROVED	2	3	\N	DANGLING	223	421	422	\N		1001
212	A	2016-10-10 11:04:10.387	\N	2016-10-10 11:04:10.387	johannorin		208	APPROVED	2	3	\N	DANGLING	223	423	424	\N		1002
213	A	2016-10-10 11:04:10.427	\N	2016-10-10 11:04:10.427	johannorin		209	APPROVED	2	3	\N	DANGLING	223	425	426	\N		1003
214	A	2016-10-10 11:04:10.469	\N	2016-10-10 11:04:10.469	johannorin		210	APPROVED	2	3	\N	DANGLING	223	427	428	\N		1004
215	A	2016-10-10 11:04:10.509	\N	2016-10-10 11:04:10.509	johannorin		211	APPROVED	2	3	\N	DANGLING	223	429	430	\N		1005
216	A	2016-10-10 11:04:10.548	\N	2016-10-10 11:04:10.548	johannorin		212	APPROVED	2	3	\N	DANGLING	223	431	432	\N		1006
217	A	2016-10-10 11:04:10.588	\N	2016-10-10 11:04:10.588	johannorin		213	APPROVED	2	3	\N	DANGLING	223	433	434	\N		1007
218	A	2016-10-10 11:04:10.627	\N	2016-10-10 11:04:10.627	johannorin		214	APPROVED	2	3	\N	DANGLING	223	435	436	\N		1008
219	A	2016-10-10 11:04:10.667	\N	2016-10-10 11:04:10.667	johannorin		215	APPROVED	2	3	\N	DANGLING	223	437	438	\N		1009
220	A	2016-10-10 11:04:10.704	\N	2016-10-10 11:04:10.704	johannorin		216	APPROVED	2	3	\N	DANGLING	223	439	440	\N		1010
221	A	2016-10-10 11:04:10.74	\N	2016-10-10 11:04:10.74	johannorin		217	APPROVED	2	3	\N	DANGLING	223	441	442	\N		1011
222	A	2016-10-10 11:04:10.776	\N	2016-10-10 11:04:10.776	johannorin		218	APPROVED	2	3	\N	DANGLING	223	443	444	\N		1012
223	A	2016-10-10 11:04:10.812	\N	2016-10-10 11:04:10.812	johannorin		219	APPROVED	2	3	\N	DANGLING	223	445	446	\N		1013
224	A	2016-10-10 11:04:10.852	\N	2016-10-10 11:04:10.852	johannorin		220	APPROVED	2	3	\N	DANGLING	223	447	448	\N		1014
225	A	2016-10-10 11:04:10.889	\N	2016-10-10 11:04:10.889	johannorin		221	APPROVED	2	3	\N	DANGLING	223	449	450	\N		1015
226	A	2016-10-10 11:04:10.927	\N	2016-10-10 11:04:10.927	johannorin		222	APPROVED	2	3	\N	DANGLING	223	451	452	\N		1016
227	A	2016-10-10 11:04:10.969	\N	2016-10-10 11:04:10.969	johannorin		223	APPROVED	2	3	\N	DANGLING	223	453	454	\N		1017
228	A	2016-10-10 11:04:11.038	\N	2016-10-10 11:04:11.038	johannorin		224	APPROVED	2	3	\N	DANGLING	223	455	456	\N		1018
229	A	2016-10-10 11:04:11.088	\N	2016-10-10 11:04:11.088	johannorin		225	APPROVED	2	3	\N	DANGLING	223	457	458	\N		1019
230	A	2016-10-10 11:04:11.128	\N	2016-10-10 11:04:11.128	johannorin		226	APPROVED	2	3	\N	DANGLING	223	459	460	\N		1020
231	A	2016-10-10 11:04:11.17	\N	2016-10-10 11:04:11.17	johannorin		227	APPROVED	2	3	\N	DANGLING	223	461	462	\N		1021
232	A	2016-10-10 11:04:11.21	\N	2016-10-10 11:04:11.21	johannorin		228	APPROVED	2	3	\N	DANGLING	223	463	464	\N		1022
233	A	2016-10-10 11:04:11.251	\N	2016-10-10 11:04:11.251	johannorin		229	APPROVED	2	3	\N	DANGLING	223	465	466	\N		1023
234	A	2016-10-10 11:04:11.289	\N	2016-10-10 11:04:11.289	johannorin		230	APPROVED	2	3	\N	DANGLING	223	467	468	\N		1024
235	A	2016-10-10 11:04:11.326	\N	2016-10-10 11:04:11.326	johannorin		231	APPROVED	2	3	\N	DANGLING	223	469	470	\N		1025
236	A	2016-10-10 11:04:11.367	\N	2016-10-10 11:04:11.367	johannorin		232	APPROVED	2	3	\N	DANGLING	223	471	472	\N		1026
237	A	2016-10-10 11:04:11.409	\N	2016-10-10 11:04:11.409	johannorin		233	APPROVED	2	3	\N	DANGLING	223	473	474	\N		1027
238	A	2016-10-10 11:04:11.447	\N	2016-10-10 11:04:11.447	johannorin		234	APPROVED	2	3	\N	DANGLING	223	475	476	\N		1028
239	A	2016-10-10 11:04:11.498	\N	2016-10-10 11:04:11.498	johannorin		235	APPROVED	2	3	\N	DANGLING	223	477	478	\N		1029
240	A	2016-10-10 11:04:11.549	\N	2016-10-10 11:04:11.549	johannorin		236	APPROVED	2	3	\N	DANGLING	223	479	480	\N		1030
241	A	2016-10-10 11:04:11.591	\N	2016-10-10 11:04:11.591	johannorin		237	APPROVED	2	3	\N	DANGLING	223	481	482	\N		1031
242	A	2016-10-10 11:04:11.63	\N	2016-10-10 11:04:11.63	johannorin		238	APPROVED	2	3	\N	DANGLING	223	483	484	\N		1032
243	A	2016-10-10 11:04:11.668	\N	2016-10-10 11:04:11.668	johannorin		239	APPROVED	2	3	\N	DANGLING	223	485	486	\N		1033
244	A	2016-10-10 11:04:11.721	\N	2016-10-10 11:04:11.721	johannorin		240	APPROVED	2	3	\N	DANGLING	223	487	488	\N		1034
245	A	2016-10-10 11:04:11.759	\N	2016-10-10 11:04:11.759	johannorin		241	APPROVED	2	3	\N	DANGLING	223	489	490	\N		1035
246	A	2016-10-10 11:04:11.798	\N	2016-10-10 11:04:11.798	johannorin		242	APPROVED	2	3	\N	DANGLING	223	491	492	\N		1036
247	A	2016-10-10 11:04:11.835	\N	2016-10-10 11:04:11.835	johannorin		243	APPROVED	2	3	\N	DANGLING	223	493	494	\N		1037
248	A	2016-10-10 11:04:11.872	\N	2016-10-10 11:04:11.872	johannorin		244	APPROVED	2	3	\N	DANGLING	223	495	496	\N		1038
249	A	2016-10-10 11:04:11.909	\N	2016-10-10 11:04:11.909	johannorin		245	APPROVED	2	3	\N	DANGLING	223	497	498	\N		1039
250	A	2016-10-10 11:04:11.948	\N	2016-10-10 11:04:11.948	johannorin		246	APPROVED	2	3	\N	DANGLING	223	499	500	\N		1040
251	A	2016-10-10 11:04:11.987	\N	2016-10-10 11:04:11.987	johannorin		247	APPROVED	2	3	\N	DANGLING	223	501	502	\N		1041
252	A	2016-10-10 11:04:12.026	\N	2016-10-10 11:04:12.026	johannorin		248	APPROVED	2	3	\N	DANGLING	223	503	504	\N		1042
253	A	2016-10-10 11:04:12.073	\N	2016-10-10 11:04:12.073	johannorin		249	APPROVED	2	3	\N	DANGLING	223	505	506	\N		1043
254	A	2016-10-10 11:04:12.113	\N	2016-10-10 11:04:12.113	johannorin		250	APPROVED	2	3	\N	DANGLING	223	507	508	\N		1044
255	A	2016-10-10 11:04:12.154	\N	2016-10-10 11:04:12.154	johannorin		251	APPROVED	2	3	\N	DANGLING	223	509	510	\N		1045
256	A	2016-10-10 11:04:12.195	\N	2016-10-10 11:04:12.195	johannorin		252	APPROVED	2	3	\N	DANGLING	223	511	512	\N		1046
257	A	2016-10-10 11:04:12.238	\N	2016-10-10 11:04:12.238	johannorin		253	APPROVED	2	3	\N	DANGLING	223	513	514	\N		1047
258	A	2016-10-10 11:04:12.297	\N	2016-10-10 11:04:12.297	johannorin		254	APPROVED	2	3	\N	DANGLING	223	515	516	\N		1048
259	A	2016-10-10 11:04:12.336	\N	2016-10-10 11:04:12.336	johannorin		255	APPROVED	2	3	\N	DANGLING	223	517	518	\N		1049
260	A	2016-10-10 11:04:12.408	\N	2016-10-10 11:04:12.408	johannorin		256	APPROVED	2	3	\N	DANGLING	223	519	520	\N		1050
261	A	2016-10-10 11:04:12.458	\N	2016-10-10 11:04:12.458	johannorin		257	APPROVED	2	3	\N	DANGLING	223	521	522	\N		1051
262	A	2016-10-10 11:04:12.546	\N	2016-10-10 11:04:12.546	johannorin		258	APPROVED	2	3	\N	DANGLING	223	523	524	\N		1052
263	A	2016-10-10 11:04:12.608	\N	2016-10-10 11:04:12.608	johannorin		259	APPROVED	2	3	\N	DANGLING	223	525	526	\N		1053
264	A	2016-10-10 11:04:12.669	\N	2016-10-10 11:04:12.669	johannorin		260	APPROVED	2	3	\N	DANGLING	223	527	528	\N		1054
265	A	2016-10-10 11:04:12.717	\N	2016-10-10 11:04:12.717	johannorin		261	APPROVED	2	3	\N	DANGLING	223	529	530	\N		1055
266	A	2016-10-10 11:04:12.759	\N	2016-10-10 11:04:12.759	johannorin		262	APPROVED	2	3	\N	DANGLING	223	531	532	\N		1056
267	A	2016-10-10 11:04:12.875	\N	2016-10-10 11:04:12.875	johannorin		263	APPROVED	2	3	\N	DANGLING	223	533	534	\N		1057
268	A	2016-10-10 11:04:12.917	\N	2016-10-10 11:04:12.917	johannorin		264	APPROVED	2	3	\N	DANGLING	223	535	536	\N		1058
269	A	2016-10-10 11:04:12.972	\N	2016-10-10 11:04:12.972	johannorin		265	APPROVED	2	3	\N	DANGLING	223	537	538	\N		1059
270	A	2016-10-10 11:04:13.036	\N	2016-10-10 11:04:13.036	johannorin		266	APPROVED	2	3	\N	DANGLING	223	539	540	\N		1060
271	A	2016-10-10 11:04:13.103	\N	2016-10-10 11:04:13.103	johannorin		267	APPROVED	2	3	\N	DANGLING	223	541	542	\N		1061
272	A	2016-10-10 11:04:13.149	\N	2016-10-10 11:04:13.149	johannorin		268	APPROVED	2	3	\N	DANGLING	223	543	544	\N		1062
273	A	2016-10-10 11:04:13.214	\N	2016-10-10 11:04:13.214	johannorin		269	APPROVED	2	3	\N	DANGLING	223	545	546	\N		1063
274	A	2016-10-10 11:04:13.256	\N	2016-10-10 11:04:13.256	johannorin		270	APPROVED	2	3	\N	DANGLING	223	547	548	\N		1064
275	A	2016-10-10 11:04:13.296	\N	2016-10-10 11:04:13.296	johannorin		271	APPROVED	2	3	\N	DANGLING	223	549	550	\N		1065
276	A	2016-10-10 11:04:13.335	\N	2016-10-10 11:04:13.335	johannorin		272	APPROVED	2	3	\N	DANGLING	223	551	552	\N		1066
277	A	2016-10-10 11:04:13.375	\N	2016-10-10 11:04:13.375	johannorin		273	APPROVED	2	3	\N	DANGLING	223	553	554	\N		1067
278	A	2016-10-10 11:04:13.414	\N	2016-10-10 11:04:13.414	johannorin		274	APPROVED	2	3	\N	DANGLING	223	555	556	\N		1068
279	A	2016-10-10 11:04:13.452	\N	2016-10-10 11:04:13.452	johannorin		275	APPROVED	2	3	\N	DANGLING	223	557	558	\N		1069
280	A	2016-10-10 11:04:13.491	\N	2016-10-10 11:04:13.491	johannorin		276	APPROVED	2	3	\N	DANGLING	223	559	560	\N		1070
281	A	2016-10-10 11:04:13.529	\N	2016-10-10 11:04:13.529	johannorin		277	APPROVED	2	3	\N	DANGLING	223	561	562	\N		1071
282	A	2016-10-10 11:04:13.566	\N	2016-10-10 11:04:13.566	johannorin		278	APPROVED	2	3	\N	DANGLING	223	563	564	\N		1072
283	A	2016-10-10 11:04:13.604	\N	2016-10-10 11:04:13.604	johannorin		279	APPROVED	2	3	\N	DANGLING	223	565	566	\N		1073
284	A	2016-10-10 11:04:13.642	\N	2016-10-10 11:04:13.642	johannorin		280	APPROVED	2	3	\N	DANGLING	223	567	568	\N		1074
285	A	2016-10-10 11:04:13.685	\N	2016-10-10 11:04:13.685	johannorin		281	APPROVED	2	3	\N	DANGLING	223	569	570	\N		1075
286	A	2016-10-10 11:04:13.723	\N	2016-10-10 11:04:13.723	johannorin		282	APPROVED	2	3	\N	DANGLING	223	571	572	\N		1076
287	A	2016-10-10 11:04:13.762	\N	2016-10-10 11:04:13.762	johannorin		283	APPROVED	2	3	\N	DANGLING	223	573	574	\N		1077
288	A	2016-10-10 11:04:13.801	\N	2016-10-10 11:04:13.801	johannorin		284	APPROVED	2	3	\N	DANGLING	223	575	576	\N		1078
289	A	2016-10-10 11:04:13.84	\N	2016-10-10 11:04:13.84	johannorin		285	APPROVED	2	3	\N	DANGLING	223	577	578	\N		1079
290	A	2016-10-10 11:04:13.887	\N	2016-10-10 11:04:13.887	johannorin		286	APPROVED	2	3	\N	DANGLING	223	579	580	\N		1080
291	A	2016-10-10 11:04:13.927	\N	2016-10-10 11:04:13.927	johannorin		287	APPROVED	2	3	\N	DANGLING	223	581	582	\N		1081
292	A	2016-10-10 11:04:13.966	\N	2016-10-10 11:04:13.966	johannorin		288	APPROVED	2	3	\N	DANGLING	223	583	584	\N		1082
293	A	2016-10-10 11:04:14.005	\N	2016-10-10 11:04:14.005	johannorin		289	APPROVED	2	3	\N	DANGLING	223	585	586	\N		1083
294	A	2016-10-10 11:04:14.044	\N	2016-10-10 11:04:14.044	johannorin		290	APPROVED	2	3	\N	DANGLING	223	587	588	\N		1084
295	A	2016-10-10 11:04:14.084	\N	2016-10-10 11:04:14.084	johannorin		291	APPROVED	2	3	\N	DANGLING	223	589	590	\N		1085
296	A	2016-10-10 11:04:14.126	\N	2016-10-10 11:04:14.126	johannorin		292	APPROVED	2	3	\N	DANGLING	223	591	592	\N		1086
297	A	2016-10-10 11:04:14.165	\N	2016-10-10 11:04:14.165	johannorin		293	APPROVED	2	3	\N	DANGLING	223	593	594	\N		1087
298	A	2016-10-10 11:04:14.205	\N	2016-10-10 11:04:14.205	johannorin		294	APPROVED	2	3	\N	DANGLING	223	595	596	\N		1088
299	A	2016-10-10 11:04:14.245	\N	2016-10-10 11:04:14.245	johannorin		295	APPROVED	2	3	\N	DANGLING	223	597	598	\N		1089
300	A	2016-10-10 11:04:14.283	\N	2016-10-10 11:04:14.283	johannorin		296	APPROVED	2	3	\N	DANGLING	223	599	600	\N		1090
301	A	2016-10-10 11:04:14.323	\N	2016-10-10 11:04:14.323	johannorin		297	APPROVED	2	3	\N	DANGLING	223	601	602	\N		1091
302	A	2016-10-10 11:04:14.364	\N	2016-10-10 11:04:14.364	johannorin		298	APPROVED	2	3	\N	DANGLING	223	603	604	\N		1092
303	A	2016-10-10 11:04:14.408	\N	2016-10-10 11:04:14.408	johannorin		299	APPROVED	2	3	\N	DANGLING	223	605	606	\N		1093
304	A	2016-10-10 11:04:14.454	\N	2016-10-10 11:04:14.454	johannorin		300	APPROVED	2	3	\N	DANGLING	223	607	608	\N		1094
305	A	2016-10-10 11:04:14.521	\N	2016-10-10 11:04:14.521	johannorin		301	APPROVED	2	3	\N	DANGLING	223	609	610	\N		1095
306	A	2016-10-10 11:04:14.576	\N	2016-10-10 11:04:14.576	johannorin		302	APPROVED	2	3	\N	DANGLING	223	611	612	\N		1096
307	A	2016-10-10 11:04:14.623	\N	2016-10-10 11:04:14.623	johannorin		303	APPROVED	2	3	\N	DANGLING	223	613	614	\N		1097
308	A	2016-10-10 11:04:14.665	\N	2016-10-10 11:04:14.665	johannorin		304	APPROVED	2	3	\N	DANGLING	223	615	616	\N		1098
309	A	2016-10-10 11:04:14.711	\N	2016-10-10 11:04:14.711	johannorin		305	APPROVED	2	3	\N	DANGLING	223	617	618	\N		1099
310	A	2016-10-10 11:04:14.753	\N	2016-10-10 11:04:14.753	johannorin		306	APPROVED	2	3	\N	DANGLING	223	619	620	\N		1100
311	A	2016-10-10 11:04:14.794	\N	2016-10-10 11:04:14.794	johannorin		307	APPROVED	2	3	\N	DANGLING	223	621	622	\N		1101
312	A	2016-10-10 11:04:14.835	\N	2016-10-10 11:04:14.835	johannorin		308	APPROVED	2	3	\N	DANGLING	223	623	624	\N		1102
313	A	2016-10-10 11:04:14.881	\N	2016-10-10 11:04:14.881	johannorin		309	APPROVED	2	3	\N	DANGLING	223	625	626	\N		1103
314	A	2016-10-10 11:04:14.956	\N	2016-10-10 11:04:14.956	johannorin		310	APPROVED	2	3	\N	DANGLING	223	627	628	\N		1104
315	A	2016-10-10 11:04:14.998	\N	2016-10-10 11:04:14.998	johannorin		311	APPROVED	2	3	\N	DANGLING	223	629	630	\N		1105
316	A	2016-10-10 11:04:15.037	\N	2016-10-10 11:04:15.037	johannorin		312	APPROVED	2	3	\N	DANGLING	223	631	632	\N		1106
317	A	2016-10-10 11:04:15.077	\N	2016-10-10 11:04:15.077	johannorin		313	APPROVED	2	3	\N	DANGLING	223	633	634	\N		1107
318	A	2016-10-10 11:04:15.117	\N	2016-10-10 11:04:15.117	johannorin		314	APPROVED	2	3	\N	DANGLING	223	635	636	\N		1108
319	A	2016-10-10 11:04:15.157	\N	2016-10-10 11:04:15.157	johannorin		315	APPROVED	2	3	\N	DANGLING	223	637	638	\N		1109
320	A	2016-10-10 11:04:15.197	\N	2016-10-10 11:04:15.197	johannorin		316	APPROVED	2	3	\N	DANGLING	223	639	640	\N		1110
321	A	2016-10-10 11:04:15.238	\N	2016-10-10 11:04:15.238	johannorin		317	APPROVED	2	3	\N	DANGLING	223	641	642	\N		1111
322	A	2016-10-10 11:04:15.277	\N	2016-10-10 11:04:15.277	johannorin		318	APPROVED	2	3	\N	DANGLING	223	643	644	\N		1112
323	A	2016-10-10 11:04:15.317	\N	2016-10-10 11:04:15.317	johannorin		319	APPROVED	2	3	\N	DANGLING	223	645	646	\N		1113
324	A	2016-10-10 11:04:15.358	\N	2016-10-10 11:04:15.358	johannorin		320	APPROVED	2	3	\N	DANGLING	223	647	648	\N		1114
325	A	2016-10-10 11:04:15.398	\N	2016-10-10 11:04:15.398	johannorin		321	APPROVED	2	3	\N	DANGLING	223	649	650	\N		1115
326	A	2016-10-10 11:04:15.438	\N	2016-10-10 11:04:15.438	johannorin		322	APPROVED	2	3	\N	DANGLING	223	651	652	\N		1116
327	A	2016-10-10 11:04:15.478	\N	2016-10-10 11:04:15.478	johannorin		323	APPROVED	2	3	\N	DANGLING	223	653	654	\N		1117
328	A	2016-10-10 11:04:15.518	\N	2016-10-10 11:04:15.518	johannorin		324	APPROVED	2	3	\N	DANGLING	223	655	656	\N		1118
329	A	2016-10-10 11:04:15.558	\N	2016-10-10 11:04:15.558	johannorin		325	APPROVED	2	3	\N	DANGLING	223	657	658	\N		1119
330	A	2016-10-10 11:04:15.6	\N	2016-10-10 11:04:15.6	johannorin		326	APPROVED	2	3	\N	DANGLING	223	659	660	\N		1120
331	A	2016-10-10 11:04:15.641	\N	2016-10-10 11:04:15.641	johannorin		327	APPROVED	2	3	\N	DANGLING	223	661	662	\N		1121
332	A	2016-10-10 11:04:15.681	\N	2016-10-10 11:04:15.681	johannorin		328	APPROVED	2	3	\N	DANGLING	223	663	664	\N		1122
333	A	2016-10-10 11:04:15.723	\N	2016-10-10 11:04:15.723	johannorin		329	APPROVED	2	4	\N	DANGLING	223	665	666	\N		1123
334	A	2016-10-10 11:04:15.765	\N	2016-10-10 11:04:15.765	johannorin		330	APPROVED	2	4	\N	DANGLING	223	667	668	\N		1124
335	A	2016-10-10 11:04:15.806	\N	2016-10-10 11:04:15.806	johannorin		331	APPROVED	2	4	\N	DANGLING	223	669	670	\N		1125
336	A	2016-10-10 11:04:15.848	\N	2016-10-10 11:04:15.848	johannorin		332	APPROVED	2	4	\N	DANGLING	223	671	672	\N		1126
337	A	2016-10-10 11:04:15.891	\N	2016-10-10 11:04:15.891	johannorin		333	APPROVED	2	4	\N	DANGLING	223	673	674	\N		1127
338	A	2016-10-10 11:04:15.933	\N	2016-10-10 11:04:15.933	johannorin		334	APPROVED	2	4	\N	DANGLING	223	675	676	\N		1128
339	A	2016-10-10 11:04:15.976	\N	2016-10-10 11:04:15.976	johannorin		335	APPROVED	2	4	\N	DANGLING	223	677	678	\N		1129
340	A	2016-10-10 11:04:16.019	\N	2016-10-10 11:04:16.019	johannorin		336	APPROVED	2	4	\N	DANGLING	223	679	680	\N		1130
341	A	2016-10-10 11:04:16.103	\N	2016-10-10 11:04:16.103	johannorin		337	APPROVED	2	4	\N	DANGLING	223	681	682	\N		1131
342	A	2016-10-10 11:04:16.16	\N	2016-10-10 11:04:16.16	johannorin		338	APPROVED	2	4	\N	DANGLING	223	683	684	\N		1132
343	A	2016-10-10 11:04:16.202	\N	2016-10-10 11:04:16.202	johannorin		339	APPROVED	2	4	\N	DANGLING	223	685	686	\N		1133
344	A	2016-10-10 11:04:16.245	\N	2016-10-10 11:04:16.245	johannorin		340	APPROVED	2	4	\N	DANGLING	223	687	688	\N		1134
345	A	2016-10-10 11:04:16.31	\N	2016-10-10 11:04:16.31	johannorin		341	APPROVED	2	4	\N	DANGLING	223	689	690	\N		1135
346	A	2016-10-10 11:04:16.353	\N	2016-10-10 11:04:16.353	johannorin		342	APPROVED	2	4	\N	DANGLING	223	691	692	\N		1136
347	A	2016-10-10 11:04:16.398	\N	2016-10-10 11:04:16.398	johannorin		343	APPROVED	2	4	\N	DANGLING	223	693	694	\N		1137
348	A	2016-10-10 11:04:16.441	\N	2016-10-10 11:04:16.441	johannorin		344	APPROVED	2	4	\N	DANGLING	223	695	696	\N		1138
349	A	2016-10-10 11:04:16.521	\N	2016-10-10 11:04:16.521	johannorin		345	APPROVED	2	4	\N	DANGLING	223	697	698	\N		1139
350	A	2016-10-10 11:04:16.572	\N	2016-10-10 11:04:16.572	johannorin		346	APPROVED	2	4	\N	DANGLING	223	699	700	\N		1140
351	A	2016-10-10 11:04:16.616	\N	2016-10-10 11:04:16.616	johannorin		347	APPROVED	2	4	\N	DANGLING	223	701	702	\N		1141
352	A	2016-10-10 11:04:16.703	\N	2016-10-10 11:04:16.703	johannorin		348	APPROVED	2	4	\N	DANGLING	223	703	704	\N		1142
353	A	2016-10-10 11:04:16.764	\N	2016-10-10 11:04:16.764	johannorin		349	APPROVED	2	4	\N	DANGLING	223	705	706	\N		1143
354	A	2016-10-10 11:04:16.834	\N	2016-10-10 11:04:16.834	johannorin		350	APPROVED	2	4	\N	DANGLING	223	707	708	\N		1144
355	A	2016-10-10 11:04:16.956	\N	2016-10-10 11:04:16.956	johannorin		351	APPROVED	2	4	\N	DANGLING	223	709	710	\N		1145
356	A	2016-10-10 11:04:16.999	\N	2016-10-10 11:04:16.999	johannorin		352	APPROVED	2	4	\N	DANGLING	223	711	712	\N		1146
357	A	2016-10-10 11:04:17.042	\N	2016-10-10 11:04:17.042	johannorin		353	APPROVED	2	4	\N	DANGLING	223	713	714	\N		1147
358	A	2016-10-10 11:04:17.088	\N	2016-10-10 11:04:17.088	johannorin		354	APPROVED	2	4	\N	DANGLING	223	715	716	\N		1148
359	A	2016-10-10 11:04:17.157	\N	2016-10-10 11:04:17.157	johannorin		355	APPROVED	2	4	\N	DANGLING	223	717	718	\N		1149
360	A	2016-10-10 11:04:17.209	\N	2016-10-10 11:04:17.209	johannorin		356	APPROVED	2	4	\N	DANGLING	223	719	720	\N		1150
361	A	2016-10-10 11:04:17.253	\N	2016-10-10 11:04:17.253	johannorin		357	APPROVED	2	4	\N	DANGLING	223	721	722	\N		1151
362	A	2016-10-10 11:04:17.296	\N	2016-10-10 11:04:17.296	johannorin		358	APPROVED	2	4	\N	DANGLING	223	723	724	\N		1152
363	A	2016-10-10 11:04:17.339	\N	2016-10-10 11:04:17.339	johannorin		359	APPROVED	2	4	\N	DANGLING	223	725	726	\N		1153
364	A	2016-10-10 11:04:17.383	\N	2016-10-10 11:04:17.383	johannorin		360	APPROVED	2	4	\N	DANGLING	223	727	728	\N		1154
365	A	2016-10-10 11:04:17.427	\N	2016-10-10 11:04:17.427	johannorin		361	APPROVED	2	4	\N	DANGLING	223	729	730	\N		1155
366	A	2016-10-10 11:04:17.47	\N	2016-10-10 11:04:17.47	johannorin		362	APPROVED	2	4	\N	DANGLING	223	731	732	\N		1156
367	A	2016-10-10 11:04:17.513	\N	2016-10-10 11:04:17.513	johannorin		363	APPROVED	2	4	\N	DANGLING	223	733	734	\N		1157
368	A	2016-10-10 11:04:17.556	\N	2016-10-10 11:04:17.556	johannorin		364	APPROVED	2	4	\N	DANGLING	223	735	736	\N		1158
369	A	2016-10-10 11:04:17.599	\N	2016-10-10 11:04:17.599	johannorin		365	APPROVED	2	4	\N	DANGLING	223	737	738	\N		1159
370	A	2016-10-10 11:04:17.644	\N	2016-10-10 11:04:17.644	johannorin		366	APPROVED	2	4	\N	DANGLING	223	739	740	\N		1160
371	A	2016-10-10 11:04:17.688	\N	2016-10-10 11:04:17.688	johannorin		367	APPROVED	2	4	\N	DANGLING	223	741	742	\N		1161
372	A	2016-10-10 11:04:17.733	\N	2016-10-10 11:04:17.733	johannorin		368	APPROVED	2	4	\N	DANGLING	223	743	744	\N		1162
373	A	2016-10-10 11:04:17.777	\N	2016-10-10 11:04:17.777	johannorin		369	APPROVED	2	4	\N	DANGLING	223	745	746	\N		1163
374	A	2016-10-10 11:04:17.823	\N	2016-10-10 11:04:17.823	johannorin		370	APPROVED	2	4	\N	DANGLING	223	747	748	\N		1164
375	A	2016-10-10 11:04:17.882	\N	2016-10-10 11:04:17.882	johannorin		371	APPROVED	2	4	\N	DANGLING	223	749	750	\N		1165
376	A	2016-10-10 11:04:17.931	\N	2016-10-10 11:04:17.931	johannorin		372	APPROVED	2	4	\N	DANGLING	223	751	752	\N		1166
377	A	2016-10-10 11:04:17.975	\N	2016-10-10 11:04:17.975	johannorin		373	APPROVED	2	4	\N	DANGLING	223	753	754	\N		1167
378	A	2016-10-10 11:04:18.019	\N	2016-10-10 11:04:18.019	johannorin		374	APPROVED	2	4	\N	DANGLING	223	755	756	\N		1168
379	A	2016-10-10 11:04:18.063	\N	2016-10-10 11:04:18.063	johannorin		375	APPROVED	2	4	\N	DANGLING	223	757	758	\N		1169
380	A	2016-10-10 11:04:18.108	\N	2016-10-10 11:04:18.108	johannorin		376	APPROVED	2	4	\N	DANGLING	223	759	760	\N		1170
381	A	2016-10-10 11:04:18.153	\N	2016-10-10 11:04:18.153	johannorin		377	APPROVED	2	4	\N	DANGLING	223	761	762	\N		1171
382	A	2016-10-10 11:04:18.2	\N	2016-10-10 11:04:18.2	johannorin		378	APPROVED	2	4	\N	DANGLING	223	763	764	\N		1172
383	A	2016-10-10 11:04:18.249	\N	2016-10-10 11:04:18.249	johannorin		379	APPROVED	2	4	\N	DANGLING	223	765	766	\N		1173
384	A	2016-10-10 11:04:18.299	\N	2016-10-10 11:04:18.299	johannorin		380	APPROVED	2	4	\N	DANGLING	223	767	768	\N		1174
385	A	2016-10-10 11:04:18.349	\N	2016-10-10 11:04:18.349	johannorin		381	APPROVED	2	4	\N	DANGLING	223	769	770	\N		1175
386	A	2016-10-10 11:04:18.397	\N	2016-10-10 11:04:18.397	johannorin		382	APPROVED	2	4	\N	DANGLING	223	771	772	\N		1176
387	A	2016-10-10 11:04:18.445	\N	2016-10-10 11:04:18.445	johannorin		383	APPROVED	2	4	\N	DANGLING	223	773	774	\N		1177
388	A	2016-10-10 11:04:18.496	\N	2016-10-10 11:04:18.496	johannorin		384	APPROVED	2	4	\N	DANGLING	223	775	776	\N		1178
389	A	2016-10-10 11:04:18.542	\N	2016-10-10 11:04:18.542	johannorin		385	APPROVED	2	4	\N	DANGLING	223	777	778	\N		1179
390	A	2016-10-10 11:04:18.586	\N	2016-10-10 11:04:18.586	johannorin		386	APPROVED	2	4	\N	DANGLING	223	779	780	\N		1180
391	A	2016-10-10 11:04:18.699	\N	2016-10-10 11:04:18.699	johannorin		387	APPROVED	2	4	\N	DANGLING	223	781	782	\N		1181
392	A	2016-10-10 11:04:18.745	\N	2016-10-10 11:04:18.745	johannorin		388	APPROVED	2	4	\N	DANGLING	223	783	784	\N		1182
393	A	2016-10-10 11:04:18.802	\N	2016-10-10 11:04:18.802	johannorin		389	APPROVED	2	4	\N	DANGLING	223	785	786	\N		1183
394	A	2016-10-10 11:04:18.848	\N	2016-10-10 11:04:18.848	johannorin		390	APPROVED	2	4	\N	DANGLING	223	787	788	\N		1184
395	A	2016-10-10 11:04:18.898	\N	2016-10-10 11:04:18.898	johannorin		391	APPROVED	2	4	\N	DANGLING	223	789	790	\N		1185
396	A	2016-10-10 11:04:18.944	\N	2016-10-10 11:04:18.944	johannorin		392	APPROVED	2	4	\N	DANGLING	223	791	792	\N		1186
397	A	2016-10-10 11:04:18.99	\N	2016-10-10 11:04:18.99	johannorin		393	APPROVED	2	4	\N	DANGLING	223	793	794	\N		1187
398	A	2016-10-10 11:04:19.037	\N	2016-10-10 11:04:19.037	johannorin		394	APPROVED	2	4	\N	DANGLING	223	795	796	\N		1188
399	A	2016-10-10 11:04:19.083	\N	2016-10-10 11:04:19.083	johannorin		395	APPROVED	2	4	\N	DANGLING	223	797	798	\N		1189
400	A	2016-10-10 11:04:19.132	\N	2016-10-10 11:04:19.132	johannorin		396	APPROVED	2	4	\N	DANGLING	223	799	800	\N		1190
401	A	2016-10-10 11:04:19.179	\N	2016-10-10 11:04:19.179	johannorin		397	APPROVED	2	4	\N	DANGLING	223	801	802	\N		1191
402	A	2016-10-10 11:04:19.228	\N	2016-10-10 11:04:19.228	johannorin		398	APPROVED	2	4	\N	DANGLING	223	803	804	\N		1192
403	A	2016-10-10 11:04:19.275	\N	2016-10-10 11:04:19.275	johannorin		399	APPROVED	2	4	\N	DANGLING	223	805	806	\N		1193
404	A	2016-10-10 11:04:19.323	\N	2016-10-10 11:04:19.323	johannorin		400	APPROVED	2	4	\N	DANGLING	223	807	808	\N		1194
405	A	2016-10-10 11:04:19.372	\N	2016-10-10 11:04:19.372	johannorin		401	APPROVED	2	4	\N	DANGLING	223	809	810	\N		1195
406	A	2016-10-10 11:04:19.421	\N	2016-10-10 11:04:19.421	johannorin		402	APPROVED	2	4	\N	DANGLING	223	811	812	\N		1196
407	A	2016-10-10 11:04:19.472	\N	2016-10-10 11:04:19.472	johannorin		403	APPROVED	2	4	\N	DANGLING	223	813	814	\N		1197
408	A	2016-10-10 11:04:19.573	\N	2016-10-10 11:04:19.573	johannorin		404	APPROVED	2	4	\N	DANGLING	223	815	816	\N		1198
409	A	2016-10-10 11:04:19.627	\N	2016-10-10 11:04:19.627	johannorin		405	APPROVED	2	4	\N	DANGLING	223	817	818	\N		1199
410	A	2016-10-10 11:04:19.675	\N	2016-10-10 11:04:19.675	johannorin		406	APPROVED	2	4	\N	DANGLING	223	819	820	\N		1200
411	A	2016-10-10 11:04:19.723	\N	2016-10-10 11:04:19.723	johannorin		407	APPROVED	2	4	\N	DANGLING	223	821	822	\N		1201
412	A	2016-10-10 11:04:19.77	\N	2016-10-10 11:04:19.77	johannorin		408	APPROVED	2	4	\N	DANGLING	223	823	824	\N		1202
413	A	2016-10-10 11:04:19.817	\N	2016-10-10 11:04:19.817	johannorin		409	APPROVED	2	4	\N	DANGLING	223	825	826	\N		1203
414	A	2016-10-10 11:04:19.866	\N	2016-10-10 11:04:19.866	johannorin		410	APPROVED	2	4	\N	DANGLING	223	827	828	\N		1204
415	A	2016-10-10 11:04:19.912	\N	2016-10-10 11:04:19.912	johannorin		411	APPROVED	2	4	\N	DANGLING	223	829	830	\N		1205
416	A	2016-10-10 11:04:19.965	\N	2016-10-10 11:04:19.965	johannorin		412	APPROVED	2	4	\N	DANGLING	223	831	832	\N		1206
417	A	2016-10-10 11:04:20.014	\N	2016-10-10 11:04:20.014	johannorin		413	APPROVED	2	4	\N	DANGLING	223	833	834	\N		1207
418	A	2016-10-10 11:04:20.061	\N	2016-10-10 11:04:20.061	johannorin		414	APPROVED	2	4	\N	DANGLING	223	835	836	\N		1208
419	A	2016-10-10 11:04:20.111	\N	2016-10-10 11:04:20.111	johannorin		415	APPROVED	2	4	\N	DANGLING	223	837	838	\N		1209
420	A	2016-10-10 11:04:20.16	\N	2016-10-10 11:04:20.16	johannorin		416	APPROVED	2	4	\N	DANGLING	223	839	840	\N		1210
421	A	2016-10-10 11:04:20.208	\N	2016-10-10 11:04:20.208	johannorin		417	APPROVED	2	2	\N	DANGLING	223	841	842	\N		1211
422	A	2016-10-10 11:04:20.255	\N	2016-10-10 11:04:20.255	johannorin		418	APPROVED	2	2	\N	DANGLING	223	843	844	\N		1212
423	A	2016-10-10 11:04:20.303	\N	2016-10-10 11:04:20.303	johannorin		419	APPROVED	2	2	\N	DANGLING	223	845	846	\N		1213
424	A	2016-10-10 11:04:20.35	\N	2016-10-10 11:04:20.35	johannorin		420	APPROVED	2	2	\N	DANGLING	223	847	848	\N		1214
425	A	2016-10-10 11:04:20.398	\N	2016-10-10 11:04:20.398	johannorin		421	APPROVED	2	2	\N	DANGLING	223	849	850	\N		1215
426	A	2016-10-10 11:04:20.446	\N	2016-10-10 11:04:20.446	johannorin		422	APPROVED	2	2	\N	DANGLING	223	851	852	\N		1216
427	A	2016-10-10 11:04:20.497	\N	2016-10-10 11:04:20.497	johannorin		423	APPROVED	2	2	\N	DANGLING	223	853	854	\N		1217
428	A	2016-10-10 11:04:20.544	\N	2016-10-10 11:04:20.544	johannorin		424	APPROVED	2	2	\N	DANGLING	223	855	856	\N		1218
429	A	2016-10-10 11:04:20.591	\N	2016-10-10 11:04:20.591	johannorin		425	APPROVED	2	2	\N	DANGLING	223	857	858	\N		1219
430	A	2016-10-10 11:04:20.641	\N	2016-10-10 11:04:20.641	johannorin		426	APPROVED	2	2	\N	DANGLING	223	859	860	\N		1220
431	A	2016-10-10 11:04:20.691	\N	2016-10-10 11:04:20.691	johannorin		427	APPROVED	2	2	\N	DANGLING	223	861	862	\N		1221
432	A	2016-10-10 11:04:20.932	\N	2016-10-10 11:04:20.932	johannorin		428	APPROVED	2	3	\N	DANGLING	223	863	864	\N		1225
433	A	2016-10-10 11:04:20.984	\N	2016-10-10 11:04:20.984	johannorin		429	APPROVED	2	3	\N	DANGLING	223	865	866	\N		1226
434	A	2016-10-10 11:04:21.035	\N	2016-10-10 11:04:21.035	johannorin		430	APPROVED	2	3	\N	DANGLING	223	867	868	\N		1227
435	A	2016-10-10 11:04:21.086	\N	2016-10-10 11:04:21.086	johannorin		431	APPROVED	2	3	\N	DANGLING	223	869	870	\N		1228
436	A	2016-10-10 11:04:21.137	\N	2016-10-10 11:04:21.137	johannorin		432	APPROVED	2	3	\N	DANGLING	223	871	872	\N		1229
437	A	2016-10-10 11:04:21.187	\N	2016-10-10 11:04:21.187	johannorin		433	APPROVED	2	3	\N	DANGLING	223	873	874	\N		1230
438	A	2016-10-10 11:04:21.254	\N	2016-10-10 11:04:21.254	johannorin		434	APPROVED	2	3	\N	DANGLING	223	875	876	\N		1231
439	A	2016-10-10 11:04:21.322	\N	2016-10-10 11:04:21.322	johannorin		435	APPROVED	2	3	\N	DANGLING	223	877	878	\N		1232
440	A	2016-10-10 11:04:21.389	\N	2016-10-10 11:04:21.389	johannorin		436	APPROVED	2	3	\N	DANGLING	223	879	880	\N		1233
441	A	2016-10-10 11:04:21.44	\N	2016-10-10 11:04:21.44	johannorin		437	APPROVED	2	3	\N	DANGLING	223	881	882	\N		1234
442	A	2016-10-10 11:04:21.493	\N	2016-10-10 11:04:21.493	johannorin		438	APPROVED	2	3	\N	DANGLING	223	883	884	\N		1235
443	A	2016-10-10 11:04:21.553	\N	2016-10-10 11:04:21.553	johannorin		439	APPROVED	2	3	\N	DANGLING	223	885	886	\N		1236
444	A	2016-10-10 11:04:21.612	\N	2016-10-10 11:04:21.612	johannorin		440	APPROVED	2	3	\N	DANGLING	223	887	888	\N		1237
445	A	2016-10-10 11:04:21.669	\N	2016-10-10 11:04:21.669	johannorin		441	APPROVED	2	3	\N	DANGLING	223	889	890	\N		1238
446	A	2016-10-10 11:04:21.719	\N	2016-10-10 11:04:21.719	johannorin		442	APPROVED	2	3	\N	DANGLING	223	891	892	\N		1239
447	A	2016-10-10 11:04:21.77	\N	2016-10-10 11:04:21.77	johannorin		443	APPROVED	2	3	\N	DANGLING	223	893	894	\N		1240
448	A	2016-10-10 11:04:21.822	\N	2016-10-10 11:04:21.822	johannorin		444	APPROVED	2	3	\N	DANGLING	223	895	896	\N		1241
449	A	2016-10-10 11:04:21.872	\N	2016-10-10 11:04:21.872	johannorin		445	APPROVED	2	3	\N	DANGLING	223	897	898	\N		1242
450	A	2016-10-10 11:04:21.929	\N	2016-10-10 11:04:21.929	johannorin		446	APPROVED	2	3	\N	DANGLING	223	899	900	\N		1243
451	A	2016-10-10 11:04:21.997	\N	2016-10-10 11:04:21.997	johannorin		447	APPROVED	2	3	\N	DANGLING	223	901	902	\N		1244
452	A	2016-10-10 11:04:22.048	\N	2016-10-10 11:04:22.048	johannorin		448	APPROVED	2	4	\N	DANGLING	223	903	904	\N		1245
453	A	2016-10-10 11:04:22.098	\N	2016-10-10 11:04:22.098	johannorin		449	APPROVED	2	4	\N	DANGLING	223	905	906	\N		1246
454	A	2016-10-10 11:04:22.152	\N	2016-10-10 11:04:22.152	johannorin		450	APPROVED	2	4	\N	DANGLING	223	907	908	\N		1247
455	A	2016-10-10 11:04:22.202	\N	2016-10-10 11:04:22.202	johannorin		451	APPROVED	2	4	\N	DANGLING	223	909	910	\N		1248
456	A	2016-10-10 11:04:22.252	\N	2016-10-10 11:04:22.252	johannorin		452	APPROVED	2	4	\N	DANGLING	223	911	912	\N		1249
457	A	2016-10-10 11:04:22.302	\N	2016-10-10 11:04:22.302	johannorin		453	APPROVED	2	4	\N	DANGLING	223	913	914	\N		1250
458	A	2016-10-10 11:04:22.352	\N	2016-10-10 11:04:22.352	johannorin		454	APPROVED	2	4	\N	DANGLING	223	915	916	\N		1251
459	A	2016-10-10 11:04:22.404	\N	2016-10-10 11:04:22.404	johannorin		455	APPROVED	2	4	\N	DANGLING	223	917	918	\N		1252
460	A	2016-10-10 11:04:22.454	\N	2016-10-10 11:04:22.454	johannorin		456	APPROVED	2	4	\N	DANGLING	223	919	920	\N		1253
461	A	2016-10-10 11:04:22.506	\N	2016-10-10 11:04:22.506	johannorin		457	APPROVED	2	4	\N	DANGLING	223	921	922	\N		1254
\.


--
-- Name: cable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('cable_id_seq', 510, true);


--
-- Data for Name: cabletype; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY cabletype (id, active, description, comments, flammability, insulation, jacket, model, name, installationtype, service, tid, weight, diameter, voltage, manufacturer_id, datasheet_id) FROM stdin;
223	t	1/2" Heliax 50Ω  , Low Density Foam Coaxial Cable, corrugated copper, 1/2 in, fire retardant polyolefin jacket		Fire Retardant	Foam Dielectric	fire retardant polyolefin jacke, halogen free	http://www.commscope.com/	1/2"RF coaxial	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	5000	\N	\N
224	t	1/2" CELLFLEX® Low-Loss Foam-Dielectric Coaxial Cable		Halogene Free	Foam Polyethylene	Polyethylene, PE	http://www.rfsworld.com/websearchecat/datasheets/?q=SCF12-50J	1/2"RF coaxial_HF	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	5000	\N	\N
225	t	1/4" Heliax 50Ω		Fire Retardant	Foam Dielectric	fire retardant polyolefin jacke, halogen free	http://www.commscope.com/catalog/wireless/product_details.aspx?id=1350	1/4"RF coaxial	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	5000	\N	\N
226	t	1/4" CELLFLEX® Low-Loss Foam-Dielectric Coaxial Cable		Halogene Free	Foam Polyethylene	Polyethylene, PE	http://www.rfsworld.com/websearch/DataSheets/pdf/?q=LCF14-50J	1/4"RF coaxial_HF	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	5000	\N	\N
227	t	1/4" CELLFLEX® Superflexible Foam-Dielectric Coaxial Cable		Flame Retardant, LS0H	Foam Polyethylene	Polyethylene, PE, Metalhydroxite Filling	http://www.rfsworld.com/websearchecat/datasheets/?q=SCF14-50J	1/4"RF coaxial superflexible	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	5000	\N	\N
228	t	3/8" CELLFLEX® Low-Loss Foam-Dielectric Coaxial Cable		Halogene Free	Foam Polyethylene	Polyethylene, PE	http://www.rfsworld.com/websearchecat/datasheets/?q=LCF38-50J	3/8"RF coaxial_HF	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	5000	\N	\N
229	t	3/8" Heliax 50Ω		Fire Retardant	Foam Dielectric	fire retardant polyolefin jacke, halogen free	http://www.commscope.com/catalog/wireless/product_details.aspx?id=1352	3/8"RF coaxial	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	4000	\N	\N
230	t	7/8" Heliax 50Ω		Fire Retardant	Foam Dielectric	fire retardant polyolefin jacke, halogen free	http://www.commscope.com/catalog/wireless/product_details.aspx?id=1331	7/8" RF coaxial	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	5000	\N	\N
231	t	7/8" CELLFLEX® Premium Attenuation Low-Loss Foam-Dielectric Coaxial Cable		Halogen Free	Foam Polyethylene	Polyethylene, PE	http://www.rfsworld.com/websearchecat/datasheets/?q=LCF78-50JA-A0	7/8" RF coaxial_HF	INDOOR	Radio Frequency Systems, Beam Instrumentaion	\N	\N	\N	8000	\N	\N
232	t	Single Conductor, AVA750,\nHELIAX® Andrew Virtual Air™ Coaxial Cable, corrugated copper, 1-5/8 in, black nonhalogenated, fire retardant polyolefin jacket.			Foam PE	Non-Halogen Polyolefin	http://www.commscope.com/catalog/wireless/product_details.aspx?id=14710	1-5/8" RF coaxial	INDOOR	RF Signal Cabling	\N	\N	\N	8000	\N	\N
233	t	10 Conductor, 0.32mm2, 22 AWG, Stranded,  Non Paired, overall shield			PVC	PVC	http://www.belden.com/techdatas/english/9946.pdf	10C22OS	INDOOR	Control Cable	\N	\N	\N	300	\N	\N
234	t	10 Pair, 0,32mm2, 22 AWG, Twisted Pair, overall shield			PVC	PVC	http://www.belden.com/techdatas/metric/8310.pdf	10PR22OS	INDOOR	Instrumentation	\N	\N	\N	300	\N	\N
235	t	10 Conductor, 0,32mm2, 22 AWG, Stranded,  Non Paired, overall shield, overall braid, drain wire			PVC	PE	http://www.alphawire.com/en/Products/Wire/Hook-Up-Wire/.../5859_10.aspx	10C22OSB	INDOOR	Instrumentation	\N	\N	\N	300	\N	\N
236	t	6 conductors, 0,5mm2, 20 AWG, color coded, overall shield, XPE, PUR, EPR insulation			XLPE	EPR	\N	6C20OSHF	INDOOR	Controls, Analog Cable	\N	\N	\N	300	\N	\N
237	t	12 Conductor ,  1,31mm2, 16 AWG,stranded, tape, drain wire, braid		Halogen Free	MPPE	MPPE	http://alphawire.com/en/Products/Cable/EcoGen/EcoCable/77228	12C16OS6VHF	INDOOR		\N	\N	\N	600	\N	\N
238	t	12 Conductor, 0,52mm2 , 20 AWG, Stranded Tinned Copper, Foil Shield, Nylon Ripcord, with copper drain wire			PVC	PVC	http://www.alphawire.com/Products/Cable/Xtra-Guard-Performance-Cable/Xtra-Guard-1/5562C.aspx	12C20OS	INDOOR		\N	\N	\N	300	\N	\N
239	t	12 Conductor, 0,52mm2 , 20 AWG, Stranded Tinned Copper, Foil Shield, Nylon Ripcord, with copper drain wire			XLPE	LSHF	\N	12C20OSHF	INDOOR	Instrumentation	\N	\N	\N	300	\N	\N
240	t	12 Twisted Pair,  1,31mm2, 16 AWG, individual shield / overall shield			PVC	PVC	http://www.belden.com/techdatas/metric/1495A.pdf	12P16OIS	INDOOR	Signal	\N	\N	\N	300	\N	\N
241	t	12 Pair,  0,52mm2, 20 AWG, overall shield, individual shield			PVC	PVC	http://www.belden.com/techdatas/metric/1078A.pdf	12PR20OIS	INDOOR	Controls, Analog Cable	\N	\N	\N	300	\N	\N
242	t	12 Pair, 0,52mm2 20 AWG, overall shield,tray rated			XLPE	LSHF	\N	12PR20OSHF	INDOOR	Controls, Analog Cable	\N	\N	\N	300	\N	\N
243	t	12 Pair, 0,32mm2, 22 AWG, overall shield/individual shield, tray rated			PVC	PVC	http://www.belden.com/techdatas/metric/3009A.pdf	12PR22OIS	INDOOR	Controls, Analog Cable	\N	\N	\N	300	\N	\N
244	t	12 Pair, 0,32mm2, 22 AWG, overall shield/individual shield, tray rated			XLPE	LSHF	\N	12PR22OISHF	INDOOR	Controls, Analog Cable	\N	\N	\N	300	\N	\N
245	t	12 pairs, 1,31mm2, 16 AWG, Individual and Overall shield, EPR, CPE			EPR	CPE	http://www.generalcable.com/	12PR16IOSTC6V	INDOOR	Controls, Analog Cable	\N	\N	\N	600	\N	\N
246	t	12 twisted pair 1,5mm2, 16AWG , individual and overall shied, tinned copper conductors,  tinned copper wire braid			EPR	Flame retardant, halogen-free and mud resistant thermoset compound, SHF2 (IEC\n60092-360)	http://www.draka.no/	12PR16IOSTCHF	UNKNOWN	Fixed installation for instrumentation,\ncommunication, Control and alarm systems	\N	\N	\N	300	\N	\N
247	t	12 pairs, 1,31mm2, 18 AWG, Individual and Overall shield, EPR, LSHF			EPR	LSHF	http://brunskabel.de/Drupal/en/products	12PR18IOSTCHF	INDOOR		\N	\N	\N	300	\N	\N
248	t	12 pairs, 0,52mm2, 20 AWG, Individual and Overall shield, EPR, LSHF, 300 Volts			EPR	LSHF	http://brunskabel.de/Drupal/en/products	12PR20IOSTCHF	INDOOR		\N	\N	\N	300	\N	\N
249	t	12 Pair, 0,2mm2, 24 AWG, stranded, tinned copper Conductors,  Individually shielded and jacketed Pairs, Overall shield, overall PVC jacket with ripcord.			Polyolefin	PVC	http://www.belden.com/techdatas/metric/1513C.pdf	12PR24IOS	INDOOR	Instrumentation	\N	\N	\N	300	\N	\N
250	t	12 Pair, 0,2mm2, 24 AWG, stranded, tinned copper Conductors,  Individually shielded and jacketed Pairs, Overall shield,  jacket with ripcord.			XLPE	LSHF	\N	12PR24IOSHF	INDOOR	Instrumentation	\N	\N	\N	300	\N	\N
251	t	4 twisted pair, 0,2mm2, 24AWG, Alum/Mylar Tape, 25% Overlap, Min., tinned copper, drain wire				LSHF	http://www.alphawire.com/en/Products/Cable/Alpha-Essentials/Communication-and-Control-Cable/5474C	4PR24OSTCHF	INDOOR		\N	\N	\N	300	\N	\N
252	t	4 pairs, 0,25mm2, 22 AWG, color coded, overall shield, PVC insulation, PE jacket, drain wire			PVC	PE	http://www.alphawire.com/en/Products/Cable/Xtra-Guard-Performance-Cable/Xtra-Guard-3/35484	4PR22OSHF	INDOOR		\N	\N	\N	300	\N	\N
253	t	4 pairs, 0,25mm2, 22 AWG, color coded, overall shield, XLPE, PUR,PE insulation			XLPE	EPR	\N	4PR22OS	INDOOR		\N	\N	\N	300	\N	\N
255	t	5 pairs,0,75mm2, 18 AWG, color coded, overall shield					\N	5P18OSHF	INDOOR		\N	\N	\N	\N	\N	\N
256	t	3 pairs, 0,25mm2/22 AWG, color coded, overall shield, XLPE, PUR,PE insulation			XLPE	EPR	\N	3P22OS3VHF	INDOOR		\N	\N	\N	300	\N	\N
257	t	Sumitomo FuterFlex 62.5 micron Multimode, 12 fibers, with Outdoor/All Dielectric tube bundle   (02, 04, 07, or 19 inner tubes)	Sumitomo FB12M6 (fiber) & TCxxTOX (tube bundle) (for 02, 07, or 19 tubes) or TCxxTOD (for 04 tubes) Where xx = the # of tubes.		NA	PE	https://www.sumitomoelectric.com/	12MFibSBFOutdoor	INDOOR	Fiber Optic Cable for Data Comm, Spec. 16129	\N	\N	\N	\N	\N	\N
258	t	14 Conductor, 0,82mm2, 18 AWG, Control Cable, Stranded Copper Conductors, Shielded XLPE Insulation, PVC Jacket			XLPE	PVC	\N	14C18OS6V	INDOOR	turbo pump cable for the insulating vacuum system	\N	\N	\N	600	\N	\N
259	t	14 Conductor, 0,82mm2, 18 AWG, Control Cable, Stranded Copper Conductors, Shielded XLPE Insulation					\N	14C18OS6VHF	INDOOR		\N	\N	\N	\N	\N	\N
260	t	15 Conductor, 0,33mm2, 22 AWG, Stranded,   overall shield.			PVC	PVC	http://www.belden.com	15C22OS3VTC	INDOOR		\N	\N	\N	300	\N	\N
261	t	15 Conductor, 0,2mm2, 24 AWG, Stranded Copper, Overall shield, RS 232 Cable			PVC	PE	http://www.drakauk.com/	15C24OSRS232	INDOOR	Beam Instrumentation	\N	\N	\N	\N	\N	\N
262	t	16 Pairs, 20 AWG, Individual/Overall Shielding, Plus One 22 AWG Communication Wire			PVC	PVC	\N	16PR20WCTC3V	INDOOR	Beam Instrumentation	\N	\N	\N	300	\N	\N
263	t	16 Pair, 1,31mm2, 16 AWG,overall shield/individual shield			PVC	PVC	http://www.belden.com/techdatas/english/3051A.pdf	16PR16IOS	INDOOR	Control Cable Analog	\N	\N	\N	300	\N	\N
264	t	17 Pair, 1,31mm2, 16 AWG,overall shield/individual shield			XLPE	Polyolefine	http://www.helmacab.nl/	16PR16IOSHF	OUTDOOR		\N	\N	\N	300	\N	\N
265	t	16 Pair, 0,32mm2, 22 AWG, overall shield			PVC	PVC	http://www.belden.com/techdatas/metric/3011A.pdf	16PR22OS	INDOOR	Control Cable Analog	\N	\N	\N	300	\N	\N
266	t	16 Pair, 0,32mm2, 22 AWG, overall shield, halogen free			XLPE	PE	\N	16P22OSHF	INDOOR	Control Cable Analog	\N	\N	\N	300	\N	\N
267	t	16 pair, 0,82mm2, 18 AWG, Individual and Overall shield			XLPO	TPE	http://www.belden.com/techdatas/english/2123A.pdf	16PR18IOS6V	INDOOR	Instrumentation Cable	\N	\N	\N	600	\N	\N
268	t	20 twisted pairs, 0,5mm2, 20 AWG, stranded tineed copper, Core Wrap Nonwoven Polyester Tape, 25% Overlap, Min., Shield: Alum/Mylar Tape, 25% Overlap, Min.				PE	http://www.alphawire.com/en/Products/Cable/EcoGen/EcoCable-Mini/78420	20PR20OSHF	INDOOR		\N	\N	\N	300	\N	\N
269	t	25 conductors, 0,32mm2, 22 AWG, Shield: A/P/A Tape, 25% Overlap, Min. Drain Wire 22 (7/30) AWG Tinned Copper\n Braid Tinned Copper,70% Coverage, Modified Polyphenylene Ether-PE			Polyester Tape	PE	http://alphawire.com/en/Products/Cable/EcoGen/EcoCable-Mini/78454	25C22TCSHF	INDOOR		\N	\N	\N	300	\N	\N
270	t	Sumitomo FuterFlex 62.5 micron Multimode, 18 fibers, with Outdoor/All Dielectric tube bundle  (02, 04, 07, or 19 inner tubes)	Sumitomo FB18M6 (fiber) & TCxxTOX (tube bundle) (for 02, 07, or 19 tubes) or TCxxTOD (for 04 tubes) Where xx = the # of tubes.		NA	PE	https://www.sumitomoelectric.com/	18MFibSBF	INDOOR	Fiber Optic Cable for Data Comm, Spec. 16129	\N	\N	\N	\N	\N	\N
271	t	Single conductor, stranded, Flexible, low loss communications coax, non-halogen (non-toxic), low smoke, fire retardant cable			Foam PE	PE	https://www.timesmicrowave.com/documents/resources/LMR-400.pdf	1C_RG-8HV	INDOOR	Beam Instrumentation	\N	\N	\N	2500	\N	\N
272	t	11 AWG stranded (7x19) .108" bare copper conductor, foam polyethylene insulation, double bare copper braid shield (96% coverage) polyethylene jacket			Foam PE	PE	http://www.belden.com/techdatas/metric/9888.pdf	1C-RG-8/U	INDOOR	Beam Instrumentation	\N	\N	\N	300	\N	\N
273	t	1 Conductor, 6mm2, 10 AWG, Locomotive cable			EPR	None	http://www.amercable.com/	1C10DEL	INDOOR	Flexible cabling for Magnet Power	\N	\N	\N	2000	\N	\N
274	t	Single Conductor, 10 AWG, 6mm2, High Voltage Silicone Lead Wire,stranded, tin coated copper wire, white			Silicone rubber	N/A	https://www.roweindustries.com	1C10SLW	INDOOR	Beam Instrumentation	\N	\N	\N	5000	\N	\N
275	t	RG-213, 1 Conductor, 2,63mm2, 13 AWG bare copper, Outer Conductor - 33 AWG bare copper with 95.3% coverage.			Solid PE	XLPE	https://www.timesmicrowave.com/documents/	1C13RG-213HF	INDOOR	RF	\N	\N	\N	5000	\N	\N
276	t	RG-213, 1 Conductor, 2,63mm2, 13 AWG bare copper, Outer Conductor - 33 AWG bare copper with 95.3% coverage.			Solid PE	XLPE	http://www.timesmicrowave.com/	1C13RG-214HF	INDOOR		\N	\N	\N	5000	\N	\N
277	t	Coax, RG-6/U, 18 AWG 75 ohm			PE	PVC	http://www.belden.com/techdatas/metric/3092A.pdf	1C18RG-6	INDOOR		\N	\N	\N	\N	\N	\N
278	t	Series 6, 18 AWG solid .040" bare copper-covered steel conductor(s), gas-injected foam polyethylene insulation, Duobond Plus® + aluminum braid shield (75% coverage), polyethylene jacket			Foam Polyethylene	PE	http://www.belden.com	1C18RG-6HF	INDOOR		\N	\N	\N	350	\N	\N
279	t	RG-223/U, One Conductor, 19 AWG, 0,65mm2			PE	PVC	http://www.nexans.com/Poland/2007/Coax.pdf	1C19RG-223	INDOOR	controls, beam diagnostic	\N	\N	\N	\N	\N	\N
280	t	RG-223/U, One Conductor Coax, 0,65mm2, 19 AWG, Tray Rated (Low Smoke, Non-Halogen)			PE	PE	http://www.nexans.com/Poland/2007/Coax.pdf	1C19RG-223HF	INDOOR	controls, beam diagnostic	\N	\N	\N	\N	\N	\N
281	t	1 Conductor, 20 AWG, RG-58, Solid			Solid PE	Flame Retardant PVC	https://www.commscope.com/Docs/Uniprise_Coax_Brochure.pdf	1C20RG-58	INDOOR		\N	\N	\N	\N	\N	\N
282	t	1 Conductor, 20 AWG, Coax, stranded, RG-58/U			Foam PE	PVC	http://www.belden.com/techdatas/metric/8219.pdf	1C20RG-58U	INDOOR		\N	\N	\N	\N	\N	\N
283	t	HV-Coax, Working voltage > 7kV, RG58 size, XPE Insulation, Red Jacket			XLPE	FRNC	http://www.prysmiangroup.com/en/business_markets/markets/multimedia/downloads/datasheets/hf43d.pdf	1C20RG-58HV	INDOOR		\N	\N	\N	\N	\N	\N
284	t	1 Conductor, coax, 0,63mm2			PE	Polyolefine	http://www.nexans.com/Poland/2007/Coax.pdf	1C20RG-58HF	INDOOR		\N	\N	\N	\N	\N	\N
285	t	RG-59, 20 AWG, Solic BC			Foam PE	LSHF	https://www.commscope.com/Docs/Coax_Brochure_EMEA.pdf	1C20RG-59	INDOOR	Linac	\N	\N	\N	\N	\N	\N
286	t	1 Conductor, RG-59/U , solid conductor			PE	Polyolefine	http://www.nexans.com/Poland/2007/Coax.pdf	1C20RG-59HF	INDOOR		\N	\N	\N	\N	\N	\N
287	t	Single Conductor 20 AWG Solid bare copper Conductor, Video Triax Cable			Foam  PE (FHDPE)	PVC	http://www.belden.com/techdatas/english/8232A.pdf	1C20RG-59U_Triax	INDOOR	Fast Valve Sensors	\N	\N	\N	\N	\N	\N
288	t	1 Conductor, 22 AWG, Coax, Standed Conductor			FPE	PVC	http://www.belden.com/techdatas/english/9259.pdf	1C22RG-59/U	INDOOR	Video Signal	\N	\N	\N	\N	\N	\N
289	t	1 conductor RG-174 A/U, 50ohm			PE	LSHF	http://www.fscables.com/Coaxial/RG+Coaxial+Cables/50+Ohm+RG+Coaxial+Cables/list.htm	1CRG-174HF	INDOOR	Cryomodule cabling	\N	\N	\N	\N	\N	\N
290	t	single conductor 16mm2 450/750V halogen free, flame retardant power cable			none	XL-HFFR	http://www.nexans.com/eservice/Corporate-en/navigate_267718/Alsecure_Plus_Alsecure_Premium_Fire_resistant_cables.html	1C-H07Z-R 16	INDOOR		\N	\N	\N	\N	\N	\N
324	t	8 Pair, I conductor 22 AWG, stranded,shielded, tray rated			Flame retardant PVC	Flame retardant PVC	http://belden.com/techdatas/english/3006A.pdf	8P1C22ST3V	INDOOR	Multiconductor	\N	\N	\N	300	\N	\N
291	t	single conductor 25mm2 450/750V halogen free, flame retardant power cable			none	XL-HFFR	http://www.nexans.com/eservice/Corporate-en/navigate_267718/Alsecure_Plus_Alsecure_Premium_Fire_resistant_cables.html	1C-H07Z-R 25	INDOOR		\N	\N	\N	\N	\N	\N
292	t	single conductor 35mm2 450/750V halogen free, flame retardant power cable			none	XL-HFFR	http://www.nexans.com/eservice/Corporate-en/navigate_267718/Alsecure_Plus_Alsecure_Premium_Fire_resistant_cables.html	1C-H07Z-R 35	INDOOR		\N	\N	\N	\N	\N	\N
293	t	single conductor 50mm2 450/750V halogen free, flame retardant power cable			none	XL-HFFR	http://www.nexans.com/eservice/Corporate-en/navigate_267718/Alsecure_Plus_Alsecure_Premium_Fire_resistant_cables.html	1C-H07Z-R 50	INDOOR		\N	\N	\N	\N	\N	\N
294	t	single conductor 70mm2 450/750V halogen free, flame retardant power cable			none	XL-HFFR	http://www.nexans.com/eservice/Corporate-en/navigate_267718/Alsecure_Plus_Alsecure_Premium_Fire_resistant_cables.html	1C-H07Z-R 70	INDOOR		\N	\N	\N	\N	\N	\N
295	t	single conductor 95mm2 450/750V halogen free, flame retardant power cable			none	XL-HFFR	http://www.nexans.com/eservice/Corporate-en/navigate_267718/Alsecure_Plus_Alsecure_Premium_Fire_resistant_cables.html	1C-H07Z-R 95	INDOOR		\N	\N	\N	\N	\N	\N
296	t	single conductor 120mm2 450/750V halogen free, flame retardant power cable			none	XL-HFFR	http://www.nexans.com/eservice/Corporate-en/navigate_267718/Alsecure_Plus_Alsecure_Premium_Fire_resistant_cables.html	1C-H07Z-R 120	INDOOR		\N	\N	\N	\N	\N	\N
297	t	1 Conductor 4/0 Locomotive cable			EPR	CPE	http://www.amercable.com	1C4/0DEL	INDOOR	Flexible cabling for Magnet Power	\N	\N	\N	2000	\N	\N
298	t	1 Conductor, 4 AWG bare copper, 21mm2, Outer Conductor - 30 AWG bare copper, 50 Ohm Coax			Solid PE	PVC	http://www.nexans.com/	1C4RG-219	INDOOR	RF	\N	\N	\N	11000	\N	\N
299	t	1 Conductor 777 Locomotive cable			EPR	CPE	https://www.anixter.com/en_uk/product-detail.PS-Flexible%2B%2F%2BRobotic%2BCable.5N-3131-CPE.html	1C777DEL	INDOOR	Flexible cabling for Magnet Power	\N	\N	\N	2000	\N	\N
300	t	16 AWG stranded (19x29) TC conductors, polyethylene insulation, twisted pair, overall Beldfoil® shield(100% coverage), 18 AWG stranded TC drain wire			PE	LSHF	http://www.belden.com/techdatas/english/8719NH.pdf	1P16STCHF	INDOOR	Control Cable, Analog  & Discrete	\N	\N	\N	300	\N	\N
301	t	1 Pair, 1,31mm2, 16 AWG, Twisted Shielded, Overall Foil, Drain Wire			PVC	FEP	http://www.digikey.com/product-detail/en/alpha-wire/5717%2F1601%20BK005/5717%2F1601%20BK005-ND/3713912	1P16THCP	INDOOR	Thermocouple Wire	\N	\N	\N	300	\N	\N
302	t	1 Pair, 1,31mm2, 16 AWG, stranded tinned copper, Overall Shield			XLPE	LSHF	https://www.anixter.com/en_us/products/Instrumentation-and-Thermocouple-Cable/p/2LS-1601POS	1P16OSTC6V	INDOOR		\N	\N	\N	600	\N	\N
303	t	1 Pair, 0,82mm2, 18 AWG, stranded tinned copper, overall  shielded			PVC	PE	http://www.alphawire.com/en/Products/Cable/Xtra-Guard-Performance-Cable/Xtra-Guard-3/35372	1P18OSTC	INDOOR	Control Cable	\N	\N	\N	300	\N	\N
304	t	2 twisted pair 18AWG 1mm2, stranded tinned copper, drain wire, overall shield			PVC	TPU	http://www.alphawire.com/en/Products/Cable/	2P18OSTC	INDOOR		\N	\N	\N	300	\N	\N
305	t	1 pair, 22 AWG Stranded (7x30), Tinned Copper, Twisted Pair, Overall 100% Beldfoil Shield, 22 AWG Stranded TC Drain Wire			PP	LSHF	http://www.belden.com/techdatas/metric/9451SB.pdf?ip=false	1P22OSTC3V	INDOOR	Audio, Control and Instrumentation Cable, LSZH	\N	\N	\N	300	\N	\N
306	t	4 twisted pair, 18 AWG 1mm2,  Screened highly flexible data transmission cable with PUR outer sheath , Extra-fine wire strand made of bare copper wires\nCore insulation: Based on Polyolefin\nTP structure\nNon-woven wrapping\nTinned-copper braiding\nOuter sheath made of special PUR compound\nOuter sheath colour: grey (RAL 7001)			Polyolefine	PUR	http://www.lappkabel.com	4PR18OTC3V	INDOOR	measuring, control and regulating circuits	\N	\N	\N	250	\N	\N
307	t	Fiber 2mm, coating diameter 3±0.15 mm,Attenuation ≤ 0.2 dB/m at 650 nm Operating Temperature Range: -55°C to+70°C			PE	PE	\N	2MFibSBF	INDOOR	RF	\N	\N	\N	\N	\N	\N
308	t	Numerical aperture NA (l = 587 nm): 0.64, Color temperature (normal light D 65) 5.475 K, Fiber diameter:70 μm, Bundle diameter: 2 mm Coating diameter: 3.6mm			PE	PE	\N	Multimode Glass Fibre cable	INDOOR	RF	\N	\N	\N	\N	\N	\N
309	t	DeviceNet, two data cores to a pair and screened with aluminium foil overall two power cores and drain wire, 1x2xAWG18/19 +\n1x2xAWG15/19			PP	PUR	http://www.nexans.nl/eservice/Netherlands-en/navigateproduct_540153911/1x2xAWG18_19_1x2xAWG15_19.html	DeviceNet Cable	INDOOR	data transmission and power supply	\N	\N	\N	\N	\N	\N
310	t	Industrial Ethernet/ Profinet cable, Pronet Type C PUR2x2xAWG22/7 2YH(ST)C11Y UL\nStyle 20236			PE	PUR	http://www.nexans-fi.com/UK/family/doc/en/industrialethernet.pdf	Ethernet/ Profinet , 2YH(ST)C11Y	INDOOR	Communication	\N	\N	\N	\N	\N	\N
311	t	1x2x1.0/2.55 2Y(ST)CY, bare copper, Aluminium foil + tinned copper braiding, 100 Ohm data transfer cable for xed laying. Max. 31.25 kBit/s			PE	PVC	http://www.nexans.com/eservice/SouthEastAsia-en/navigate_121089/Profinet_Cable.html	Profibus 2Y(ST)CY	INDOOR		\N	\N	\N	\N	\N	\N
312	t	Profinet, 2x2xAWG22/1, 0,32mm2, Shielded, overall braid (85% coverage)			PVC	PUR	http://www.farnell.com/datasheets/1319746.pdf	Profinet  Cat 5	INDOOR		\N	\N	\N	\N	\N	\N
313	t	UC900 SS27 Cat.7 S/FTP LSHF­FR, 2 cores twisted to a pair, wrapped with metallized polyester foil, solid plain copper conductor, tineed copper screen			PE	LSHF	http://www.prysmiangroup.com/en/business_markets/markets/multimedia/products/datacom/draka-uc300-u1500/	Cat.7 S/FTP LSHF­FR	INDOOR	Data cable ­Multimedia	\N	\N	\N	\N	\N	\N
314	t	UC400 SS23 Cat.6 S/FTP LSHF, 2 cores twisted to a pair, wrapped with metallized polyester foil, solid plain copper conductor, tineed copper screen			PE	LSHF	http://www.draka.nl/catalogue/uc400-ss23-cat-6-s-ftp-lshf/21431	Cat.6 S/FTP LSHF	INDOOR	Data cable ­Multimedia	\N	\N	\N	\N	\N	\N
315	t	78 Ohm Flexible RG-108 Twinax Cable Single Shielded			PE	PVC	http://www.pasternack.cn/images/ProductPDF/RG108A-U.pdf	1CRG-108	INDOOR		\N	\N	\N	\N	\N	\N
316	t	RG11/U 75 Ohm  Coaxial Cable, tinned copped braiding			PE	Polyolefine	http://www.nexans.com/Poland/2007/Coax.pdf	1CRG-11U	INDOOR		\N	\N	\N	\N	\N	\N
317	t	Coaxial and Triaxial FRNC-High Voltage Low Power Cables acc. to CERN and DESY Specifications			PE	FRNC	http://www.prysmiangroup.com/en/business_markets/markets/multimedia/downloads/datasheets/hf47e.pdf	2YC2YC(St)H	INDOOR	Vacuum	\N	\N	\N	\N	\N	\N
318	t	FXQJ EMC 1 kV 3X150			HFFR	XLPE	\N	FXQJ EMC 0,6/1 kV 3x150	INDOOR	Magnet Power	\N	\N	\N	1000	\N	\N
319	t	FXQJ EMC 1 kV 3X10			HFFR	XLPE	\N	FXQJ EMC 0,6/1 kV 3x10	INDOOR	Magnet Power	\N	\N	\N	1000	\N	\N
320	t	FXQJ EMC 1 kV 3X25			HFFR	XLPE	\N	FXQJ EMC 0,6/1 kV 3x25	INDOOR	Magnet Power	\N	\N	\N	1000	\N	\N
321	t	FXQJ EMC 1 kV 3X50			HFFR	XLPE	\N	FXQJ EMC 0,6/1 kV 3x50	INDOOR	Magnet Power	\N	\N	\N	1000	\N	\N
322	t	FXQJ EMC 1 kV 3X95			HFFR	XLPE	\N	FXQJ EMC 0,6/1 kV 3x95	INDOOR	Magnet Power	\N	\N	\N	1000	\N	\N
323	t	12 Pair, 18 AWG, unshielded, tray rated			PVC	PVC	http://www.belden.com/techdatas/metric/9741.pdf	12P18U1V	INDOOR	Control Cable Analog	\N	\N	\N	100	\N	\N
326	t	22 AWG stranded (7x30) tinned copper conductors, conductors cabled, PVC insulation, PVC Jacket.			PVC	PVC	http://www.belden.com/techdatas/metric/9421.pdf	8C22STC3V	INDOOR	Multi-Conductor - Audio, Control and Instrumentation	\N	\N	\N	300	\N	\N
327	t	22 AWG stranded (7x30) tinned copper conductors, conductors cabled, PVC insulation, PVC Jacket.			PVC	PVC	http://www.belden.com/techdatas/metric/8421.pdf	10C22STC3V	INDOOR	Multi-Conductor - Audio, Control and Instrumentation	\N	\N	\N	300	\N	\N
328	t	15 X 1 COND,24 (7/32) AWG TC, 24 (7/32) AWG TC, drain wire 24 (7/32) AWG TC, A/P/A Tape, 25% Overlap, Min.				TPE	http://www.alphawire.com/en/Products/Cable/Xtra-Guard-Performance-Cable/Xtra-Guard-4/45120_15	15C24S3V	INDOOR		\N	\N	\N	300	\N	\N
329	t	3 conductors, 0,2mm2, 24 AWG stranded (7x32) tinned copper conductors, semi-rigid PVC insulation, overall Beldfoil® shield, (100% coverage) + tinned copper braid shield (65% coverage), PVC jacket.			PVC	PVC	http://www.belden.com/techdatas/english/9608.pdf	3C24STC	INDOOR	Multi-Conductor - Computer Cable for EIA RS-232 Applications	\N	\N	\N	300	\N	\N
330	t	3 conductors, 0,12mm2, 26AWG stranded tinned copper, braid shield 85% coverage				PUR	http://www.farnell.com/datasheets/1934492.pdf	3C26STC6VHF	INDOOR		\N	\N	\N	600	\N	\N
332	t	4 conductors, 1mm2, Bare copper, fine wire conductors				LSHF	http://www.helukabel.com/en/home.html	4C1OSTCHF	INDOOR	Temperature Sensors	\N	\N	\N	300	\N	\N
333	t	4 conductors, 1,31mm2, 16 AWG, ceramifiable silicone insulation, Beldfoil® shield (100% coverage), stranded tinned copper drain wire, FRPE (low smoke non-halogen) jacket			Silicone	LSHF	http://www.belden.com/techdatas/metric/5222FZ.pdf	4C16OSBCHF	INDOOR	Beam Instrumentation	\N	\N	\N	300	\N	\N
334	t	3G 0.75 LNPE,  Connecting cable, Overal shield				LSHF	http://www.helukabel.com/en/home.html	3G0,75	INDOOR	Vacuum Power Cable	\N	\N	\N	300	\N	\N
335	t	3G 1.5 LNPE, Connecting cable, Overral shield				LSHF	\N	3G1,5	INDOOR	Vacuum Power Cable	\N	\N	\N	300	\N	\N
336	t	6G 1.5 NRPE (or 7G 1.5 NRPE), Connecting cable, Overal shield					\N	6G1,5	INDOOR	Vacuum control cable	\N	\N	\N	300	\N	\N
337	t	3 cored 6mm2 power cable black, brown, grey			XLPE	LSHF	http://www.cablel.com/	3x6 N2XCH	OUTDOOR	Power Cable	\N	\N	\N	1000	\N	\N
338	t	AXQJ LT is a XLPE-insulated, copper wire screened, HFFR sheathed 3-core, cable with circular aluminium conductors			HFFR	XLPE	http://www.nexans.se/eservice/Sweden-sv_SE/navigate_183516/AXQJ_LT_12_kV.html	AXQJ LT 12 kV 3x240/35	OUTDOOR	Compressors' Power	\N	\N	\N	12000	\N	\N
339	t	AXQJ LT is a XLPE-insulated, copper wire screened, HFFR sheathed 3-core, cable with circular aluminium conductors			HFFR	XLPE	http://www.nexans.se/eservice/Sweden-sv_SE/navigate_183516/AXQJ_LT_12_kV.html	AXQJ LT 12 kV 3x150/25	OUTDOOR	Compressors' Power	\N	\N	\N	12000	\N	\N
340	t	AXQJ is a halogen free, XLPE-insulated, HFFR-sheathed cable with sector, shaped, stranded aluminium conductors			HFFR	XLPE	http://www.nexans.de/eservice/Germany-en/navigate_291476/AXQJ_1_kV_4_conductor.html	AXQJ 1 kV 4X240/72	OUTDOOR	Power Distribution Feeder	\N	\N	\N	1000	\N	\N
341	t	AXQJ is a halogen free, XLPE-insulated, HFFR-sheathed cable with sector, shaped, stranded aluminium conductors			HFFR	XLPE	http://www.nexans.de/eservice/Germany-en/navigate_291476/AXQJ_1_kV_4_conductor.html	AXQJ 1 kV 4X50/15	OUTDOOR	Power Distribution Feeder	\N	\N	\N	1000	\N	\N
342	t	AXQJ is a halogen free, XLPE-insulated, HFFR-sheathed cable with sector, shaped, stranded aluminium conductors			HFFR	XLPE	http://www.nexans.de/eservice/Germany-en/navigate_291476/AXQJ_1_kV_4_conductor.html	AXQJ 1 kV 4X120/41	OUTDOOR	Power Distribution Feeder	\N	\N	\N	1000	\N	\N
343	t	AXQJ is a halogen free, XLPE-insulated, HFFR-sheathed cable with sector, shaped, stranded aluminium conductors			HFFR	XLPE	http://www.nexans.de/eservice/Germany-en/navigate_291476/AXQJ_1_kV_4_conductor.html	AXQJ 1 kV 4X150/41	OUTDOOR	Power Distribution Feeder	\N	\N	\N	1000	\N	\N
254	t	4 pairs, 0,52mm2, 20 AWG, color coded, overall shield, XLPE, PUR,PE insulation			XLPE	EPR	\N	4PR20OS	INDOOR		\N	\N	\N	300	\N	\N
345	t	16 pairs, 1mm2, 17 AWG, individual  aluminium  screen  for  each  conductor  pair  and  one  \r\ncommon aluminium screen, drain conductor, 16X2X1,0, halogen free		Cat. D	XLPE	PO	http://www.nexans.se/eservice/Sweden-en/pdf-product_540072665/FQAR_PIG_150_250_V_16X2X1_0.pdf	16P17IOTC250VHF	OUTDOOR	Instrumentation	\N	7.63000011	23.2999992	250	\N	\N
325	t	8 twisted pairs,  24 AWG stranded (41x40) bare copper conductor, polyolefin insulation, individually shielded with double\nserve “French Braid” (93% coverage), TC drain wire, numbered/color-coded PVC jackets, overall Black\nPVC jacket.			PO	PVC	http://www.belden.com/techdatas/metric/1908A.pdf	8P24ST3V	INDOOR	Multi-Conductor - FleXnake® Super-Flexible	\N	\N	\N	300	\N	\N
344	t	18 AWG stranded (16X30) tinned copper conductors,1mm2,  polyethylene insulation,single twisted pair, overall Beldfoil®\nshield (100% coverage), 20 AWG stranded tinned copper drain wire, LSZH jacket			PE	LSHF	http://www.tme.eu/se/Document/4d49e421ef28ea609cf337ed9e2041fd/8760NH.pdf	1P18OSCHF	INDOOR		\N	40.1809998	5.63899994	300	\N	\N
348	t	2 pairs, 2 x 2 x .75, 19AWG, stranded, annealed copper wire, drain wire, individual and overall shield,  low smoke, zero halogen, high frequency signals, Separating foil over overall shield, CDS tag: CBL_T	CDS tag: CBL_T		XLPE	LSHF	https://www.summit.com/industries/marine/Low-Smoke-Zero-Halogen/FM2XAAH-Individually-and-Overall-Shielded-Instrumentation-Cable	2P19IOSHF	INDOOR	cryo distribution line	\N	\N	12.6999998	250	\N	\N
349	t	2 conductors,0.75mm2, 19 AWG,  stranded tinned copper, XLPE Insulated, LSZH Sheathed & Overall Screened Instrumentation\r\nCable, tinned copper drain wire, RE-2X(St)H, 2×1×.75, 300/500V,\r\nflame retardant, halogen free, CDS tag:CBL_P	CDS tag:CBL_P		XLPE	LSHF	http://www.fireproof-cables.com/download/lszh/5308.pdf	2C19OSHF	UNKNOWN	cryo distribution line	\N	0.057	7.30000019	300	\N	\N
347	t	50 pairs, conductor cross section 18 AWG, 1mm2, XLPE Insulated, LSZH Sheathed, Individual and Overall Screened Instrumentation Cable, RE-2X(St)H Pimf, CDS tag:CBL_A	CDS tag: CBL_A		XLPE	LSHF	http://www.fireproof-cables.com/download/lszh/5308.pdf	50P18IOSHF	INDOOR	cryo distribution line	\N	2.3599999	42.5	300	\N	\N
331	t	3 conductors, Bare stranded copper  conductor, halogen  free core  insulation and  outer  sheath,  colour code in  accordance  with DIN  47100 3x0,34mm2, 22AWG				LSHF	http://lapplimited.lappgroup.com/products/online-catalogue/data-communication-systems/low-frequency-data-transmission-cables/single-halogen-free-cable/unitronic-lihch.html	3C0,34OSHF	INDOOR	Vacuum	\N	\N	\N	300	\N	\N
350	t	3 conductors, 21 AWG, conductor cross section 0,5 mm2, 3×1×0.5, stranded tinned copper, tinned drain wire, XLPE Insulated, LSZH Sheathed & Overall Screened Instrumentation cable, CDS tag: CBL_GS	CDS tag: CBL_GS		XLPE	LSHF	http://www.fireproof-cables.com/download/lszh/5308.pdf	3C21OSHF	INDOOR	cryo distribution line	\N	0.0590000004	7.30000019	300	\N	\N
351	t	4pairs, 21 AWG, 0,5mm2, 4 x 2 x .50, stranded annealed copper wire, Low smoke, halogen free, flame retardant, control and communication cable , tinned, stranded, copper drain wire below overall shield, CDS tag: CBL_G, CBL_P2	CDS tag: CBL_G, CBL_P2		XLPE	LSHF	https://www.summit.com/industries/marine/Low-Smoke-Zero-Halogen/FM2XAAH-Individually-and-Overall-Shielded-Instrumentation-Cable	4P21IOSLSHF	INDOOR	cryo distribution line	\N	\N	13.7159996	250	\N	\N
346	f	2 conductors,1,5mm2, 16AWG,  XLPE Insulated, LS ZH Sheathed & Overall Screened Instrumentation Cables, RE-2X(St)H, 2×1×1.5, 300/500V, flame retardant, halogen free, CDS tag:CBL_V	CDS tag:CBL_V 		XLPE 	LSHF	http://www.fireproof-cables.com/download/lszh/5308.pdf	2C16OCHF	INDOOR	cryo distribution line	\N	0.0780000016	8.30000019	300	\N	\N
352	t	2 conductors,1,5mm2, 16AWG,  XLPE Insulated, LS ZH Sheathed & Overall Screened Instrumentation Cables, RE-2X(St)H, 2×1×1.5, 300/500V, flame retardant, halogen free, CDS tag:CBL_V	CDS tag:CBL_V		XLPE	LSHF	http://www.fireproof-cables.com/	2C16OSHF	INDOOR	Cryo Distribution LIne	\N	0.0780000016	8.30000019	300	\N	\N
\.


--
-- Name: cabletype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('cabletype_id_seq', 352, true);


--
-- Data for Name: connector; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY connector (id, description, created, connectortype, modified, name, drawing, active) FROM stdin;
\.


--
-- Name: connector_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('connector_id_seq', 1, false);


--
-- Data for Name: displayview; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY displayview (id, created, description, entitytype, executed, owner) FROM stdin;
1	2016-04-25 10:52:58.827	testView	CABLE_TYPE	2016-04-25 10:53:10.5	sunilsah
2	2016-04-29 12:55:06.375	test	CABLE_TYPE	2016-04-29 12:56:14.084	ricardofernandes
3	2016-05-12 09:30:31.386	test	CABLE_TYPE	\N	evangeliavaena
4	2016-09-12 12:10:59.436	test	CABLE	\N	ricardofernandes
6	2016-09-20 12:45:09.401	my view	CABLE	\N	evangeliavaena
7	2016-09-20 12:45:30.456	lia	CABLE	\N	evangeliavaena
8	2016-09-20 12:52:19.137	contractor	CABLE	2016-09-20 12:54:04.898	evangeliavaena
5	2016-09-20 12:42:48.912	lia's view	CABLE	2016-09-20 12:56:19.92	evangeliavaena
\.


--
-- Data for Name: displayview_displayviewcolumn; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY displayview_displayviewcolumn (displayview_id, columns_id) FROM stdin;
1	1
2	3
2	2
3	4
4	5
5	7
5	8
5	9
5	10
5	11
5	12
5	13
5	14
5	15
6	16
7	17
8	19
8	20
8	21
8	18
\.


--
-- Name: displayview_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('displayview_id_seq', 9, true);


--
-- Data for Name: displayviewcolumn; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY displayviewcolumn (id, columnname, "position", displayview) FROM stdin;
1	Name/Code	0	\N
3	Name/Code	0	2
2	Description	1	\N
4	Name/Code	0	\N
5	Type	0	\N
7	Sy	0	5
8	Su	1	5
9	Cl	2	5
10	Number	3	5
11	Type	4	5
12	From Device A	5	5
13	To Device B	6	5
14	Sy	7	5
15	Connector Type B (wiring)	8	5
16	Type	0	\N
17	Type	0	\N
19	Sy	0	8
20	Number	1	8
21	Connector Type A (wiring)	2	8
18	Connector Type B (wiring)	3	\N
\.


--
-- Name: displayviewcolumn_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('displayviewcolumn_id_seq', 22, true);


--
-- Data for Name: endpoint; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY endpoint (id, building, device, drawing, label, rack, validity, connector_id) FROM stdin;
9		MBL-060CRM:EMR-Cav-010	\N			VALID	\N
10		MBL-060CRM:EMR-Cav-010	\N			VALID	\N
11		MBL-060CRM:EMR-Cav-010	\N			VALID	\N
12		MBL-060CRM:EMR-Cav-010	\N			VALID	\N
13	FEB	LNS-LEBT-010:VAC-PLC-11111	\N		FEB-030ROW:CNPW-U-004	VALID	\N
14	FEB	LNS-LEBT-010:VAC-PLC-11111	\N		FEB-030ROW:CNPW-U-003	VALID	\N
15		LabS-BI:PBI-NPM-001	\N	Vertical setup: Camera readout		VALID	\N
16		LabS-BI:CNPW-U-001	\N	PBI Lab room,		VALID	\N
39		SPK-020LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
47		SPK-030LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
23		SPK-010LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
57		SPK-040LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
25		SPK-010LWU:PBI-BPM-002	\N	BPM signal S		VALID	\N
21		SPK-010LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
29		SPK-010LWU:PBI-BPM-002	\N	BPM signal N		VALID	\N
27		SPK-010LWU:PBI-BPM-002	\N	BPM signal U		VALID	\N
31		SPK-010LWU:PBI-BPM-002	\N	BPM signal D		VALID	\N
35		SPK-020LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
18		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
20		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
37		SPK-020LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
41		SPK-020LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
43		SPK-030LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
45		SPK-030LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
22		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
24		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
26		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
33		SPK-010LWU:PBI-BPM-001	\N	BPM phase reference tap 12		VALID	\N
51		SPK-020LWU:PBI-BPM-001	\N	BPM phase reference tap 13		VALID	\N
61		SPK-040LWU:PBI-BPM-001	\N	BPM phase reference tap 14		VALID	\N
28		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
30		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
32		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
34		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
36		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
38		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
40		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
42		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
44		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
46		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
48		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
50		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
52		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
54		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
56		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
49		SPK-030LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
53		SPK-040LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
55		SPK-040LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
59		SPK-040LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
17		SPK-010LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
19		SPK-010LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
83		SPK-020CRM:PBI-BLM-001	\N	ICBLM signal		VALID	\N
81		SPK-020LWU:PBI-BLM-003	\N	ICBLM signal		VALID	\N
87		SPK-030LWU:PBI-BLM-001	\N	ICBLM signal		VALID	\N
89		SPK-030LWU:PBI-BLM-002	\N	ICBLM signal		VALID	\N
91		SPK-030LWU:PBI-BLM-003	\N	ICBLM signal		VALID	\N
93		SPK-030CRM:PBI-BLM-001	\N	ICBLM signal		VALID	\N
97		SPK-040LWU:PBI-BLM-001	\N	ICBLM signal		VALID	\N
99		SPK-040LWU:PBI-BLM-002	\N	ICBLM signal		VALID	\N
101		SPK-040LWU:PBI-BLM-003	\N	ICBLM signal		VALID	\N
103		SPK-040CRM:PBI-BLM-001	\N	ICBLM signal		VALID	\N
105		DTL-050:PBI-BLM-002	\N	nBLM signal		VALID	\N
107		DTL-050:PBI-BLM-003	\N	nBLM signal		VALID	\N
109		SPK-010LWU:PBI-BLM-004	\N	nBLM signal		VALID	\N
111		SPK-020LWU:PBI-BLM-004	\N	nBLM signal		VALID	\N
113		SPK-030LWU:PBI-BLM-004	\N	nBLM signal		VALID	\N
115		SPK-040LWU:PBI-BLM-004	\N	nBLM signal		VALID	\N
82		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
84		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
86		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
88		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
90		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
92		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
94		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
96		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
98		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
100		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
102		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
104		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
106		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
108		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
110		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
112		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
114		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
116		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
118		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
120		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
122		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
124		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
126		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
128		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
130		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
136		SPK-020ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
140		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
175		SPK-020LWU:PBI-NPM-001	\N	NPM 2D sensor ribbon		VALID	\N
142		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
144		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
148		SPK-020ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
152		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
154		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
156		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
162		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
164		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
166		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
168		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
170		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
176		SPK-020ROW:CNPW-U-018	\N			VALID	\N
70		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
58		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
60		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
62		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
63		DTL-050:PBI-BLM-001	\N	ICBLM signal		VALID	\N
64		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
71		SPK-010LWU:PBI-BLM-003	\N	ICBLM signal		VALID	\N
72		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
73		SPK-010CRM:PBI-BLM-001	\N	ICBLM signal		VALID	\N
74		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
75		SPK-020LWU:PBI-BLM-001	\N	ICBLM HV (1.5 kV, RG-59 Red)		VALID	\N
76		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
77		SPK-020LWU:PBI-BLM-001	\N	ICBLM signal		VALID	\N
85		SPK-030LWU:PBI-BLM-001	\N	ICBLM HV (1.5 kV, RG-59 Red)		VALID	\N
95		SPK-040LWU:PBI-BLM-001	\N	ICBLM HV (1.5 kV, RG-59 Red)		VALID	\N
117		DTL-050:PBI-BLM-002	\N	nBLM HV (RG-59 Red)		VALID	\N
119		DTL-050:PBI-BLM-003	\N	nBLM HV (RG-59 Red)		VALID	\N
121		SPK-010LWU:PBI-BLM-004	\N	nBLM HV (RG-59 Red)		VALID	\N
123		SPK-020LWU:PBI-BLM-004	\N	nBLM HV (RG-59 Red)		VALID	\N
125		SPK-030LWU:PBI-BLM-004	\N	nBLM HV (RG-59 Red)		VALID	\N
127		SPK-040LWU:PBI-BLM-004	\N	nBLM HV (RG-59 Red)		VALID	\N
129		DTL-050:PBI-BCM-001	\N	BCM signal		VALID	\N
131		DTL-050:PBI-BCM-001	\N	BCM calibration		VALID	\N
133		SPK-010LWU:PBI-IBS-001	\N	IBS motor control (TP AWG19)		VALID	\N
135		SPK-020LWU:PBI-WS-001	\N	WS Signal W6 (TP AWG20)		VALID	\N
137		SPK-020LWU:PBI-WS-001	\N	WS Signal W7 (TP AWG20)		VALID	\N
139		SPK-020LWU:PBI-WS-001	\N	WS motor H control W5 (TP AWG19)		VALID	\N
141		SPK-020LWU:PBI-WS-001	\N	WS motor H encoder W5a		VALID	\N
143		SPK-020LWU:PBI-WS-001	\N	WS motor V control W5 (TP AWG19)		VALID	\N
145		SPK-020LWU:PBI-WS-001	\N	WS motor V encoder W5a		VALID	\N
147		SPK-040LWU:PBI-WS-001	\N	WS Signal W6 (TP AWG20)		VALID	\N
149		SPK-040LWU:PBI-WS-001	\N	WS Signal W7 (TP AWG20)		VALID	\N
151		SPK-040LWU:PBI-WS-001	\N	WS motor H control W5 (TP AWG19)		VALID	\N
153		SPK-040LWU:PBI-WS-001	\N	WS motor H encoder W5a		VALID	\N
155		SPK-040LWU:PBI-WS-001	\N	WS motor V control W5 (TP AWG19)		VALID	\N
157		SPK-040LWU:PBI-WS-001	\N	WS motor V encoder W5a		VALID	\N
159		SPK-020LWU:PBI-NPM-001	\N	NPM HV 1 (100kV)		VALID	\N
161		SPK-020LWU:PBI-NPM-001	\N	NPM HV 2 (100kV)		VALID	\N
163		SPK-020LWU:PBI-NPM-001	\N	NPM MCP signal 1		VALID	\N
165		SPK-020LWU:PBI-NPM-001	\N	NPM MCP signal 2		VALID	\N
167		SPK-020LWU:PBI-NPM-001	\N	NPM MCP HV 1 (5kV)		VALID	\N
169		SPK-020LWU:PBI-NPM-001	\N	NPM MCP HV 2 (5kV)		VALID	\N
171		SPK-020LWU:PBI-NPM-001	\N	NPM camera interface 1		VALID	\N
173		SPK-020LWU:PBI-NPM-001	\N	NPM camera interface 2		VALID	\N
65		DTL-050:PBI-BLM-001	\N	ICBLM HV (1.5 kV, RG-59 Red)		VALID	\N
66		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
67		SPK-010LWU:PBI-BLM-001	\N	ICBLM signal		VALID	\N
68		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
69		SPK-010LWU:PBI-BLM-002	\N	ICBLM signal		VALID	\N
78		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
79		SPK-020LWU:PBI-BLM-002	\N	ICBLM signal		VALID	\N
80		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
132		SPK-010ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
134		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
138		SPK-020ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
146		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
150		SPK-020ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
158		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
160		SPK-020ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
172		SPK-030ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
174		SPK-020ROW:CNPW-U-018	\N	ACC_STUB_120		VALID	\N
177		MEBT-010:PBI-BPM-001	\N	BPM signal S		VALID	\N
178		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
179		MEBT-010:PBI-BPM-001	\N	BPM signal U		VALID	\N
180		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
181		MEBT-010:PBI-BPM-001	\N	BPM signal N		VALID	\N
182		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
183		MEBT-010:PBI-BPM-001	\N	BPM signal D		VALID	\N
184		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
185		MEBT-010:PBI-BPM-002	\N	BPM signal S		VALID	\N
186		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
187		MEBT-010:PBI-BPM-002	\N	BPM signal U		VALID	\N
188		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
189		MEBT-010:PBI-BPM-002	\N	BPM signal N		VALID	\N
190		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
191		MEBT-010:PBI-BPM-002	\N	BPM signal D		VALID	\N
192		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
193		MEBT-010:PBI-BPM-003	\N	BPM signal S		VALID	\N
194		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
195		MEBT-010:PBI-BPM-003	\N	BPM signal U		VALID	\N
196		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
197		MEBT-010:PBI-BPM-003	\N	BPM signal N		VALID	\N
198		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
199		MEBT-010:PBI-BPM-003	\N	BPM signal D		VALID	\N
200		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
201		MEBT-010:PBI-BPM-005	\N	BPM signal S		VALID	\N
202		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
203		MEBT-010:PBI-BPM-005	\N	BPM signal U		VALID	\N
204		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
205		MEBT-010:PBI-BPM-005	\N	BPM signal N		VALID	\N
206		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
207		MEBT-010:PBI-BPM-005	\N	BPM signal D		VALID	\N
208		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
209		MEBT-010:PBI-BPM-006	\N	BPM signal S		VALID	\N
210		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
211		MEBT-010:PBI-BPM-006	\N	BPM signal U		VALID	\N
212		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
213		MEBT-010:PBI-BPM-006	\N	BPM signal N		VALID	\N
214		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
215		MEBT-010:PBI-BPM-006	\N	BPM signal D		VALID	\N
216		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
217		MEBT-010:PBI-BPM-007	\N	BPM signal S		VALID	\N
218		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
219		MEBT-010:PBI-BPM-007	\N	BPM signal U		VALID	\N
220		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
221		MEBT-010:PBI-BPM-007	\N	BPM signal N		VALID	\N
222		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
223		MEBT-010:PBI-BPM-007	\N	BPM signal D		VALID	\N
224		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
225		MEBT-010:PBI-BPM-008	\N	BPM signal S		VALID	\N
226		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
227		MEBT-010:PBI-BPM-008	\N	BPM signal U		VALID	\N
228		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
229		MEBT-010:PBI-BPM-008	\N	BPM signal N		VALID	\N
230		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
231		MEBT-010:PBI-BPM-008	\N	BPM signal D		VALID	\N
232		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
233		DTL-010:PBI-BPM-001	\N	BPM signal S		VALID	\N
234		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
235		DTL-010:PBI-BPM-001	\N	BPM signal U		VALID	\N
236		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
237		DTL-010:PBI-BPM-001	\N	BPM signal N		VALID	\N
238		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
239		DTL-010:PBI-BPM-001	\N	BPM signal D		VALID	\N
240		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
241		DTL-010:PBI-BPM-002	\N	BPM signal S		VALID	\N
242		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
243		DTL-010:PBI-BPM-002	\N	BPM signal U		VALID	\N
244		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
245		DTL-010:PBI-BPM-002	\N	BPM signal N		VALID	\N
246		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
247		DTL-010:PBI-BPM-002	\N	BPM signal D		VALID	\N
248		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
249		DTL-010:PBI-BPM-003	\N	BPM signal S		VALID	\N
250		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
251		DTL-010:PBI-BPM-003	\N	BPM signal U		VALID	\N
252		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
253		DTL-010:PBI-BPM-003	\N	BPM signal N		VALID	\N
254		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
255		DTL-010:PBI-BPM-003	\N	BPM signal D		VALID	\N
256		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
257		DTL-010:PBI-BPM-004	\N	BPM signal S		VALID	\N
258		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
259		DTL-010:PBI-BPM-004	\N	BPM signal U		VALID	\N
260		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
261		DTL-010:PBI-BPM-004	\N	BPM signal N		VALID	\N
262		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
263		DTL-010:PBI-BPM-004	\N	BPM signal D		VALID	\N
264		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
265		DTL-010:PBI-BPM-005	\N	BPM signal S		VALID	\N
266		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
267		DTL-010:PBI-BPM-005	\N	BPM signal U		VALID	\N
268		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
269		DTL-010:PBI-BPM-005	\N	BPM signal N		VALID	\N
270		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
271		DTL-010:PBI-BPM-005	\N	BPM signal D		VALID	\N
272		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
273		DTL-010:PBI-BPM-006	\N	BPM signal S		VALID	\N
274		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
275		DTL-010:PBI-BPM-006	\N	BPM signal U		VALID	\N
276		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
277		DTL-010:PBI-BPM-006	\N	BPM signal N		VALID	\N
278		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
279		DTL-010:PBI-BPM-006	\N	BPM signal D		VALID	\N
280		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
281		DTL-020:PBI-BPM-001	\N	BPM signal S		VALID	\N
282		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
283		DTL-020:PBI-BPM-001	\N	BPM signal U		VALID	\N
284		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
285		DTL-020:PBI-BPM-001	\N	BPM signal N		VALID	\N
286		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
287		DTL-020:PBI-BPM-001	\N	BPM signal D		VALID	\N
288		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
289		DTL-020:PBI-BPM-002	\N	BPM signal S		VALID	\N
290		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
291		DTL-020:PBI-BPM-002	\N	BPM signal U		VALID	\N
292		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
293		DTL-020:PBI-BPM-002	\N	BPM signal N		VALID	\N
294		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
295		DTL-020:PBI-BPM-002	\N	BPM signal D		VALID	\N
296		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
297		DTL-020:PBI-BPM-003	\N	BPM signal S		VALID	\N
298		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
299		DTL-020:PBI-BPM-003	\N	BPM signal U		VALID	\N
300		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
301		DTL-020:PBI-BPM-003	\N	BPM signal N		VALID	\N
302		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
303		DTL-020:PBI-BPM-003	\N	BPM signal D		VALID	\N
304		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
305		DTL-030:PBI-BPM-001	\N	BPM signal S		VALID	\N
306		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
307		DTL-030:PBI-BPM-001	\N	BPM signal U		VALID	\N
308		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
309		DTL-030:PBI-BPM-001	\N	BPM signal N		VALID	\N
310		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
311		DTL-030:PBI-BPM-001	\N	BPM signal D		VALID	\N
312		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
313		DTL-030:PBI-BPM-002	\N	BPM signal S		VALID	\N
314		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
315		DTL-030:PBI-BPM-002	\N	BPM signal U		VALID	\N
316		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
317		DTL-030:PBI-BPM-002	\N	BPM signal N		VALID	\N
318		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
319		DTL-030:PBI-BPM-002	\N	BPM signal D		VALID	\N
320		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
321		DTL-040:PBI-BPM-001	\N	BPM signal S		VALID	\N
322		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
323		DTL-040:PBI-BPM-001	\N	BPM signal U		VALID	\N
324		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
325		DTL-040:PBI-BPM-001	\N	BPM signal N		VALID	\N
326		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
327		DTL-040:PBI-BPM-001	\N	BPM signal D		VALID	\N
328		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
329		DTL-040:PBI-BPM-002	\N	BPM signal S		VALID	\N
330		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
331		DTL-040:PBI-BPM-002	\N	BPM signal U		VALID	\N
332		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
333		DTL-040:PBI-BPM-002	\N	BPM signal N		VALID	\N
334		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
335		DTL-040:PBI-BPM-002	\N	BPM signal D		VALID	\N
336		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
337		DTL-050:PBI-BPM-001	\N	BPM signal S		VALID	\N
338		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
339		DTL-050:PBI-BPM-001	\N	BPM signal U		VALID	\N
340		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
341		DTL-050:PBI-BPM-001	\N	BPM signal N		VALID	\N
342		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
343		DTL-050:PBI-BPM-001	\N	BPM signal D		VALID	\N
344		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
345		DTL-050:PBI-BPM-002	\N	BPM signal S		VALID	\N
346		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
347		DTL-050:PBI-BPM-002	\N	BPM signal U		VALID	\N
348		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
349		DTL-050:PBI-BPM-002	\N	BPM signal N		VALID	\N
350		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
351		DTL-050:PBI-BPM-002	\N	BPM signal D		VALID	\N
352		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
353		SPK-050LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
354		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
355		SPK-050LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
356		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
357		SPK-050LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
358		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
359		SPK-050LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
360		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
361		SPK-060LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
362		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
363		SPK-060LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
364		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
365		SPK-060LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
366		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
367		SPK-060LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
368		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
369		SPK-070LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
370		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
371		SPK-070LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
372		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
373		SPK-070LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
374		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
375		SPK-070LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
376		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
377		SPK-080LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
378		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
379		SPK-080LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
380		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
381		SPK-080LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
382		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
383		SPK-080LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
384		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
385		SPK-090LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
386		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
387		SPK-090LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
388		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
389		SPK-090LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
390		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
391		SPK-090LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
392		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
393		SPK-100LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
394		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
395		SPK-100LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
396		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
397		SPK-100LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
398		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
399		SPK-100LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
400		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
401		SPK-110LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
402		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
403		SPK-110LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
404		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
405		SPK-110LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
406		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
407		SPK-110LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
408		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
409		SPK-120LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
410		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
411		SPK-120LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
412		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
413		SPK-120LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
414		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
415		SPK-120LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
416		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
417		SPK-130LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
418		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
419		SPK-130LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
420		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
421		SPK-130LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
422		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
423		SPK-130LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
424		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
425		MBL-010LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
426		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
427		MBL-010LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
428		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
429		MBL-010LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
430		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
431		MBL-010LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
432		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
433		MBL-020LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
434		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
435		MBL-020LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
436		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
437		MBL-020LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
438		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
439		MBL-020LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
440		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
441		MBL-030LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
442		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
443		MBL-030LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
444		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
445		MBL-030LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
446		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
447		MBL-030LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
448		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
449		MBL-040LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
450		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
451		MBL-040LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
452		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
453		MBL-040LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
454		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
455		MBL-040LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
456		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
457		MBL-050LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
458		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
459		MBL-050LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
460		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
461		MBL-050LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
462		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
463		MBL-050LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
464		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
465		MBL-060LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
466		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
467		MBL-060LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
468		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
469		MBL-060LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
470		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
471		MBL-060LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
472		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
473		MBL-070LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
474		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
475		MBL-070LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
476		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
477		MBL-070LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
478		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
479		MBL-070LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
480		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
481		MBL-080LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
482		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
483		MBL-080LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
484		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
485		MBL-080LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
486		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
487		MBL-080LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
488		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
489		MBL-090LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
490		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
491		MBL-090LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
492		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
493		MBL-090LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
494		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
495		MBL-090LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
496		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
497		HBL-010LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
498		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
499		HBL-010LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
500		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
501		HBL-010LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
502		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
503		HBL-010LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
504		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
505		HBL-020LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
506		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
507		HBL-020LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
508		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
509		HBL-020LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
510		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
511		HBL-020LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
512		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
513		HBL-030LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
514		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
515		HBL-030LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
516		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
517		HBL-030LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
518		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
519		HBL-030LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
520		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
521		HBL-040LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
522		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
523		HBL-040LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
524		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
525		HBL-040LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
526		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
527		HBL-040LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
528		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
529		HBL-050LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
530		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
531		HBL-050LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
532		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
533		HBL-050LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
534		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
535		HBL-050LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
536		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
537		HBL-060LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
538		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
539		HBL-060LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
540		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
541		HBL-060LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
542		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
543		HBL-060LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
544		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
545		HBL-070LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
546		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
547		HBL-070LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
548		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
549		HBL-070LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
550		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
551		HBL-070LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
552		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
553		HBL-080LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
554		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
555		HBL-080LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
556		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
557		HBL-080LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
558		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
559		HBL-080LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
560		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
561		HBL-090LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
562		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
563		HBL-090LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
564		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
565		HBL-090LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
566		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
567		HBL-090LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
568		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
569		HBL-100LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
570		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
571		HBL-100LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
572		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
573		HBL-100LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
574		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
575		HBL-100LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
576		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
577		HBL-110LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
578		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
579		HBL-110LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
580		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
581		HBL-110LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
582		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
583		HBL-110LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
584		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
585		HBL-120LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
586		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
587		HBL-120LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
588		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
589		HBL-120LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
590		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
591		HBL-120LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
592		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
593		HBL-130LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
594		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
595		HBL-130LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
596		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
597		HBL-130LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
598		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
599		HBL-130LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
600		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
601		HBL-140LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
602		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
603		HBL-140LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
604		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
605		HBL-140LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
606		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
607		HBL-140LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
608		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
609		HBL-150LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
610		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
611		HBL-150LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
612		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
613		HBL-150LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
614		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
615		HBL-150LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
616		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
617		HBL-160LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
618		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
619		HBL-160LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
620		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
621		HBL-160LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
622		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
623		HBL-160LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
624		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
625		HBL-170LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
626		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
627		HBL-170LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
628		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
629		HBL-170LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
630		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
631		HBL-170LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
632		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
633		HBL-180LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
634		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
635		HBL-180LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
636		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
637		HBL-180LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
638		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
639		HBL-180LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
640		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
641		HBL-190LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
642		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
643		HBL-190LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
644		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
645		HBL-190LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
646		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
647		HBL-190LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
648		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
649		HBL-200LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
650		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
651		HBL-200LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
652		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
653		HBL-200LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
654		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
655		HBL-200LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
656		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
657		HBL-210LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
658		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
659		HBL-210LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
660		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
661		HBL-210LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
662		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
663		HBL-210LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
664		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
665		HEBT-010LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
666		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
667		HEBT-010LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
668		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
669		HEBT-010LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
670		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
671		HEBT-010LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
672		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
673		HEBT-020LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
674		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
675		HEBT-020LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
676		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
677		HEBT-020LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
678		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
679		HEBT-020LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
680		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
681		HEBT-030LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
682		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
683		HEBT-030LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
684		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
685		HEBT-030LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
686		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
687		HEBT-030LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
688		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
689		HEBT-040LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
690		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
691		HEBT-040LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
692		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
693		HEBT-040LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
694		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
695		HEBT-040LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
696		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
697		HEBT-050LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
698		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
699		HEBT-050LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
700		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
701		HEBT-050LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
702		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
703		HEBT-050LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
704		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
705		HEBT-060LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
706		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
707		HEBT-060LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
708		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
709		HEBT-060LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
710		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
711		HEBT-060LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
712		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
713		HEBT-070LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
714		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
715		HEBT-070LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
716		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
717		HEBT-070LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
718		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
719		HEBT-070LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
720		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
721		HEBT-080LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
722		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
723		HEBT-080LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
724		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
725		HEBT-080LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
726		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
727		HEBT-080LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
728		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
729		HEBT-090LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
730		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
731		HEBT-090LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
732		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
733		HEBT-090LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
734		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
735		HEBT-090LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
736		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
737		HEBT-100LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
738		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
739		HEBT-100LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
740		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
741		HEBT-100LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
742		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
743		HEBT-100LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
744		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
745		HEBT-110LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
746		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
747		HEBT-110LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
748		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
749		HEBT-110LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
750		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
751		HEBT-110LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
752		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
753		HEBT-120LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
754		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
755		HEBT-120LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
756		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
757		HEBT-120LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
758		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
759		HEBT-120LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
760		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
761		HEBT-130LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
762		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
763		HEBT-130LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
764		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
765		HEBT-130LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
766		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
767		HEBT-130LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
768		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
769		HEBT-140LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
770		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
771		HEBT-140LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
772		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
773		HEBT-140LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
774		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
775		HEBT-140LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
776		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
777		HEBT-150LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
778		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
779		HEBT-150LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
780		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
781		HEBT-150LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
782		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
783		HEBT-150LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
784		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
785		HEBT-160LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
786		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
787		HEBT-160LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
788		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
789		HEBT-160LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
790		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
791		HEBT-160LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
792		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
793		A2T-010LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
794		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
795		A2T-010LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
796		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
797		A2T-010LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
798		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
799		A2T-010LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
800		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
801		A2T-020LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
802		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
803		A2T-020LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
804		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
805		A2T-020LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
806		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
807		A2T-020LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
808		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
809		DMPL-010QC:PBI-BPM-001	\N	BPM signal S		VALID	\N
810		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
811		DMPL-010QC:PBI-BPM-001	\N	BPM signal U		VALID	\N
812		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
813		DMPL-010QC:PBI-BPM-001	\N	BPM signal N		VALID	\N
814		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
815		DMPL-010QC:PBI-BPM-001	\N	BPM signal D		VALID	\N
816		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
817		DMPL-020QC:PBI-BPM-001	\N	BPM signal S		VALID	\N
818		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
819		DMPL-020QC:PBI-BPM-001	\N	BPM signal U		VALID	\N
820		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
821		DMPL-020QC:PBI-BPM-001	\N	BPM signal N		VALID	\N
822		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
823		DMPL-020QC:PBI-BPM-001	\N	BPM signal D		VALID	\N
824		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
825		DMPL-030QC:PBI-BPM-001	\N	BPM signal S		VALID	\N
826		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
827		DMPL-030QC:PBI-BPM-001	\N	BPM signal U		VALID	\N
828		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
829		DMPL-030QC:PBI-BPM-001	\N	BPM signal N		VALID	\N
830		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
831		DMPL-030QC:PBI-BPM-001	\N	BPM signal D		VALID	\N
832		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
833		DMPL-050DRF:PBI-BPM-001	\N	BPM signal S		VALID	\N
834		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
835		DMPL-050DRF:PBI-BPM-001	\N	BPM signal U		VALID	\N
836		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
837		DMPL-050DRF:PBI-BPM-001	\N	BPM signal N		VALID	\N
838		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
839		DMPL-050DRF:PBI-BPM-001	\N	BPM signal D		VALID	\N
840		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
841		MEBT-010:PBI-BPM-001	\N	BPM phase reference tap 01		VALID	\N
842		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
843		MEBT-010:PBI-BPM-003	\N	BPM phase reference tap 02		VALID	\N
844		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
845		MEBT-010:PBI-BPM-006	\N	BPM phase reference tap 03		VALID	\N
846		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
847		MEBT-010:PBI-BPM-008	\N	BPM phase reference tap 04		VALID	\N
848		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
849		DTL-010:PBI-BPM-002	\N	BPM phase reference tap 05		VALID	\N
850		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
851		DTL-010:PBI-BPM-004	\N	BPM phase reference tap 06		VALID	\N
852		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
853		DTL-010:PBI-BPM-006	\N	BPM phase reference tap 07		VALID	\N
854		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
855		DTL-020:PBI-BPM-002	\N	BPM phase reference tap 08		VALID	\N
856		FEB-050ROW:CNPW-U-001	\N	FEB		VALID	\N
857		DTL-030:PBI-BPM-001	\N	BPM phase reference tap 09		VALID	\N
858		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
859		DTL-040:PBI-BPM-001	\N	BPM phase reference tap 10		VALID	\N
860		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
861		DTL-050:PBI-BPM-001	\N	BPM phase reference tap 11		VALID	\N
862		DTL-030ROW:CNPW-U-018	\N	ACC_STUB_110		VALID	\N
863		SPK-060LWU:PBI-BPM-001	\N	BPM phase reference tap 15		VALID	\N
864		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
865		SPK-080LWU:PBI-BPM-001	\N	BPM phase reference tap 16		VALID	\N
866		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_130		VALID	\N
867		SPK-100LWU:PBI-BPM-001	\N	BPM phase reference tap 17		VALID	\N
868		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
869		SPK-120LWU:PBI-BPM-001	\N	BPM phase reference tap 18		VALID	\N
870		SPK-040ROW:CNPW-U-018	\N	ACC_STUB_140		VALID	\N
871		MBL-010LWU:PBI-BPM-001	\N	BPM phase reference tap 19		VALID	\N
872		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_150		VALID	\N
873		MBL-030LWU:PBI-BPM-001	\N	BPM phase reference tap 20		VALID	\N
874		MBL-010ROW:CNPW-U-018	\N	ACC_STUB_160		VALID	\N
875		MBL-050LWU:PBI-BPM-001	\N	BPM phase reference tap 21		VALID	\N
876		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_170		VALID	\N
877		MBL-070LWU:PBI-BPM-001	\N	BPM phase reference tap 22		VALID	\N
878		MBL-050ROW:CNPW-U-018	\N	ACC_STUB_180		VALID	\N
879		MBL-090LWU:PBI-BPM-001	\N	BPM phase reference tap 23		VALID	\N
880		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_190		VALID	\N
881		HBL-020LWU:PBI-BPM-001	\N	BPM phase reference tap 24		VALID	\N
882		MBL-090ROW:CNPW-U-018	\N	ACC_STUB_200		VALID	\N
883		HBL-040LWU:PBI-BPM-001	\N	BPM phase reference tap 25		VALID	\N
884		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_210		VALID	\N
885		HBL-060LWU:PBI-BPM-001	\N	BPM phase reference tap 26		VALID	\N
886		HBL-040ROW:CNPW-U-016	\N	ACC_STUB_220		VALID	\N
887		HBL-080LWU:PBI-BPM-001	\N	BPM phase reference tap 27		VALID	\N
888		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_230		VALID	\N
889		HBL-100LWU:PBI-BPM-001	\N	BPM phase reference tap 28		VALID	\N
890		HBL-090ROW:CNPW-U-018	\N	ACC_STUB_240		VALID	\N
891		HBL-120LWU:PBI-BPM-001	\N	BPM phase reference tap 29		VALID	\N
892		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_250		VALID	\N
893		HBL-140LWU:PBI-BPM-001	\N	BPM phase reference tap 30		VALID	\N
894		HBL-120ROW:CNPW-U-018	\N	ACC_STUB_260		VALID	\N
895		HBL-160LWU:PBI-BPM-001	\N	BPM phase reference tap 31		VALID	\N
896		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_270		VALID	\N
897		HBL-180LWU:PBI-BPM-001	\N	BPM phase reference tap 32		VALID	\N
898		HBL-160ROW:CNPW-U-018	\N	ACC_STUB_280		VALID	\N
899		HBL-200LWU:PBI-BPM-001	\N	BPM phase reference tap 33		VALID	\N
900		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_290		VALID	\N
901		HBL-210LWU:PBI-BPM-001	\N	BPM phase reference tap 34		VALID	\N
902		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
903		HEBT-020LWU:PBI-BPM-001	\N	BPM phase reference tap 35		VALID	\N
904		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
905		HEBT-040LWU:PBI-BPM-001	\N	BPM phase reference tap 36		VALID	\N
906		HBL-210ROW:CNPW-U-018	\N	ACC_STUB_300		VALID	\N
907		HEBT-060LWU:PBI-BPM-001	\N	BPM phase reference tap 37		VALID	\N
908		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
909		HEBT-080LWU:PBI-BPM-001	\N	BPM phase reference tap 38		VALID	\N
910		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
911		HEBT-100LWU:PBI-BPM-001	\N	BPM phase reference tap 39		VALID	\N
912		HEBT-010ROW:CNPW-U-012	\N	ACC_STUB_340		VALID	\N
913		HEBT-120LWU:PBI-BPM-001	\N	BPM phase reference tap 40		VALID	\N
914		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
915		HEBT-140LWU:PBI-BPM-001	\N	BPM phase reference tap 41		VALID	\N
916		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
917		HEBT-160LWU:PBI-BPM-001	\N	BPM phase reference tap 42		VALID	\N
918		HEBT-020ROW:CNPW-U-005	\N	ACC_STUB_360		VALID	\N
919		DMPL-020QC:PBI-BPM-001	\N	BPM phase reference tap 50		VALID	\N
920		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
921		DMPL-050DRF:PBI-BPM-001	\N	BPM phase reference tap 51		VALID	\N
922		HEBT-020ROW:CNPW-U-005	\N	DMPL		VALID	\N
923		A2T-030LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
924		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
925		A2T-030LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
926		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
927		A2T-030LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
928		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
929		A2T-030LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
930		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
931		A2T-040LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
932		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
933		A2T-040LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
934		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
935		A2T-040LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
936		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
937		A2T-040LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
938		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
939		A2T-050LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
940		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
941		A2T-050LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
942		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
943		A2T-050LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
944		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
945		A2T-050LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
946		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
947		A2T-060LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
948		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
949		A2T-060LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
950		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
951		A2T-060LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
952		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
953		A2T-060LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
954		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
955		A2T-080LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
956		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
957		A2T-080LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
958		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
959		A2T-080LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
960		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
961		A2T-080LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
962		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
963		A2T-080LWU:PBI-BPM-002	\N	BPM signal S		VALID	\N
964		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
965		A2T-080LWU:PBI-BPM-002	\N	BPM signal U		VALID	\N
966		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
967		A2T-080LWU:PBI-BPM-002	\N	BPM signal N		VALID	\N
968		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
969		A2T-080LWU:PBI-BPM-002	\N	BPM signal D		VALID	\N
970		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
971		A2T-090LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
972		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
973		A2T-090LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
974		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
975		A2T-090LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
976		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
977		A2T-090LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
978		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
979		A2T-090LWU:PBI-BPM-002	\N	BPM signal S		VALID	\N
980		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
981		A2T-090LWU:PBI-BPM-002	\N	BPM signal U		VALID	\N
982		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
983		A2T-090LWU:PBI-BPM-002	\N	BPM signal N		VALID	\N
984		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
985		A2T-090LWU:PBI-BPM-002	\N	BPM signal D		VALID	\N
986		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
987		A2T-120LWU:PBI-BPM-001	\N	BPM signal S		VALID	\N
988		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
989		A2T-120LWU:PBI-BPM-001	\N	BPM signal U		VALID	\N
990		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
991		A2T-120LWU:PBI-BPM-001	\N	BPM signal N		VALID	\N
992		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
993		A2T-120LWU:PBI-BPM-001	\N	BPM signal D		VALID	\N
994		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
995		A2T-120LWU:PBI-BPM-002	\N	BPM signal S		VALID	\N
996		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
997		A2T-120LWU:PBI-BPM-002	\N	BPM signal U		VALID	\N
998		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
999		A2T-120LWU:PBI-BPM-002	\N	BPM signal N		VALID	\N
1000		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1001		A2T-120LWU:PBI-BPM-002	\N	BPM signal D		VALID	\N
1002		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1003		A2T-130DRF:PBI-BPM-001	\N	BPM signal S		VALID	\N
1004		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1005		A2T-130DRF:PBI-BPM-001	\N	BPM signal U		VALID	\N
1006		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1007		A2T-130DRF:PBI-BPM-001	\N	BPM signal N		VALID	\N
1008		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1009		A2T-130DRF:PBI-BPM-001	\N	BPM signal D		VALID	\N
1010		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1011		A2T-060LWU:PBI-BPM-001	\N	BPM phase reference tap 45		VALID	\N
1012		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1013		A2T-080LWU:PBI-BPM-002	\N	BPM phase reference tap 46		VALID	\N
1014		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1015		A2T-090LWU:PBI-BPM-002	\N	BPM phase reference tap 47		VALID	\N
1016		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1017		A2T-120LWU:PBI-BPM-002	\N	BPM phase reference tap 48		VALID	\N
1018		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
1019		A2T-130DRF:PBI-BPM-001	\N	BPM phase reference tap 49		VALID	\N
1020		A2T-010ROW:CNPW-U-004	\N	A2T		VALID	\N
\.


--
-- Name: endpoint_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('endpoint_id_seq', 1020, true);


--
-- Data for Name: history; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY history (id, entityid, entitytype, entry, operation, "timestamp", userid, entityname) FROM stdin;
171	223	CABLE_TYPE	Created cable type with name: 1/2"RF coaxial	CREATE	2016-04-15 15:11:09.558	evangeliavaena	1/2"RF coaxial
172	224	CABLE_TYPE	Created cable type with name: 1/2"RF coaxial_HF	CREATE	2016-04-15 15:11:09.568	evangeliavaena	1/2"RF coaxial_HF
173	225	CABLE_TYPE	Created cable type with name: 1/4"RF coaxial	CREATE	2016-04-15 15:11:09.578	evangeliavaena	1/4"RF coaxial
174	226	CABLE_TYPE	Created cable type with name: 1/4"RF coaxial_HF	CREATE	2016-04-15 15:11:09.587	evangeliavaena	1/4"RF coaxial_HF
175	227	CABLE_TYPE	Created cable type with name: 1/4"RF coaxial superflexible	CREATE	2016-04-15 15:11:09.596	evangeliavaena	1/4"RF coaxial superflexible
176	228	CABLE_TYPE	Created cable type with name: 3/8"RF coaxial_HF	CREATE	2016-04-15 15:11:09.606	evangeliavaena	3/8"RF coaxial_HF
177	229	CABLE_TYPE	Created cable type with name: 3/8"RF coaxial	CREATE	2016-04-15 15:11:09.616	evangeliavaena	3/8"RF coaxial
178	230	CABLE_TYPE	Created cable type with name: 7/8" RF coaxial	CREATE	2016-04-15 15:11:09.626	evangeliavaena	7/8" RF coaxial
179	231	CABLE_TYPE	Created cable type with name: 7/8" RF coaxial_HF	CREATE	2016-04-15 15:11:09.635	evangeliavaena	7/8" RF coaxial_HF
180	232	CABLE_TYPE	Created cable type with name: 1-5/8" RF coaxial	CREATE	2016-04-15 15:11:09.644	evangeliavaena	1-5/8" RF coaxial
181	233	CABLE_TYPE	Created cable type with name: 10C22OS	CREATE	2016-04-15 15:11:09.654	evangeliavaena	10C22OS
182	234	CABLE_TYPE	Created cable type with name: 10PR22OS	CREATE	2016-04-15 15:11:09.665	evangeliavaena	10PR22OS
183	235	CABLE_TYPE	Created cable type with name: 10C22OSB	CREATE	2016-04-15 15:11:09.675	evangeliavaena	10C22OSB
184	236	CABLE_TYPE	Created cable type with name: 6C20OSHF	CREATE	2016-04-15 15:11:09.684	evangeliavaena	6C20OSHF
185	237	CABLE_TYPE	Created cable type with name: 12C16OS6VHF	CREATE	2016-04-15 15:11:09.694	evangeliavaena	12C16OS6VHF
186	238	CABLE_TYPE	Created cable type with name: 12C20OS	CREATE	2016-04-15 15:11:09.703	evangeliavaena	12C20OS
187	239	CABLE_TYPE	Created cable type with name: 12C20OSHF	CREATE	2016-04-15 15:11:09.713	evangeliavaena	12C20OSHF
188	240	CABLE_TYPE	Created cable type with name: 12P16OIS	CREATE	2016-04-15 15:11:09.722	evangeliavaena	12P16OIS
189	241	CABLE_TYPE	Created cable type with name: 12PR20OIS	CREATE	2016-04-15 15:11:09.731	evangeliavaena	12PR20OIS
190	242	CABLE_TYPE	Created cable type with name: 12PR20OSHF	CREATE	2016-04-15 15:11:09.74	evangeliavaena	12PR20OSHF
191	243	CABLE_TYPE	Created cable type with name: 12PR22OIS	CREATE	2016-04-15 15:11:09.752	evangeliavaena	12PR22OIS
192	244	CABLE_TYPE	Created cable type with name: 12PR22OISHF	CREATE	2016-04-15 15:11:09.762	evangeliavaena	12PR22OISHF
193	245	CABLE_TYPE	Created cable type with name: 12PR16IOSTC6V	CREATE	2016-04-15 15:11:09.773	evangeliavaena	12PR16IOSTC6V
194	246	CABLE_TYPE	Created cable type with name: 12PR16IOSTCHF	CREATE	2016-04-15 15:11:09.784	evangeliavaena	12PR16IOSTCHF
195	247	CABLE_TYPE	Created cable type with name: 12PR18IOSTCHF	CREATE	2016-04-15 15:11:09.794	evangeliavaena	12PR18IOSTCHF
196	248	CABLE_TYPE	Created cable type with name: 12PR20IOSTCHF	CREATE	2016-04-15 15:11:09.803	evangeliavaena	12PR20IOSTCHF
197	249	CABLE_TYPE	Created cable type with name: 12PR24IOS	CREATE	2016-04-15 15:11:09.813	evangeliavaena	12PR24IOS
198	250	CABLE_TYPE	Created cable type with name: 12PR24IOSHF	CREATE	2016-04-15 15:11:09.823	evangeliavaena	12PR24IOSHF
199	251	CABLE_TYPE	Created cable type with name: 4PR24OSTCHF	CREATE	2016-04-15 15:11:09.833	evangeliavaena	4PR24OSTCHF
200	252	CABLE_TYPE	Created cable type with name: 4PR22OSHF	CREATE	2016-04-15 15:11:09.843	evangeliavaena	4PR22OSHF
201	253	CABLE_TYPE	Created cable type with name: 4PR22OS	CREATE	2016-04-15 15:11:09.868	evangeliavaena	4PR22OS
202	254	CABLE_TYPE	Created cable type with name: 4PR20OS	CREATE	2016-04-15 15:11:09.89	evangeliavaena	4PR20OS
203	255	CABLE_TYPE	Created cable type with name: 5P18OSHF	CREATE	2016-04-15 15:11:09.906	evangeliavaena	5P18OSHF
204	256	CABLE_TYPE	Created cable type with name: 3P22OS3VHF	CREATE	2016-04-15 15:11:09.929	evangeliavaena	3P22OS3VHF
205	257	CABLE_TYPE	Created cable type with name: 12MFibSBFOutdoor	CREATE	2016-04-15 15:11:09.953	evangeliavaena	12MFibSBFOutdoor
206	258	CABLE_TYPE	Created cable type with name: 14C18OS6V	CREATE	2016-04-15 15:11:09.977	evangeliavaena	14C18OS6V
207	259	CABLE_TYPE	Created cable type with name: 14C18OS6VHF	CREATE	2016-04-15 15:11:09.992	evangeliavaena	14C18OS6VHF
208	260	CABLE_TYPE	Created cable type with name: 15C22OS3VTC	CREATE	2016-04-15 15:11:10.001	evangeliavaena	15C22OS3VTC
209	261	CABLE_TYPE	Created cable type with name: 15C24OSRS232	CREATE	2016-04-15 15:11:10.011	evangeliavaena	15C24OSRS232
210	262	CABLE_TYPE	Created cable type with name: 16PR20WCTC3V	CREATE	2016-04-15 15:11:10.02	evangeliavaena	16PR20WCTC3V
211	263	CABLE_TYPE	Created cable type with name: 16PR16IOS	CREATE	2016-04-15 15:11:10.03	evangeliavaena	16PR16IOS
212	264	CABLE_TYPE	Created cable type with name: 16PR16IOSHF	CREATE	2016-04-15 15:11:10.04	evangeliavaena	16PR16IOSHF
213	265	CABLE_TYPE	Created cable type with name: 16PR22OS	CREATE	2016-04-15 15:11:10.05	evangeliavaena	16PR22OS
214	266	CABLE_TYPE	Created cable type with name: 16P22OSHF	CREATE	2016-04-15 15:11:10.059	evangeliavaena	16P22OSHF
215	267	CABLE_TYPE	Created cable type with name: 16PR18IOS6V	CREATE	2016-04-15 15:11:10.068	evangeliavaena	16PR18IOS6V
216	268	CABLE_TYPE	Created cable type with name: 20PR20OSHF	CREATE	2016-04-15 15:11:10.078	evangeliavaena	20PR20OSHF
217	269	CABLE_TYPE	Created cable type with name: 25C22TCSHF	CREATE	2016-04-15 15:11:10.087	evangeliavaena	25C22TCSHF
218	270	CABLE_TYPE	Created cable type with name: 18MFibSBF	CREATE	2016-04-15 15:11:10.096	evangeliavaena	18MFibSBF
219	271	CABLE_TYPE	Created cable type with name: 1C_RG-8HV	CREATE	2016-04-15 15:11:10.106	evangeliavaena	1C_RG-8HV
220	272	CABLE_TYPE	Created cable type with name: 1C-RG-8/U	CREATE	2016-04-15 15:11:10.115	evangeliavaena	1C-RG-8/U
221	273	CABLE_TYPE	Created cable type with name: 1C10DEL	CREATE	2016-04-15 15:11:10.125	evangeliavaena	1C10DEL
222	274	CABLE_TYPE	Created cable type with name: 1C10SLW	CREATE	2016-04-15 15:11:10.136	evangeliavaena	1C10SLW
223	275	CABLE_TYPE	Created cable type with name: 1C13RG-213HF	CREATE	2016-04-15 15:11:10.145	evangeliavaena	1C13RG-213HF
224	276	CABLE_TYPE	Created cable type with name: 1C13RG-214HF	CREATE	2016-04-15 15:11:10.156	evangeliavaena	1C13RG-214HF
225	277	CABLE_TYPE	Created cable type with name: 1C18RG-6	CREATE	2016-04-15 15:11:10.165	evangeliavaena	1C18RG-6
226	278	CABLE_TYPE	Created cable type with name: 1C18RG-6HF	CREATE	2016-04-15 15:11:10.175	evangeliavaena	1C18RG-6HF
227	279	CABLE_TYPE	Created cable type with name: 1C19RG-223	CREATE	2016-04-15 15:11:10.185	evangeliavaena	1C19RG-223
228	280	CABLE_TYPE	Created cable type with name: 1C19RG-223HF	CREATE	2016-04-15 15:11:10.194	evangeliavaena	1C19RG-223HF
229	281	CABLE_TYPE	Created cable type with name: 1C20RG-58	CREATE	2016-04-15 15:11:10.203	evangeliavaena	1C20RG-58
230	282	CABLE_TYPE	Created cable type with name: 1C20RG-58U	CREATE	2016-04-15 15:11:10.213	evangeliavaena	1C20RG-58U
231	283	CABLE_TYPE	Created cable type with name: 1C20RG-58HV	CREATE	2016-04-15 15:11:10.222	evangeliavaena	1C20RG-58HV
232	284	CABLE_TYPE	Created cable type with name: 1C20RG-58HF	CREATE	2016-04-15 15:11:10.232	evangeliavaena	1C20RG-58HF
233	285	CABLE_TYPE	Created cable type with name: 1C20RG-59	CREATE	2016-04-15 15:11:10.26	evangeliavaena	1C20RG-59
234	286	CABLE_TYPE	Created cable type with name: 1C20RG-59HF	CREATE	2016-04-15 15:11:10.284	evangeliavaena	1C20RG-59HF
235	287	CABLE_TYPE	Created cable type with name: 1C20RG-59U_Triax	CREATE	2016-04-15 15:11:10.308	evangeliavaena	1C20RG-59U_Triax
236	288	CABLE_TYPE	Created cable type with name: 1C22RG-59/U	CREATE	2016-04-15 15:11:10.328	evangeliavaena	1C22RG-59/U
237	289	CABLE_TYPE	Created cable type with name: 1CRG-174HF	CREATE	2016-04-15 15:11:10.338	evangeliavaena	1CRG-174HF
238	290	CABLE_TYPE	Created cable type with name: 1C-H07Z-R 16	CREATE	2016-04-15 15:11:10.347	evangeliavaena	1C-H07Z-R 16
239	291	CABLE_TYPE	Created cable type with name: 1C-H07Z-R 25	CREATE	2016-04-15 15:11:10.357	evangeliavaena	1C-H07Z-R 25
240	292	CABLE_TYPE	Created cable type with name: 1C-H07Z-R 35	CREATE	2016-04-15 15:11:10.367	evangeliavaena	1C-H07Z-R 35
241	293	CABLE_TYPE	Created cable type with name: 1C-H07Z-R 50	CREATE	2016-04-15 15:11:10.376	evangeliavaena	1C-H07Z-R 50
242	294	CABLE_TYPE	Created cable type with name: 1C-H07Z-R 70	CREATE	2016-04-15 15:11:10.386	evangeliavaena	1C-H07Z-R 70
243	295	CABLE_TYPE	Created cable type with name: 1C-H07Z-R 95	CREATE	2016-04-15 15:11:10.397	evangeliavaena	1C-H07Z-R 95
244	296	CABLE_TYPE	Created cable type with name: 1C-H07Z-R 120	CREATE	2016-04-15 15:11:10.406	evangeliavaena	1C-H07Z-R 120
245	297	CABLE_TYPE	Created cable type with name: 1C4/0DEL	CREATE	2016-04-15 15:11:10.418	evangeliavaena	1C4/0DEL
246	298	CABLE_TYPE	Created cable type with name: 1C4RG-219	CREATE	2016-04-15 15:11:10.433	evangeliavaena	1C4RG-219
247	299	CABLE_TYPE	Created cable type with name: 1C777DEL	CREATE	2016-04-15 15:11:10.444	evangeliavaena	1C777DEL
248	300	CABLE_TYPE	Created cable type with name: 1P16STCHF	CREATE	2016-04-15 15:11:10.454	evangeliavaena	1P16STCHF
249	301	CABLE_TYPE	Created cable type with name: 1P16THCP	CREATE	2016-04-15 15:11:10.464	evangeliavaena	1P16THCP
250	302	CABLE_TYPE	Created cable type with name: 1P16OSTC6V	CREATE	2016-04-15 15:11:10.474	evangeliavaena	1P16OSTC6V
251	303	CABLE_TYPE	Created cable type with name: 1P18OSTC	CREATE	2016-04-15 15:11:10.486	evangeliavaena	1P18OSTC
252	304	CABLE_TYPE	Created cable type with name: 2P18OSTC	CREATE	2016-04-15 15:11:10.496	evangeliavaena	2P18OSTC
253	305	CABLE_TYPE	Created cable type with name: 1P22OSTC3V	CREATE	2016-04-15 15:11:10.506	evangeliavaena	1P22OSTC3V
254	306	CABLE_TYPE	Created cable type with name: 4PR18OTC3V	CREATE	2016-04-15 15:11:10.516	evangeliavaena	4PR18OTC3V
255	307	CABLE_TYPE	Created cable type with name: 2MFibSBF	CREATE	2016-04-15 15:11:10.525	evangeliavaena	2MFibSBF
256	308	CABLE_TYPE	Created cable type with name: Multimode Glass Fibre cable	CREATE	2016-04-15 15:11:10.535	evangeliavaena	Multimode Glass Fibre cable
257	309	CABLE_TYPE	Created cable type with name: DeviceNet Cable	CREATE	2016-04-15 15:11:10.545	evangeliavaena	DeviceNet Cable
258	310	CABLE_TYPE	Created cable type with name: Ethernet/ Profinet , 2YH(ST)C11Y	CREATE	2016-04-15 15:11:10.554	evangeliavaena	Ethernet/ Profinet , 2YH(ST)C11Y
259	311	CABLE_TYPE	Created cable type with name: Profibus 2Y(ST)CY	CREATE	2016-04-15 15:11:10.564	evangeliavaena	Profibus 2Y(ST)CY
260	312	CABLE_TYPE	Created cable type with name: Profinet  Cat 5	CREATE	2016-04-15 15:11:10.573	evangeliavaena	Profinet  Cat 5
261	313	CABLE_TYPE	Created cable type with name: Cat.7 S/FTP LSHF­FR	CREATE	2016-04-15 15:11:10.583	evangeliavaena	Cat.7 S/FTP LSHF­FR
262	314	CABLE_TYPE	Created cable type with name: Cat.6 S/FTP LSHF	CREATE	2016-04-15 15:11:10.592	evangeliavaena	Cat.6 S/FTP LSHF
263	315	CABLE_TYPE	Created cable type with name: 1CRG-108	CREATE	2016-04-15 15:11:10.602	evangeliavaena	1CRG-108
264	316	CABLE_TYPE	Created cable type with name: 1CRG-11U	CREATE	2016-04-15 15:11:10.612	evangeliavaena	1CRG-11U
265	317	CABLE_TYPE	Created cable type with name: 2YC2YC(St)H	CREATE	2016-04-15 15:11:10.623	evangeliavaena	2YC2YC(St)H
266	318	CABLE_TYPE	Created cable type with name: FXQJ EMC 0,6/1 kV 3x150	CREATE	2016-04-15 15:11:10.634	evangeliavaena	FXQJ EMC 0,6/1 kV 3x150
267	319	CABLE_TYPE	Created cable type with name: FXQJ EMC 0,6/1 kV 3x10	CREATE	2016-04-15 15:11:10.644	evangeliavaena	FXQJ EMC 0,6/1 kV 3x10
268	320	CABLE_TYPE	Created cable type with name: FXQJ EMC 0,6/1 kV 3x25	CREATE	2016-04-15 15:11:10.654	evangeliavaena	FXQJ EMC 0,6/1 kV 3x25
269	321	CABLE_TYPE	Created cable type with name: FXQJ EMC 0,6/1 kV 3x50	CREATE	2016-04-15 15:11:10.664	evangeliavaena	FXQJ EMC 0,6/1 kV 3x50
270	322	CABLE_TYPE	Created cable type with name: FXQJ EMC 0,6/1 kV 3x95	CREATE	2016-04-15 15:11:10.673	evangeliavaena	FXQJ EMC 0,6/1 kV 3x95
271	323	CABLE_TYPE	Created cable type with name: 12P18U1V	CREATE	2016-04-15 15:11:10.683	evangeliavaena	12P18U1V
272	324	CABLE_TYPE	Created cable type with name: 8P1C22ST3V	CREATE	2016-04-15 15:11:10.694	evangeliavaena	8P1C22ST3V
273	325	CABLE_TYPE	Created cable type with name: 8P24ST3V	CREATE	2016-04-15 15:11:10.704	evangeliavaena	8P24ST3V
274	326	CABLE_TYPE	Created cable type with name: 8C22STC3V	CREATE	2016-04-15 15:11:10.715	evangeliavaena	8C22STC3V
275	327	CABLE_TYPE	Created cable type with name: 10C22STC3V	CREATE	2016-04-15 15:11:10.725	evangeliavaena	10C22STC3V
276	328	CABLE_TYPE	Created cable type with name: 15C24S3V	CREATE	2016-04-15 15:11:10.735	evangeliavaena	15C24S3V
277	329	CABLE_TYPE	Created cable type with name: 3C24STC	CREATE	2016-04-15 15:11:10.746	evangeliavaena	3C24STC
278	330	CABLE_TYPE	Created cable type with name: 3C26STC6VHF	CREATE	2016-04-15 15:11:10.756	evangeliavaena	3C26STC6VHF
279	331	CABLE_TYPE	Created cable type with name: 3C0,34OSHF	CREATE	2016-04-15 15:11:10.765	evangeliavaena	3C0,34OSHF
280	332	CABLE_TYPE	Created cable type with name: 4C1OSTCHF	CREATE	2016-04-15 15:11:10.775	evangeliavaena	4C1OSTCHF
281	333	CABLE_TYPE	Created cable type with name: 4C16OSBCHF	CREATE	2016-04-15 15:11:10.785	evangeliavaena	4C16OSBCHF
282	334	CABLE_TYPE	Created cable type with name: 3G0,75	CREATE	2016-04-15 15:11:10.794	evangeliavaena	3G0,75
283	335	CABLE_TYPE	Created cable type with name: 3G1,5	CREATE	2016-04-15 15:11:10.805	evangeliavaena	3G1,5
284	336	CABLE_TYPE	Created cable type with name: 6G1,5	CREATE	2016-04-15 15:11:10.815	evangeliavaena	6G1,5
285	337	CABLE_TYPE	Created cable type with name: 3x6 N2XCH	CREATE	2016-04-15 15:11:10.825	evangeliavaena	3x6 N2XCH
286	338	CABLE_TYPE	Created cable type with name: AXQJ LT 12 kV 3x240/35	CREATE	2016-04-15 15:11:10.835	evangeliavaena	AXQJ LT 12 kV 3x240/35
287	339	CABLE_TYPE	Created cable type with name: AXQJ LT 12 kV 3x150/25	CREATE	2016-04-15 15:11:10.845	evangeliavaena	AXQJ LT 12 kV 3x150/25
288	340	CABLE_TYPE	Created cable type with name: AXQJ 1 kV 4X240/72	CREATE	2016-04-15 15:11:10.857	evangeliavaena	AXQJ 1 kV 4X240/72
289	341	CABLE_TYPE	Created cable type with name: AXQJ 1 kV 4X50/15	CREATE	2016-04-15 15:11:10.867	evangeliavaena	AXQJ 1 kV 4X50/15
290	342	CABLE_TYPE	Created cable type with name: AXQJ 1 kV 4X120/41	CREATE	2016-04-15 15:11:10.876	evangeliavaena	AXQJ 1 kV 4X120/41
291	343	CABLE_TYPE	Created cable type with name: AXQJ 1 kV 4X150/41	CREATE	2016-04-15 15:11:10.886	evangeliavaena	AXQJ 1 kV 4X150/41
292	344	CABLE_TYPE	Created cable type with name: 1P18OSCHF	CREATE	2016-04-18 11:29:51.938	evangeliavaena	1P18OSCHF
293	223	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.27	evangeliavaena	1/2"RF coaxial
294	224	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.278	evangeliavaena	1/2"RF coaxial_HF
295	225	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.285	evangeliavaena	1/4"RF coaxial
296	226	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.291	evangeliavaena	1/4"RF coaxial_HF
297	227	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.298	evangeliavaena	1/4"RF coaxial superflexible
298	228	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.309	evangeliavaena	3/8"RF coaxial_HF
299	229	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.314	evangeliavaena	3/8"RF coaxial
300	230	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.316	evangeliavaena	7/8" RF coaxial
301	231	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.319	evangeliavaena	7/8" RF coaxial_HF
302	232	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.321	evangeliavaena	1-5/8" RF coaxial
303	233	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.324	evangeliavaena	10C22OS
304	234	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.326	evangeliavaena	10PR22OS
305	235	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.328	evangeliavaena	10C22OSB
306	236	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.332	evangeliavaena	6C20OSHF
307	237	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.335	evangeliavaena	12C16OS6VHF
308	238	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.339	evangeliavaena	12C20OS
309	239	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.347	evangeliavaena	12C20OSHF
310	240	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.35	evangeliavaena	12P16OIS
311	241	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.353	evangeliavaena	12PR20OIS
312	242	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.355	evangeliavaena	12PR20OSHF
313	243	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.357	evangeliavaena	12PR22OIS
314	244	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.36	evangeliavaena	12PR22OISHF
315	245	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.363	evangeliavaena	12PR16IOSTC6V
316	246	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.366	evangeliavaena	12PR16IOSTCHF
317	247	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.368	evangeliavaena	12PR18IOSTCHF
318	248	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.371	evangeliavaena	12PR20IOSTCHF
319	249	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.373	evangeliavaena	12PR24IOS
320	250	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.375	evangeliavaena	12PR24IOSHF
321	251	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.379	evangeliavaena	4PR24OSTCHF
322	252	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.381	evangeliavaena	4PR22OSHF
323	253	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.383	evangeliavaena	4PR22OS
324	254	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.386	evangeliavaena	4PR20OS
325	255	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.39	evangeliavaena	5P18OSHF
326	256	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.392	evangeliavaena	3P22OS3VHF
327	257	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.395	evangeliavaena	12MFibSBFOutdoor
328	258	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.398	evangeliavaena	14C18OS6V
329	259	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.4	evangeliavaena	14C18OS6VHF
330	260	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.403	evangeliavaena	15C22OS3VTC
331	261	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.405	evangeliavaena	15C24OSRS232
332	262	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.408	evangeliavaena	16PR20WCTC3V
333	263	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.411	evangeliavaena	16PR16IOS
334	264	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.413	evangeliavaena	16PR16IOSHF
335	265	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.416	evangeliavaena	16PR22OS
336	266	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.419	evangeliavaena	16P22OSHF
337	267	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.422	evangeliavaena	16PR18IOS6V
338	268	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.425	evangeliavaena	20PR20OSHF
339	269	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.428	evangeliavaena	25C22TCSHF
340	270	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.431	evangeliavaena	18MFibSBF
341	271	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.434	evangeliavaena	1C_RG-8HV
342	272	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.437	evangeliavaena	1C-RG-8/U
343	273	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.442	evangeliavaena	1C10DEL
344	274	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.447	evangeliavaena	1C10SLW
345	275	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.45	evangeliavaena	1C13RG-213HF
346	276	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.452	evangeliavaena	1C13RG-214HF
347	277	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.456	evangeliavaena	1C18RG-6
348	278	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.458	evangeliavaena	1C18RG-6HF
349	279	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.461	evangeliavaena	1C19RG-223
350	280	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.463	evangeliavaena	1C19RG-223HF
351	281	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.466	evangeliavaena	1C20RG-58
352	282	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.469	evangeliavaena	1C20RG-58U
353	283	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.471	evangeliavaena	1C20RG-58HV
354	284	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.474	evangeliavaena	1C20RG-58HF
355	285	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.478	evangeliavaena	1C20RG-59
356	286	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.48	evangeliavaena	1C20RG-59HF
357	287	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.483	evangeliavaena	1C20RG-59U_Triax
358	288	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.486	evangeliavaena	1C22RG-59/U
359	289	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.489	evangeliavaena	1CRG-174HF
360	290	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.492	evangeliavaena	1C-H07Z-R 16
361	324	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.494	evangeliavaena	8P1C22ST3V
362	291	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.497	evangeliavaena	1C-H07Z-R 25
363	292	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.499	evangeliavaena	1C-H07Z-R 35
364	293	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.502	evangeliavaena	1C-H07Z-R 50
365	294	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.505	evangeliavaena	1C-H07Z-R 70
366	295	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.507	evangeliavaena	1C-H07Z-R 95
367	296	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.51	evangeliavaena	1C-H07Z-R 120
368	297	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.513	evangeliavaena	1C4/0DEL
369	298	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.515	evangeliavaena	1C4RG-219
370	299	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.518	evangeliavaena	1C777DEL
371	300	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.521	evangeliavaena	1P16STCHF
372	301	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.524	evangeliavaena	1P16THCP
373	302	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.527	evangeliavaena	1P16OSTC6V
374	303	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.529	evangeliavaena	1P18OSTC
375	304	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.532	evangeliavaena	2P18OSTC
376	305	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.535	evangeliavaena	1P22OSTC3V
377	306	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.538	evangeliavaena	4PR18OTC3V
378	307	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.541	evangeliavaena	2MFibSBF
379	308	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.543	evangeliavaena	Multimode Glass Fibre cable
380	309	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.546	evangeliavaena	DeviceNet Cable
381	310	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.549	evangeliavaena	Ethernet/ Profinet , 2YH(ST)C11Y
382	311	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.552	evangeliavaena	Profibus 2Y(ST)CY
383	312	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.555	evangeliavaena	Profinet  Cat 5
384	313	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.557	evangeliavaena	Cat.7 S/FTP LSHF­FR
385	314	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.56	evangeliavaena	Cat.6 S/FTP LSHF
386	315	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.563	evangeliavaena	1CRG-108
387	316	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.566	evangeliavaena	1CRG-11U
388	317	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.569	evangeliavaena	2YC2YC(St)H
389	318	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.572	evangeliavaena	FXQJ EMC 0,6/1 kV 3x150
390	319	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.574	evangeliavaena	FXQJ EMC 0,6/1 kV 3x10
391	320	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.577	evangeliavaena	FXQJ EMC 0,6/1 kV 3x25
392	321	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.58	evangeliavaena	FXQJ EMC 0,6/1 kV 3x50
393	322	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.583	evangeliavaena	FXQJ EMC 0,6/1 kV 3x95
394	323	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.586	evangeliavaena	12P18U1V
395	325	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.589	evangeliavaena	8P24ST3V
396	326	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.592	evangeliavaena	8C22STC3V
397	327	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.595	evangeliavaena	10C22STC3V
398	328	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.598	evangeliavaena	15C24S3V
399	329	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.601	evangeliavaena	3C24STC
400	330	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.604	evangeliavaena	3C26STC6VHF
401	331	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.607	evangeliavaena	3C0,34OSHF
402	332	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.611	evangeliavaena	4C1OSTCHF
403	333	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.614	evangeliavaena	4C16OSBCHF
404	334	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.617	evangeliavaena	3G0,75
405	335	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.619	evangeliavaena	3G1,5
406	336	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.622	evangeliavaena	6G1,5
407	337	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.625	evangeliavaena	3x6 N2XCH
408	338	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.628	evangeliavaena	AXQJ LT 12 kV 3x240/35
409	339	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.631	evangeliavaena	AXQJ LT 12 kV 3x150/25
410	340	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.634	evangeliavaena	AXQJ 1 kV 4X240/72
411	341	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.637	evangeliavaena	AXQJ 1 kV 4X50/15
412	342	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.64	evangeliavaena	AXQJ 1 kV 4X120/41
413	343	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.644	evangeliavaena	AXQJ 1 kV 4X150/41
414	344	CABLE_TYPE		UPDATE	2016-04-19 17:08:34.647	evangeliavaena	1P18OSCHF
415	254	CABLE_TYPE	Updated (Attribute: newValue (oldValue)):\nDescription: 4 pairs, 0,52mm2, 20 AWG, color coded, overall shield, XLPE, PUR,PE insulation (4 pairs, 0,25mm2/22 AWG, color coded, overall shield, XLPE, PUR,PE insulation)\n	UPDATE	2016-06-30 13:36:16.7	evangeliavaena	4PR20OS
416	345	CABLE_TYPE	Created cable type with name: 16P17IOTC250VHF	CREATE	2016-08-24 17:12:03.843	evangeliavaena	16P17IOTC250VHF
417	5	CABLE	Created cable with number: 30A000001	CREATE	2016-08-26 10:03:30.168	evangeliavaena	30A000001
418	5	CABLE	Deleted cable with number: 30A000001	DELETE	2016-08-26 10:04:41.1	evangeliavaena	30A000001
419	6	CABLE	Created cable with number: 10A000002	CREATE	2016-08-26 10:13:38.936	evangeliavaena	10A000002
420	6	CABLE	Deleted cable with number: 10A000002	DELETE	2016-08-26 10:13:47.702	evangeliavaena	10A000002
477	22	CABLE		UPDATE	2016-10-04 13:16:52.739	johannorin	32A000018
421	346	CABLE_TYPE	Created cable type with name: 2C16OCHF	CREATE	2016-08-30 12:53:39.921	evangeliavaena	2C16OCHF
422	347	CABLE_TYPE	Created cable type with name: 50P18IOSHF	CREATE	2016-08-30 13:26:15.306	evangeliavaena	50P18IOSHF
423	348	CABLE_TYPE	Created cable type with name: 2P19IOSHF	CREATE	2016-08-30 13:52:42.929	evangeliavaena	2P19IOSHF
424	349	CABLE_TYPE	Created cable type with name: 2C19OSHF	CREATE	2016-08-30 14:06:47	evangeliavaena	2C19OSHF
425	346	CABLE_TYPE	Updated (Attribute: newValue (oldValue)):\nModel: http://www.fireproof-cables.com/download/lszh/5308.pdf (http://www.caledonian-cables.com/products/EN50288-7/EN50288-15.shtml)\n	UPDATE	2016-08-30 14:07:45.215	evangeliavaena	2C16OCHF
426	349	CABLE_TYPE	Updated (Attribute: newValue (oldValue)):\nModel: http://www.fireproof-cables.com/download/lszh/5308.pdf (null)\n	UPDATE	2016-08-30 14:08:03.418	evangeliavaena	2C19OSHF
427	347	CABLE_TYPE	Updated (Attribute: newValue (oldValue)):\nModel: http://www.fireproof-cables.com/download/lszh/5308.pdf (http://www.caledonian-cables.net/British_Standards/EN50288-7/EN%2050288%20RE-2X(St)H%20Pimf%20multi%20pair.html)\n	UPDATE	2016-08-30 14:09:40.919	evangeliavaena	50P18IOSHF
428	347	CABLE_TYPE	Updated (Attribute: newValue (oldValue)):\nComments: CDS tag: CBL_A (CBL_A)\n	UPDATE	2016-08-30 14:10:09.976	evangeliavaena	50P18IOSHF
429	347	CABLE_TYPE	Updated (Attribute: newValue (oldValue)):\nJacket: LSHF (XLPE)\n	UPDATE	2016-08-30 14:10:35.942	evangeliavaena	50P18IOSHF
430	346	CABLE_TYPE	Updated (Attribute: newValue (oldValue)):\nJacket: LSHF (LSZH)\n	UPDATE	2016-08-30 14:10:54.547	evangeliavaena	2C16OCHF
431	350	CABLE_TYPE	Created cable type with name: 3C21OSHF	CREATE	2016-08-30 14:16:21.356	evangeliavaena	3C21OSHF
432	331	CABLE_TYPE	Updated (Attribute: newValue (oldValue)):\nDescription: 3 conductors, Bare stranded copper  conductor, halogen  free core  insulation and  outer  sheath,  colour code in  accordance  with DIN  47100 3x0,34mm2, 22AWG (3 conductors, Bare stranded copper  conductor, halogen  free core  insulation and  outer  sheath,  colour code in  accordance  with DIN  47100 3x0,34mm2)\nService/Function: Vacuum ()\n	UPDATE	2016-08-30 14:17:22.926	evangeliavaena	3C0,34OSHF
433	351	CABLE_TYPE	Created cable type with name: 4P21IOSLSHF	CREATE	2016-08-30 14:26:51.224	evangeliavaena	4P21IOSLSHF
434	346	CABLE_TYPE	Deleted cable type with name: 2C16OCHF	DELETE	2016-08-30 14:28:32.077	evangeliavaena	2C16OCHF
435	352	CABLE_TYPE	Created cable type with name: 2C16OSHF	CREATE	2016-08-30 14:30:30.856	evangeliavaena	2C16OSHF
436	7	CABLE	Created cable with number: 83B000003	CREATE	2016-09-05 16:28:16.924	benedettogallese	83B000003
437	7	CABLE	Deleted cable with number: 83B000003	DELETE	2016-09-05 16:30:15.644	benedettogallese	83B000003
438	8	CABLE	Created cable with number: 22B000004	CREATE	2016-09-07 14:50:43.011	johannorin	22B000004
439	8	CABLE	Updated (Attribute: newValue (oldValue)):\nUser Label Text B: PBI Lab room, (PBI Lab room)\n	UPDATE	2016-09-07 15:43:26.862	johannorin	22B000004
440	8	CABLE	Validated cable with number: 22B000004.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-09-23 16:44:00.072	inigoalonso	22B000004
441	9	CABLE	Created cable with number: 32A000005	CREATE	2016-09-29 15:20:27.739	johannorin	32A000005
442	10	CABLE	Created cable with number: 32A000006	CREATE	2016-09-29 15:20:27.747	johannorin	32A000006
443	11	CABLE	Created cable with number: 32A000007	CREATE	2016-09-29 15:20:27.754	johannorin	32A000007
444	12	CABLE	Created cable with number: 32A000008	CREATE	2016-09-29 15:20:27.76	johannorin	32A000008
445	13	CABLE	Created cable with number: 32A000009	CREATE	2016-09-29 15:20:27.771	johannorin	32A000009
446	14	CABLE	Created cable with number: 32A000010	CREATE	2016-09-29 15:20:27.777	johannorin	32A000010
447	15	CABLE	Created cable with number: 32A000011	CREATE	2016-09-29 15:20:27.785	johannorin	32A000011
448	16	CABLE	Created cable with number: 32A000012	CREATE	2016-09-29 15:20:27.791	johannorin	32A000012
449	17	CABLE	Created cable with number: 32A000013	CREATE	2016-09-29 15:20:27.797	johannorin	32A000013
450	18	CABLE	Created cable with number: 32A000014	CREATE	2016-09-29 15:20:27.804	johannorin	32A000014
451	19	CABLE	Created cable with number: 32A000015	CREATE	2016-09-29 15:20:27.811	johannorin	32A000015
452	20	CABLE	Created cable with number: 32A000016	CREATE	2016-09-29 15:20:27.818	johannorin	32A000016
453	21	CABLE	Created cable with number: 32A000017	CREATE	2016-09-29 15:20:27.824	johannorin	32A000017
454	22	CABLE	Created cable with number: 32A000018	CREATE	2016-09-29 15:20:27.831	johannorin	32A000018
455	23	CABLE	Created cable with number: 32A000019	CREATE	2016-09-29 15:20:27.837	johannorin	32A000019
456	24	CABLE	Created cable with number: 32A000020	CREATE	2016-09-29 15:20:27.847	johannorin	32A000020
457	25	CABLE	Created cable with number: 32A000021	CREATE	2016-09-29 15:20:27.854	johannorin	32A000021
458	26	CABLE	Created cable with number: 32A000022	CREATE	2016-09-29 15:20:27.861	johannorin	32A000022
459	27	CABLE	Created cable with number: 32A000023	CREATE	2016-09-29 15:20:27.868	johannorin	32A000023
460	28	CABLE	Created cable with number: 32A000024	CREATE	2016-09-29 15:20:27.878	johannorin	32A000024
461	29	CABLE	Created cable with number: 32A000025	CREATE	2016-09-29 15:20:27.885	johannorin	32A000025
462	30	CABLE	Created cable with number: 32A000026	CREATE	2016-09-29 15:20:27.894	johannorin	32A000026
463	31	CABLE	Created cable with number: 32A000027	CREATE	2016-09-29 15:20:27.903	johannorin	32A000027
464	9	CABLE		UPDATE	2016-10-04 13:16:52.432	johannorin	32A000005
465	10	CABLE		UPDATE	2016-10-04 13:16:52.46	johannorin	32A000006
466	11	CABLE		UPDATE	2016-10-04 13:16:52.481	johannorin	32A000007
467	12	CABLE		UPDATE	2016-10-04 13:16:52.511	johannorin	32A000008
468	13	CABLE		UPDATE	2016-10-04 13:16:52.535	johannorin	32A000009
469	14	CABLE		UPDATE	2016-10-04 13:16:52.56	johannorin	32A000010
470	15	CABLE		UPDATE	2016-10-04 13:16:52.58	johannorin	32A000011
471	16	CABLE		UPDATE	2016-10-04 13:16:52.6	johannorin	32A000012
472	17	CABLE		UPDATE	2016-10-04 13:16:52.622	johannorin	32A000013
473	18	CABLE		UPDATE	2016-10-04 13:16:52.649	johannorin	32A000014
474	19	CABLE		UPDATE	2016-10-04 13:16:52.671	johannorin	32A000015
475	20	CABLE		UPDATE	2016-10-04 13:16:52.693	johannorin	32A000016
476	21	CABLE		UPDATE	2016-10-04 13:16:52.715	johannorin	32A000017
478	23	CABLE		UPDATE	2016-10-04 13:16:52.764	johannorin	32A000019
479	24	CABLE		UPDATE	2016-10-04 13:16:52.789	johannorin	32A000020
480	25	CABLE		UPDATE	2016-10-04 13:16:52.816	johannorin	32A000021
481	26	CABLE		UPDATE	2016-10-04 13:16:52.84	johannorin	32A000022
482	27	CABLE		UPDATE	2016-10-04 13:16:52.866	johannorin	32A000023
483	28	CABLE		UPDATE	2016-10-04 13:16:52.89	johannorin	32A000024
484	29	CABLE		UPDATE	2016-10-04 13:16:52.915	johannorin	32A000025
485	30	CABLE		UPDATE	2016-10-04 13:16:52.943	johannorin	32A000026
486	31	CABLE		UPDATE	2016-10-04 13:16:52.973	johannorin	32A000027
487	32	CABLE	Created cable with number: 32A000028	CREATE	2016-10-04 13:16:52.996	johannorin	32A000028
488	33	CABLE	Created cable with number: 32G000029	CREATE	2016-10-04 13:16:53.006	johannorin	32G000029
489	34	CABLE	Created cable with number: 32A000030	CREATE	2016-10-04 13:16:53.016	johannorin	32A000030
490	35	CABLE	Created cable with number: 32A000031	CREATE	2016-10-04 13:16:53.029	johannorin	32A000031
491	36	CABLE	Created cable with number: 32A000032	CREATE	2016-10-04 13:16:53.039	johannorin	32A000032
492	37	CABLE	Created cable with number: 32G000033	CREATE	2016-10-04 13:16:53.05	johannorin	32G000033
493	38	CABLE	Created cable with number: 32A000034	CREATE	2016-10-04 13:16:53.06	johannorin	32A000034
494	39	CABLE	Created cable with number: 32A000035	CREATE	2016-10-04 13:16:53.069	johannorin	32A000035
495	40	CABLE	Created cable with number: 32A000036	CREATE	2016-10-04 13:16:53.078	johannorin	32A000036
496	41	CABLE	Created cable with number: 32A000037	CREATE	2016-10-04 13:16:53.088	johannorin	32A000037
497	42	CABLE	Created cable with number: 32A000038	CREATE	2016-10-04 13:16:53.098	johannorin	32A000038
498	43	CABLE	Created cable with number: 32G000039	CREATE	2016-10-04 13:16:53.11	johannorin	32G000039
499	44	CABLE	Created cable with number: 32A000040	CREATE	2016-10-04 13:16:53.122	johannorin	32A000040
500	45	CABLE	Created cable with number: 32A000041	CREATE	2016-10-04 13:16:53.132	johannorin	32A000041
501	46	CABLE	Created cable with number: 32A000042	CREATE	2016-10-04 13:16:53.143	johannorin	32A000042
502	47	CABLE	Created cable with number: 32A000043	CREATE	2016-10-04 13:16:53.155	johannorin	32A000043
503	48	CABLE	Created cable with number: 32G000044	CREATE	2016-10-04 13:16:53.169	johannorin	32G000044
504	49	CABLE	Created cable with number: 32A000045	CREATE	2016-10-04 13:16:53.244	johannorin	32A000045
505	50	CABLE	Created cable with number: 32A000046	CREATE	2016-10-04 13:16:53.279	johannorin	32A000046
506	51	CABLE	Created cable with number: 32A000047	CREATE	2016-10-04 13:16:53.291	johannorin	32A000047
507	52	CABLE	Created cable with number: 32A000048	CREATE	2016-10-04 13:16:53.302	johannorin	32A000048
508	53	CABLE	Created cable with number: 32A000049	CREATE	2016-10-04 13:16:53.314	johannorin	32A000049
509	54	CABLE	Created cable with number: 32A000050	CREATE	2016-10-04 13:16:53.327	johannorin	32A000050
510	55	CABLE	Created cable with number: 32A000051	CREATE	2016-10-04 13:16:53.339	johannorin	32A000051
511	56	CABLE	Created cable with number: 32A000052	CREATE	2016-10-04 13:16:53.351	johannorin	32A000052
512	57	CABLE	Created cable with number: 32A000053	CREATE	2016-10-04 13:16:53.363	johannorin	32A000053
513	58	CABLE	Created cable with number: 32A000054	CREATE	2016-10-04 13:16:53.376	johannorin	32A000054
514	59	CABLE	Created cable with number: 32G000055	CREATE	2016-10-04 13:16:53.388	johannorin	32G000055
515	60	CABLE	Created cable with number: 32G000056	CREATE	2016-10-04 13:16:53.399	johannorin	32G000056
516	61	CABLE	Created cable with number: 32G000057	CREATE	2016-10-04 13:16:53.412	johannorin	32G000057
517	62	CABLE	Created cable with number: 32G000058	CREATE	2016-10-04 13:16:53.423	johannorin	32G000058
518	63	CABLE	Created cable with number: 32G000059	CREATE	2016-10-04 13:16:53.434	johannorin	32G000059
519	64	CABLE	Created cable with number: 32A000060	CREATE	2016-10-04 13:16:53.441	johannorin	32A000060
520	65	CABLE	Created cable with number: 32C000061	CREATE	2016-10-04 13:16:53.449	johannorin	32C000061
521	66	CABLE	Created cable with number: 32E000062	CREATE	2016-10-04 15:39:39.888	johannorin	32E000062
522	67	CABLE	Created cable with number: 32A000063	CREATE	2016-10-04 15:39:39.898	johannorin	32A000063
523	68	CABLE	Created cable with number: 32A000064	CREATE	2016-10-04 15:39:39.907	johannorin	32A000064
524	69	CABLE	Created cable with number: 32E000065	CREATE	2016-10-04 15:39:39.916	johannorin	32E000065
525	70	CABLE	Created cable with number: 32D000066	CREATE	2016-10-04 15:39:39.925	johannorin	32D000066
526	71	CABLE	Created cable with number: 32E000067	CREATE	2016-10-04 15:39:39.935	johannorin	32E000067
527	72	CABLE	Created cable with number: 32D000068	CREATE	2016-10-04 15:39:39.943	johannorin	32D000068
528	73	CABLE	Created cable with number: 32A000069	CREATE	2016-10-04 15:39:39.951	johannorin	32A000069
529	74	CABLE	Created cable with number: 32A000070	CREATE	2016-10-04 15:39:39.962	johannorin	32A000070
530	75	CABLE	Created cable with number: 32E000071	CREATE	2016-10-04 15:39:39.971	johannorin	32E000071
531	76	CABLE	Created cable with number: 32D000072	CREATE	2016-10-04 15:39:39.984	johannorin	32D000072
532	77	CABLE	Created cable with number: 32E000073	CREATE	2016-10-04 15:39:39.997	johannorin	32E000073
533	78	CABLE	Created cable with number: 32D000074	CREATE	2016-10-04 15:39:40.006	johannorin	32D000074
534	79	CABLE	Created cable with number: 32A000075	CREATE	2016-10-04 15:39:40.014	johannorin	32A000075
535	80	CABLE	Created cable with number: 32G000076	CREATE	2016-10-04 15:39:40.022	johannorin	32G000076
536	81	CABLE	Created cable with number: 32K000077	CREATE	2016-10-04 15:39:40.031	johannorin	32K000077
537	82	CABLE	Created cable with number: 32K000078	CREATE	2016-10-04 15:39:40.041	johannorin	32K000078
538	83	CABLE	Created cable with number: 32K000079	CREATE	2016-10-04 15:39:40.05	johannorin	32K000079
539	84	CABLE	Created cable with number: 32K000080	CREATE	2016-10-04 15:39:40.058	johannorin	32K000080
540	85	CABLE	Created cable with number: 32E000081	CREATE	2016-10-04 15:39:40.066	johannorin	32E000081
541	86	CABLE	Created cable with number: 32E000082	CREATE	2016-10-04 15:39:40.075	johannorin	32E000082
542	87	CABLE	Created cable with number: 32E000083	CREATE	2016-10-04 15:39:40.083	johannorin	32E000083
543	88	CABLE	Created cable with number: 32L000084	CREATE	2016-10-04 15:39:40.095	johannorin	32L000084
544	9	CABLE		UPDATE	2016-10-05 17:39:24.205	johannorin	32A000005
545	10	CABLE		UPDATE	2016-10-05 17:39:24.238	johannorin	32A000006
546	11	CABLE		UPDATE	2016-10-05 17:39:24.339	johannorin	32A000007
547	12	CABLE		UPDATE	2016-10-05 17:39:24.387	johannorin	32A000008
548	13	CABLE		UPDATE	2016-10-05 17:39:24.421	johannorin	32A000009
549	14	CABLE		UPDATE	2016-10-05 17:39:24.457	johannorin	32A000010
550	15	CABLE		UPDATE	2016-10-05 17:39:24.493	johannorin	32A000011
551	16	CABLE		UPDATE	2016-10-05 17:39:24.521	johannorin	32A000012
552	17	CABLE		UPDATE	2016-10-05 17:39:24.556	johannorin	32A000013
553	18	CABLE		UPDATE	2016-10-05 17:39:24.584	johannorin	32A000014
554	19	CABLE		UPDATE	2016-10-05 17:39:24.619	johannorin	32A000015
555	20	CABLE		UPDATE	2016-10-05 17:39:24.649	johannorin	32A000016
556	21	CABLE		UPDATE	2016-10-05 17:39:24.68	johannorin	32A000017
557	22	CABLE		UPDATE	2016-10-05 17:39:24.71	johannorin	32A000018
558	23	CABLE		UPDATE	2016-10-05 17:39:24.731	johannorin	32A000019
559	24	CABLE		UPDATE	2016-10-05 17:39:24.761	johannorin	32A000020
560	25	CABLE		UPDATE	2016-10-05 17:39:24.779	johannorin	32A000021
561	26	CABLE		UPDATE	2016-10-05 17:39:24.801	johannorin	32A000022
562	27	CABLE		UPDATE	2016-10-05 17:39:24.827	johannorin	32A000023
563	28	CABLE		UPDATE	2016-10-05 17:39:24.855	johannorin	32A000024
564	29	CABLE		UPDATE	2016-10-05 17:39:24.875	johannorin	32A000025
565	30	CABLE		UPDATE	2016-10-05 17:39:24.895	johannorin	32A000026
566	31	CABLE		UPDATE	2016-10-05 17:39:24.93	johannorin	32A000027
567	32	CABLE		UPDATE	2016-10-05 17:39:24.942	johannorin	32A000028
568	33	CABLE		UPDATE	2016-10-05 17:39:24.952	johannorin	32G000029
569	34	CABLE		UPDATE	2016-10-05 17:39:24.976	johannorin	32A000030
570	35	CABLE		UPDATE	2016-10-05 17:39:24.986	johannorin	32A000031
571	36	CABLE		UPDATE	2016-10-05 17:39:24.997	johannorin	32A000032
572	37	CABLE		UPDATE	2016-10-05 17:39:25.008	johannorin	32G000033
573	38	CABLE		UPDATE	2016-10-05 17:39:25.02	johannorin	32A000034
574	39	CABLE		UPDATE	2016-10-05 17:39:25.035	johannorin	32A000035
575	40	CABLE		UPDATE	2016-10-05 17:39:25.047	johannorin	32A000036
576	41	CABLE		UPDATE	2016-10-05 17:39:25.058	johannorin	32A000037
577	42	CABLE		UPDATE	2016-10-05 17:39:25.069	johannorin	32A000038
578	43	CABLE		UPDATE	2016-10-05 17:39:25.081	johannorin	32G000039
579	44	CABLE		UPDATE	2016-10-05 17:39:25.093	johannorin	32A000040
580	45	CABLE		UPDATE	2016-10-05 17:39:25.104	johannorin	32A000041
581	46	CABLE		UPDATE	2016-10-05 17:39:25.115	johannorin	32A000042
582	47	CABLE		UPDATE	2016-10-05 17:39:25.128	johannorin	32A000043
583	48	CABLE		UPDATE	2016-10-05 17:39:25.141	johannorin	32G000044
584	49	CABLE		UPDATE	2016-10-05 17:39:25.153	johannorin	32A000045
585	50	CABLE		UPDATE	2016-10-05 17:39:25.174	johannorin	32A000046
586	51	CABLE		UPDATE	2016-10-05 17:39:25.187	johannorin	32A000047
587	52	CABLE		UPDATE	2016-10-05 17:39:25.199	johannorin	32A000048
588	53	CABLE		UPDATE	2016-10-05 17:39:25.211	johannorin	32A000049
589	54	CABLE		UPDATE	2016-10-05 17:39:25.225	johannorin	32A000050
590	55	CABLE		UPDATE	2016-10-05 17:39:25.237	johannorin	32A000051
591	56	CABLE		UPDATE	2016-10-05 17:39:25.251	johannorin	32A000052
592	57	CABLE		UPDATE	2016-10-05 17:39:25.269	johannorin	32A000053
593	58	CABLE		UPDATE	2016-10-05 17:39:25.282	johannorin	32A000054
594	59	CABLE		UPDATE	2016-10-05 17:39:25.297	johannorin	32G000055
595	60	CABLE		UPDATE	2016-10-05 17:39:25.311	johannorin	32G000056
596	61	CABLE		UPDATE	2016-10-05 17:39:25.323	johannorin	32G000057
597	62	CABLE		UPDATE	2016-10-05 17:39:25.336	johannorin	32G000058
598	63	CABLE		UPDATE	2016-10-05 17:39:25.36	johannorin	32G000059
599	64	CABLE		UPDATE	2016-10-05 17:39:25.388	johannorin	32G000060
600	65	CABLE		UPDATE	2016-10-05 17:39:25.433	johannorin	32A000061
601	66	CABLE		UPDATE	2016-10-05 17:39:25.47	johannorin	32C000062
602	67	CABLE		UPDATE	2016-10-05 17:39:25.506	johannorin	32E000063
603	68	CABLE		UPDATE	2016-10-05 17:39:25.54	johannorin	32A000064
604	69	CABLE		UPDATE	2016-10-05 17:39:25.577	johannorin	32A000065
605	70	CABLE		UPDATE	2016-10-05 17:39:25.621	johannorin	32E000066
606	71	CABLE		UPDATE	2016-10-05 17:39:25.658	johannorin	32D000067
607	72	CABLE		UPDATE	2016-10-05 17:39:25.693	johannorin	32E000068
608	73	CABLE		UPDATE	2016-10-05 17:39:25.729	johannorin	32D000069
609	74	CABLE		UPDATE	2016-10-05 17:39:25.765	johannorin	32A000070
610	75	CABLE		UPDATE	2016-10-05 17:39:25.8	johannorin	32A000071
611	76	CABLE		UPDATE	2016-10-05 17:39:25.849	johannorin	32E000072
612	77	CABLE		UPDATE	2016-10-05 17:39:25.884	johannorin	32D000073
613	78	CABLE		UPDATE	2016-10-05 17:39:25.918	johannorin	32E000074
614	79	CABLE		UPDATE	2016-10-05 17:39:25.952	johannorin	32D000075
615	80	CABLE		UPDATE	2016-10-05 17:39:25.963	johannorin	32A000076
616	81	CABLE		UPDATE	2016-10-05 17:39:25.975	johannorin	32G000077
617	82	CABLE		UPDATE	2016-10-05 17:39:25.988	johannorin	32K000078
618	83	CABLE		UPDATE	2016-10-05 17:39:26.003	johannorin	32K000079
619	84	CABLE		UPDATE	2016-10-05 17:39:26.017	johannorin	32K000080
620	85	CABLE		UPDATE	2016-10-05 17:39:26.031	johannorin	32K000081
621	86	CABLE		UPDATE	2016-10-05 17:39:26.055	johannorin	32E000082
622	87	CABLE		UPDATE	2016-10-05 17:39:26.083	johannorin	32E000083
623	88	CABLE	Deleted cable with number: 32L000084	DELETE	2016-10-05 17:39:26.092	johannorin	32L000084
624	9	CABLE		UPDATE	2016-10-05 17:46:16.715	johannorin	32A000005
625	10	CABLE		UPDATE	2016-10-05 17:46:16.756	johannorin	32A000006
626	11	CABLE		UPDATE	2016-10-05 17:46:16.784	johannorin	32A000007
627	12	CABLE		UPDATE	2016-10-05 17:46:16.806	johannorin	32A000008
628	13	CABLE		UPDATE	2016-10-05 17:46:16.828	johannorin	32A000009
629	14	CABLE		UPDATE	2016-10-05 17:46:16.856	johannorin	32A000010
630	15	CABLE		UPDATE	2016-10-05 17:46:16.881	johannorin	32A000011
631	16	CABLE		UPDATE	2016-10-05 17:46:16.907	johannorin	32A000012
632	17	CABLE		UPDATE	2016-10-05 17:46:16.934	johannorin	32A000013
633	18	CABLE		UPDATE	2016-10-05 17:46:17.108	johannorin	32A000014
634	19	CABLE		UPDATE	2016-10-05 17:46:17.135	johannorin	32A000015
635	20	CABLE		UPDATE	2016-10-05 17:46:17.162	johannorin	32A000016
636	21	CABLE		UPDATE	2016-10-05 17:46:17.192	johannorin	32A000017
637	22	CABLE		UPDATE	2016-10-05 17:46:17.227	johannorin	32A000018
638	23	CABLE		UPDATE	2016-10-05 17:46:17.25	johannorin	32A000019
639	24	CABLE		UPDATE	2016-10-05 17:46:17.274	johannorin	32A000020
640	25	CABLE		UPDATE	2016-10-05 17:46:17.3	johannorin	32A000021
641	26	CABLE		UPDATE	2016-10-05 17:46:17.321	johannorin	32A000022
642	27	CABLE		UPDATE	2016-10-05 17:46:17.344	johannorin	32A000023
643	28	CABLE		UPDATE	2016-10-05 17:46:17.368	johannorin	32A000024
644	29	CABLE		UPDATE	2016-10-05 17:46:17.394	johannorin	32A000025
645	30	CABLE		UPDATE	2016-10-05 17:46:17.421	johannorin	32A000026
646	31	CABLE		UPDATE	2016-10-05 17:46:17.449	johannorin	32A000027
647	37	CABLE		UPDATE	2016-10-05 17:46:17.464	johannorin	32A000033
648	38	CABLE		UPDATE	2016-10-05 17:46:17.491	johannorin	32G000034
649	39	CABLE		UPDATE	2016-10-05 17:46:17.508	johannorin	32A000035
650	40	CABLE		UPDATE	2016-10-05 17:46:17.523	johannorin	32A000036
651	41	CABLE		UPDATE	2016-10-05 17:46:17.541	johannorin	32A000037
652	32	CABLE		UPDATE	2016-10-05 17:46:17.556	johannorin	32A000028
653	33	CABLE		UPDATE	2016-10-05 17:46:17.573	johannorin	32G000029
654	34	CABLE		UPDATE	2016-10-05 17:46:17.588	johannorin	32A000030
655	35	CABLE		UPDATE	2016-10-05 17:46:17.604	johannorin	32A000031
656	36	CABLE		UPDATE	2016-10-05 17:46:17.619	johannorin	32A000032
657	42	CABLE		UPDATE	2016-10-05 17:46:17.633	johannorin	32A000038
658	43	CABLE		UPDATE	2016-10-05 17:46:17.645	johannorin	32G000039
659	44	CABLE		UPDATE	2016-10-05 17:46:17.658	johannorin	32A000040
660	45	CABLE		UPDATE	2016-10-05 17:46:17.671	johannorin	32A000041
661	46	CABLE		UPDATE	2016-10-05 17:46:17.685	johannorin	32A000042
662	47	CABLE		UPDATE	2016-10-05 17:46:17.699	johannorin	32A000043
663	48	CABLE		UPDATE	2016-10-05 17:46:17.711	johannorin	32G000044
664	49	CABLE		UPDATE	2016-10-05 17:46:17.727	johannorin	32A000045
665	50	CABLE		UPDATE	2016-10-05 17:46:17.749	johannorin	32A000046
666	51	CABLE		UPDATE	2016-10-05 17:46:17.774	johannorin	32A000047
667	52	CABLE		UPDATE	2016-10-05 17:46:17.792	johannorin	32A000048
668	53	CABLE		UPDATE	2016-10-05 17:46:17.828	johannorin	32A000049
669	54	CABLE		UPDATE	2016-10-05 17:46:17.847	johannorin	32A000050
670	55	CABLE		UPDATE	2016-10-05 17:46:17.861	johannorin	32A000051
671	56	CABLE		UPDATE	2016-10-05 17:46:17.875	johannorin	32A000052
672	57	CABLE		UPDATE	2016-10-05 17:46:17.889	johannorin	32A000053
673	58	CABLE		UPDATE	2016-10-05 17:46:17.902	johannorin	32A000054
674	59	CABLE		UPDATE	2016-10-05 17:46:17.916	johannorin	32G000055
675	60	CABLE		UPDATE	2016-10-05 17:46:17.931	johannorin	32G000056
676	61	CABLE		UPDATE	2016-10-05 17:46:17.946	johannorin	32G000057
677	62	CABLE		UPDATE	2016-10-05 17:46:17.964	johannorin	32G000058
678	63	CABLE		UPDATE	2016-10-05 17:46:17.978	johannorin	32G000059
679	64	CABLE		UPDATE	2016-10-05 17:46:17.993	johannorin	32G000060
680	65	CABLE		UPDATE	2016-10-05 17:46:18.03	johannorin	32A000061
681	66	CABLE		UPDATE	2016-10-05 17:46:18.067	johannorin	32C000062
682	67	CABLE		UPDATE	2016-10-05 17:46:18.11	johannorin	32E000063
683	68	CABLE		UPDATE	2016-10-05 17:46:18.16	johannorin	32A000064
684	69	CABLE		UPDATE	2016-10-05 17:46:18.203	johannorin	32A000065
685	70	CABLE		UPDATE	2016-10-05 17:46:18.239	johannorin	32E000066
686	71	CABLE		UPDATE	2016-10-05 17:46:18.277	johannorin	32D000067
687	72	CABLE		UPDATE	2016-10-05 17:46:18.316	johannorin	32E000068
688	73	CABLE		UPDATE	2016-10-05 17:46:18.356	johannorin	32D000069
689	74	CABLE		UPDATE	2016-10-05 17:46:18.4	johannorin	32A000070
690	75	CABLE		UPDATE	2016-10-05 17:46:18.44	johannorin	32A000071
691	76	CABLE		UPDATE	2016-10-05 17:46:18.478	johannorin	32E000072
692	77	CABLE		UPDATE	2016-10-05 17:46:18.516	johannorin	32D000073
693	78	CABLE		UPDATE	2016-10-05 17:46:18.554	johannorin	32E000074
694	79	CABLE		UPDATE	2016-10-05 17:46:18.593	johannorin	32D000075
695	80	CABLE		UPDATE	2016-10-05 17:46:18.604	johannorin	32G000076
696	81	CABLE		UPDATE	2016-10-05 17:46:18.616	johannorin	32G000077
697	82	CABLE		UPDATE	2016-10-05 17:46:18.633	johannorin	32B000078
698	83	CABLE		UPDATE	2016-10-05 17:46:18.664	johannorin	32B000079
699	84	CABLE		UPDATE	2016-10-05 17:46:18.699	johannorin	32G000080
700	85	CABLE		UPDATE	2016-10-05 17:46:18.717	johannorin	32G000081
701	86	CABLE		UPDATE	2016-10-05 17:46:18.769	johannorin	32B000082
702	87	CABLE		UPDATE	2016-10-05 17:46:18.802	johannorin	32B000083
703	9	CABLE	Validated cable with number: 32A000005.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.253	johannorin	32A000005
704	10	CABLE	Validated cable with number: 32A000006.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.33	johannorin	32A000006
705	11	CABLE	Validated cable with number: 32A000007.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.34	johannorin	32A000007
706	12	CABLE	Validated cable with number: 32A000008.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.348	johannorin	32A000008
707	13	CABLE	Validated cable with number: 32A000009.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.356	johannorin	32A000009
708	14	CABLE	Validated cable with number: 32A000010.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.363	johannorin	32A000010
709	15	CABLE	Validated cable with number: 32A000011.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.371	johannorin	32A000011
710	16	CABLE	Validated cable with number: 32A000012.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.378	johannorin	32A000012
711	17	CABLE	Validated cable with number: 32A000013.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.386	johannorin	32A000013
712	18	CABLE	Validated cable with number: 32A000014.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.399	johannorin	32A000014
713	19	CABLE	Validated cable with number: 32A000015.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.408	johannorin	32A000015
714	20	CABLE	Validated cable with number: 32A000016.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.421	johannorin	32A000016
715	21	CABLE	Validated cable with number: 32A000017.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.435	johannorin	32A000017
716	22	CABLE	Validated cable with number: 32A000018.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.448	johannorin	32A000018
717	23	CABLE	Validated cable with number: 32A000019.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.456	johannorin	32A000019
718	24	CABLE	Validated cable with number: 32A000020.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.465	johannorin	32A000020
719	25	CABLE	Validated cable with number: 32A000021.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.472	johannorin	32A000021
720	26	CABLE	Validated cable with number: 32A000022.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.48	johannorin	32A000022
721	27	CABLE	Validated cable with number: 32A000023.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.489	johannorin	32A000023
722	28	CABLE	Validated cable with number: 32A000024.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.5	johannorin	32A000024
723	29	CABLE	Validated cable with number: 32A000025.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.516	johannorin	32A000025
724	30	CABLE	Validated cable with number: 32A000026.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.536	johannorin	32A000026
725	31	CABLE	Validated cable with number: 32A000027.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.557	johannorin	32A000027
726	50	CABLE	Validated cable with number: 32A000046.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.585	johannorin	32A000046
727	51	CABLE	Validated cable with number: 32A000047.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.602	johannorin	32A000047
728	52	CABLE	Validated cable with number: 32A000048.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.624	johannorin	32A000048
729	53	CABLE	Validated cable with number: 32A000049.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.64	johannorin	32A000049
730	37	CABLE	Validated cable with number: 32A000033.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.65	johannorin	32A000033
731	38	CABLE	Validated cable with number: 32G000034.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.66	johannorin	32G000034
732	39	CABLE	Validated cable with number: 32A000035.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.67	johannorin	32A000035
733	40	CABLE	Validated cable with number: 32A000036.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.68	johannorin	32A000036
734	41	CABLE	Validated cable with number: 32A000037.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.689	johannorin	32A000037
735	54	CABLE	Validated cable with number: 32A000050.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.699	johannorin	32A000050
736	55	CABLE	Validated cable with number: 32A000051.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.713	johannorin	32A000051
737	56	CABLE	Validated cable with number: 32A000052.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.723	johannorin	32A000052
830	57	CABLE		UPDATE	2016-10-05 18:11:37.987	johannorin	32A000053
738	57	CABLE	Validated cable with number: 32A000053.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.732	johannorin	32A000053
739	58	CABLE	Validated cable with number: 32A000054.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.742	johannorin	32A000054
740	59	CABLE	Validated cable with number: 32G000055.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.752	johannorin	32G000055
741	60	CABLE	Validated cable with number: 32G000056.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.762	johannorin	32G000056
742	61	CABLE	Validated cable with number: 32G000057.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.771	johannorin	32G000057
743	62	CABLE	Validated cable with number: 32G000058.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.779	johannorin	32G000058
744	63	CABLE	Validated cable with number: 32G000059.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.788	johannorin	32G000059
745	64	CABLE	Validated cable with number: 32G000060.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.796	johannorin	32G000060
746	65	CABLE	Validated cable with number: 32A000061.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.802	johannorin	32A000061
747	66	CABLE	Validated cable with number: 32C000062.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.809	johannorin	32C000062
748	67	CABLE	Validated cable with number: 32E000063.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.816	johannorin	32E000063
749	68	CABLE	Validated cable with number: 32A000064.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.831	johannorin	32A000064
750	69	CABLE	Validated cable with number: 32A000065.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.84	johannorin	32A000065
751	70	CABLE	Validated cable with number: 32E000066.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.847	johannorin	32E000066
752	71	CABLE	Validated cable with number: 32D000067.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.855	johannorin	32D000067
753	72	CABLE	Validated cable with number: 32E000068.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.861	johannorin	32E000068
754	73	CABLE	Validated cable with number: 32D000069.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.868	johannorin	32D000069
755	74	CABLE	Validated cable with number: 32A000070.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.875	johannorin	32A000070
756	75	CABLE	Validated cable with number: 32A000071.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.882	johannorin	32A000071
757	76	CABLE	Validated cable with number: 32E000072.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.889	johannorin	32E000072
758	77	CABLE	Validated cable with number: 32D000073.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.896	johannorin	32D000073
759	78	CABLE	Validated cable with number: 32E000074.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.902	johannorin	32E000074
760	79	CABLE	Validated cable with number: 32D000075.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.909	johannorin	32D000075
761	80	CABLE	Validated cable with number: 32G000076.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.917	johannorin	32G000076
762	81	CABLE	Validated cable with number: 32G000077.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.924	johannorin	32G000077
763	82	CABLE	Validated cable with number: 32B000078.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.932	johannorin	32B000078
764	83	CABLE	Validated cable with number: 32B000079.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.94	johannorin	32B000079
765	84	CABLE	Validated cable with number: 32G000080.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.947	johannorin	32G000080
766	85	CABLE	Validated cable with number: 32G000081.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.956	johannorin	32G000081
767	86	CABLE	Validated cable with number: 32B000082.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.963	johannorin	32B000082
831	58	CABLE		UPDATE	2016-10-05 18:11:37.998	johannorin	32A000054
768	87	CABLE	Validated cable with number: 32B000083.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.971	johannorin	32B000083
769	32	CABLE	Validated cable with number: 32A000028.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.977	johannorin	32A000028
770	33	CABLE	Validated cable with number: 32G000029.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.984	johannorin	32G000029
771	34	CABLE	Validated cable with number: 32A000030.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:30.996	johannorin	32A000030
772	35	CABLE	Validated cable with number: 32A000031.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.002	johannorin	32A000031
773	36	CABLE	Validated cable with number: 32A000032.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.009	johannorin	32A000032
774	42	CABLE	Validated cable with number: 32A000038.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.016	johannorin	32A000038
775	43	CABLE	Validated cable with number: 32G000039.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.023	johannorin	32G000039
776	44	CABLE	Validated cable with number: 32A000040.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.03	johannorin	32A000040
777	45	CABLE	Validated cable with number: 32A000041.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.037	johannorin	32A000041
778	46	CABLE	Validated cable with number: 32A000042.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.044	johannorin	32A000042
779	47	CABLE	Validated cable with number: 32A000043.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.05	johannorin	32A000043
780	48	CABLE	Validated cable with number: 32G000044.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.061	johannorin	32G000044
781	49	CABLE	Validated cable with number: 32A000045.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-05 17:50:31.069	johannorin	32A000045
782	9	CABLE		UPDATE	2016-10-05 18:11:37.133	johannorin	32A000005
783	10	CABLE		UPDATE	2016-10-05 18:11:37.171	johannorin	32A000006
784	11	CABLE		UPDATE	2016-10-05 18:11:37.201	johannorin	32A000007
785	12	CABLE		UPDATE	2016-10-05 18:11:37.231	johannorin	32A000008
786	13	CABLE		UPDATE	2016-10-05 18:11:37.272	johannorin	32A000009
787	14	CABLE		UPDATE	2016-10-05 18:11:37.314	johannorin	32A000010
788	15	CABLE		UPDATE	2016-10-05 18:11:37.338	johannorin	32A000011
789	16	CABLE		UPDATE	2016-10-05 18:11:37.356	johannorin	32A000012
790	17	CABLE		UPDATE	2016-10-05 18:11:37.375	johannorin	32A000013
791	18	CABLE		UPDATE	2016-10-05 18:11:37.409	johannorin	32A000014
792	19	CABLE		UPDATE	2016-10-05 18:11:37.428	johannorin	32A000015
793	20	CABLE		UPDATE	2016-10-05 18:11:37.447	johannorin	32A000016
794	21	CABLE		UPDATE	2016-10-05 18:11:37.466	johannorin	32A000017
795	22	CABLE		UPDATE	2016-10-05 18:11:37.487	johannorin	32A000018
796	23	CABLE		UPDATE	2016-10-05 18:11:37.506	johannorin	32A000019
797	24	CABLE		UPDATE	2016-10-05 18:11:37.526	johannorin	32A000020
798	25	CABLE		UPDATE	2016-10-05 18:11:37.546	johannorin	32A000021
799	26	CABLE		UPDATE	2016-10-05 18:11:37.567	johannorin	32A000022
800	27	CABLE		UPDATE	2016-10-05 18:11:37.589	johannorin	32A000023
801	28	CABLE		UPDATE	2016-10-05 18:11:37.61	johannorin	32A000024
802	29	CABLE		UPDATE	2016-10-05 18:11:37.631	johannorin	32A000025
803	30	CABLE		UPDATE	2016-10-05 18:11:37.654	johannorin	32A000026
804	31	CABLE		UPDATE	2016-10-05 18:11:37.675	johannorin	32A000027
805	32	CABLE		UPDATE	2016-10-05 18:11:37.685	johannorin	32A000028
806	33	CABLE		UPDATE	2016-10-05 18:11:37.696	johannorin	32G000029
807	34	CABLE		UPDATE	2016-10-05 18:11:37.714	johannorin	32A000030
808	35	CABLE		UPDATE	2016-10-05 18:11:37.726	johannorin	32A000031
809	36	CABLE		UPDATE	2016-10-05 18:11:37.738	johannorin	32A000032
810	37	CABLE		UPDATE	2016-10-05 18:11:37.75	johannorin	32A000033
811	38	CABLE		UPDATE	2016-10-05 18:11:37.762	johannorin	32G000034
812	39	CABLE		UPDATE	2016-10-05 18:11:37.774	johannorin	32A000035
813	40	CABLE		UPDATE	2016-10-05 18:11:37.786	johannorin	32A000036
814	41	CABLE		UPDATE	2016-10-05 18:11:37.799	johannorin	32A000037
815	42	CABLE		UPDATE	2016-10-05 18:11:37.812	johannorin	32A000038
816	43	CABLE		UPDATE	2016-10-05 18:11:37.824	johannorin	32G000039
817	44	CABLE		UPDATE	2016-10-05 18:11:37.836	johannorin	32A000040
818	45	CABLE		UPDATE	2016-10-05 18:11:37.848	johannorin	32A000041
819	46	CABLE		UPDATE	2016-10-05 18:11:37.861	johannorin	32A000042
820	47	CABLE		UPDATE	2016-10-05 18:11:37.873	johannorin	32A000043
821	48	CABLE		UPDATE	2016-10-05 18:11:37.885	johannorin	32G000044
822	49	CABLE		UPDATE	2016-10-05 18:11:37.895	johannorin	32A000045
823	50	CABLE		UPDATE	2016-10-05 18:11:37.907	johannorin	32A000046
824	51	CABLE		UPDATE	2016-10-05 18:11:37.919	johannorin	32A000047
825	52	CABLE		UPDATE	2016-10-05 18:11:37.931	johannorin	32A000048
826	53	CABLE		UPDATE	2016-10-05 18:11:37.942	johannorin	32A000049
827	54	CABLE		UPDATE	2016-10-05 18:11:37.953	johannorin	32A000050
828	55	CABLE		UPDATE	2016-10-05 18:11:37.964	johannorin	32A000051
829	56	CABLE		UPDATE	2016-10-05 18:11:37.976	johannorin	32A000052
832	59	CABLE		UPDATE	2016-10-05 18:11:38.01	johannorin	32G000055
833	60	CABLE		UPDATE	2016-10-05 18:11:38.022	johannorin	32G000056
834	61	CABLE		UPDATE	2016-10-05 18:11:38.035	johannorin	32G000057
835	62	CABLE		UPDATE	2016-10-05 18:11:38.047	johannorin	32G000058
836	63	CABLE		UPDATE	2016-10-05 18:11:38.061	johannorin	32G000059
837	64	CABLE		UPDATE	2016-10-05 18:11:38.073	johannorin	32G000060
838	65	CABLE		UPDATE	2016-10-05 18:11:38.105	johannorin	32A000061
839	66	CABLE		UPDATE	2016-10-05 18:11:38.137	johannorin	32C000062
840	67	CABLE		UPDATE	2016-10-05 18:11:38.17	johannorin	32E000063
841	68	CABLE		UPDATE	2016-10-05 18:11:38.205	johannorin	32A000064
842	69	CABLE		UPDATE	2016-10-05 18:11:38.242	johannorin	32A000065
843	70	CABLE		UPDATE	2016-10-05 18:11:38.276	johannorin	32E000066
844	71	CABLE		UPDATE	2016-10-05 18:11:38.311	johannorin	32D000067
845	72	CABLE		UPDATE	2016-10-05 18:11:38.345	johannorin	32E000068
846	73	CABLE		UPDATE	2016-10-05 18:11:38.377	johannorin	32D000069
847	74	CABLE		UPDATE	2016-10-05 18:11:38.41	johannorin	32A000070
848	75	CABLE		UPDATE	2016-10-05 18:11:38.443	johannorin	32A000071
849	76	CABLE		UPDATE	2016-10-05 18:11:38.476	johannorin	32E000072
850	77	CABLE		UPDATE	2016-10-05 18:11:38.509	johannorin	32D000073
851	78	CABLE		UPDATE	2016-10-05 18:11:38.542	johannorin	32E000074
852	79	CABLE		UPDATE	2016-10-05 18:11:38.576	johannorin	32D000075
853	80	CABLE		UPDATE	2016-10-05 18:11:38.587	johannorin	32G000076
854	81	CABLE		UPDATE	2016-10-05 18:11:38.597	johannorin	32G000077
855	82	CABLE		UPDATE	2016-10-05 18:11:38.612	johannorin	32B000078
856	83	CABLE		UPDATE	2016-10-05 18:11:38.626	johannorin	32B000079
857	84	CABLE		UPDATE	2016-10-05 18:11:38.639	johannorin	32G000080
858	85	CABLE		UPDATE	2016-10-05 18:11:38.654	johannorin	32G000081
859	86	CABLE		UPDATE	2016-10-05 18:11:38.679	johannorin	32B000082
860	87	CABLE		UPDATE	2016-10-05 18:11:38.715	johannorin	32B000083
861	89	CABLE	Created cable with number: 22A000085	CREATE	2016-10-10 11:04:05.265	johannorin	22A000085
862	90	CABLE	Created cable with number: 22A000086	CREATE	2016-10-10 11:04:05.314	johannorin	22A000086
863	91	CABLE	Created cable with number: 22A000087	CREATE	2016-10-10 11:04:05.363	johannorin	22A000087
864	92	CABLE	Created cable with number: 22A000088	CREATE	2016-10-10 11:04:05.393	johannorin	22A000088
865	93	CABLE	Created cable with number: 22A000089	CREATE	2016-10-10 11:04:05.421	johannorin	22A000089
866	94	CABLE	Created cable with number: 22A000090	CREATE	2016-10-10 11:04:05.448	johannorin	22A000090
867	95	CABLE	Created cable with number: 22A000091	CREATE	2016-10-10 11:04:05.478	johannorin	22A000091
868	96	CABLE	Created cable with number: 22A000092	CREATE	2016-10-10 11:04:05.507	johannorin	22A000092
869	97	CABLE	Created cable with number: 22A000093	CREATE	2016-10-10 11:04:05.535	johannorin	22A000093
870	98	CABLE	Created cable with number: 22A000094	CREATE	2016-10-10 11:04:05.563	johannorin	22A000094
871	99	CABLE	Created cable with number: 22A000095	CREATE	2016-10-10 11:04:05.595	johannorin	22A000095
872	100	CABLE	Created cable with number: 22A000096	CREATE	2016-10-10 11:04:05.625	johannorin	22A000096
873	101	CABLE	Created cable with number: 22A000097	CREATE	2016-10-10 11:04:05.652	johannorin	22A000097
874	102	CABLE	Created cable with number: 22A000098	CREATE	2016-10-10 11:04:05.677	johannorin	22A000098
875	103	CABLE	Created cable with number: 22A000099	CREATE	2016-10-10 11:04:05.702	johannorin	22A000099
876	104	CABLE	Created cable with number: 22A000100	CREATE	2016-10-10 11:04:05.729	johannorin	22A000100
877	105	CABLE	Created cable with number: 22A000101	CREATE	2016-10-10 11:04:05.755	johannorin	22A000101
878	106	CABLE	Created cable with number: 22A000102	CREATE	2016-10-10 11:04:05.785	johannorin	22A000102
879	107	CABLE	Created cable with number: 22A000103	CREATE	2016-10-10 11:04:05.819	johannorin	22A000103
880	108	CABLE	Created cable with number: 22A000104	CREATE	2016-10-10 11:04:05.854	johannorin	22A000104
881	109	CABLE	Created cable with number: 22A000105	CREATE	2016-10-10 11:04:05.883	johannorin	22A000105
882	110	CABLE	Created cable with number: 22A000106	CREATE	2016-10-10 11:04:05.911	johannorin	22A000106
883	111	CABLE	Created cable with number: 22A000107	CREATE	2016-10-10 11:04:05.939	johannorin	22A000107
884	112	CABLE	Created cable with number: 22A000108	CREATE	2016-10-10 11:04:05.966	johannorin	22A000108
885	113	CABLE	Created cable with number: 22A000109	CREATE	2016-10-10 11:04:05.993	johannorin	22A000109
886	114	CABLE	Created cable with number: 22A000110	CREATE	2016-10-10 11:04:06.021	johannorin	22A000110
887	115	CABLE	Created cable with number: 22A000111	CREATE	2016-10-10 11:04:06.047	johannorin	22A000111
888	116	CABLE	Created cable with number: 22A000112	CREATE	2016-10-10 11:04:06.073	johannorin	22A000112
889	117	CABLE	Created cable with number: 22A000113	CREATE	2016-10-10 11:04:06.103	johannorin	22A000113
890	118	CABLE	Created cable with number: 22A000114	CREATE	2016-10-10 11:04:06.13	johannorin	22A000114
891	119	CABLE	Created cable with number: 22A000115	CREATE	2016-10-10 11:04:06.162	johannorin	22A000115
892	120	CABLE	Created cable with number: 22A000116	CREATE	2016-10-10 11:04:06.188	johannorin	22A000116
893	121	CABLE	Created cable with number: 22A000117	CREATE	2016-10-10 11:04:06.217	johannorin	22A000117
894	122	CABLE	Created cable with number: 22A000118	CREATE	2016-10-10 11:04:06.245	johannorin	22A000118
895	123	CABLE	Created cable with number: 22A000119	CREATE	2016-10-10 11:04:06.271	johannorin	22A000119
896	124	CABLE	Created cable with number: 22A000120	CREATE	2016-10-10 11:04:06.297	johannorin	22A000120
897	125	CABLE	Created cable with number: 22A000121	CREATE	2016-10-10 11:04:06.324	johannorin	22A000121
898	126	CABLE	Created cable with number: 22A000122	CREATE	2016-10-10 11:04:06.351	johannorin	22A000122
899	127	CABLE	Created cable with number: 22A000123	CREATE	2016-10-10 11:04:06.38	johannorin	22A000123
900	128	CABLE	Created cable with number: 22A000124	CREATE	2016-10-10 11:04:06.408	johannorin	22A000124
901	129	CABLE	Created cable with number: 22A000125	CREATE	2016-10-10 11:04:06.436	johannorin	22A000125
902	130	CABLE	Created cable with number: 22A000126	CREATE	2016-10-10 11:04:06.465	johannorin	22A000126
903	131	CABLE	Created cable with number: 22A000127	CREATE	2016-10-10 11:04:06.498	johannorin	22A000127
904	132	CABLE	Created cable with number: 22A000128	CREATE	2016-10-10 11:04:06.535	johannorin	22A000128
905	133	CABLE	Created cable with number: 22A000129	CREATE	2016-10-10 11:04:06.585	johannorin	22A000129
906	134	CABLE	Created cable with number: 22A000130	CREATE	2016-10-10 11:04:06.638	johannorin	22A000130
907	135	CABLE	Created cable with number: 22A000131	CREATE	2016-10-10 11:04:06.677	johannorin	22A000131
908	136	CABLE	Created cable with number: 22A000132	CREATE	2016-10-10 11:04:06.722	johannorin	22A000132
909	137	CABLE	Created cable with number: 22A000133	CREATE	2016-10-10 11:04:06.751	johannorin	22A000133
910	138	CABLE	Created cable with number: 22A000134	CREATE	2016-10-10 11:04:06.779	johannorin	22A000134
911	139	CABLE	Created cable with number: 22A000135	CREATE	2016-10-10 11:04:06.807	johannorin	22A000135
912	140	CABLE	Created cable with number: 22A000136	CREATE	2016-10-10 11:04:06.834	johannorin	22A000136
913	141	CABLE	Created cable with number: 22A000137	CREATE	2016-10-10 11:04:06.864	johannorin	22A000137
914	142	CABLE	Created cable with number: 22A000138	CREATE	2016-10-10 11:04:06.896	johannorin	22A000138
915	143	CABLE	Created cable with number: 22A000139	CREATE	2016-10-10 11:04:06.927	johannorin	22A000139
916	144	CABLE	Created cable with number: 22A000140	CREATE	2016-10-10 11:04:06.957	johannorin	22A000140
917	145	CABLE	Created cable with number: 22A000141	CREATE	2016-10-10 11:04:06.985	johannorin	22A000141
918	146	CABLE	Created cable with number: 22A000142	CREATE	2016-10-10 11:04:07.014	johannorin	22A000142
919	147	CABLE	Created cable with number: 22A000143	CREATE	2016-10-10 11:04:07.043	johannorin	22A000143
920	148	CABLE	Created cable with number: 22A000144	CREATE	2016-10-10 11:04:07.072	johannorin	22A000144
921	149	CABLE	Created cable with number: 22A000145	CREATE	2016-10-10 11:04:07.102	johannorin	22A000145
922	150	CABLE	Created cable with number: 22A000146	CREATE	2016-10-10 11:04:07.134	johannorin	22A000146
923	151	CABLE	Created cable with number: 22A000147	CREATE	2016-10-10 11:04:07.164	johannorin	22A000147
924	152	CABLE	Created cable with number: 22A000148	CREATE	2016-10-10 11:04:07.194	johannorin	22A000148
925	153	CABLE	Created cable with number: 22A000149	CREATE	2016-10-10 11:04:07.272	johannorin	22A000149
926	154	CABLE	Created cable with number: 22A000150	CREATE	2016-10-10 11:04:07.3	johannorin	22A000150
927	155	CABLE	Created cable with number: 22A000151	CREATE	2016-10-10 11:04:07.329	johannorin	22A000151
928	156	CABLE	Created cable with number: 22A000152	CREATE	2016-10-10 11:04:07.36	johannorin	22A000152
929	157	CABLE	Created cable with number: 22A000153	CREATE	2016-10-10 11:04:07.391	johannorin	22A000153
930	158	CABLE	Created cable with number: 22A000154	CREATE	2016-10-10 11:04:07.423	johannorin	22A000154
931	159	CABLE	Created cable with number: 22A000155	CREATE	2016-10-10 11:04:07.466	johannorin	22A000155
932	160	CABLE	Created cable with number: 22A000156	CREATE	2016-10-10 11:04:07.54	johannorin	22A000156
933	161	CABLE	Created cable with number: 22A000157	CREATE	2016-10-10 11:04:07.588	johannorin	22A000157
934	162	CABLE	Created cable with number: 22A000158	CREATE	2016-10-10 11:04:07.618	johannorin	22A000158
935	163	CABLE	Created cable with number: 22A000159	CREATE	2016-10-10 11:04:07.649	johannorin	22A000159
936	164	CABLE	Created cable with number: 22A000160	CREATE	2016-10-10 11:04:07.682	johannorin	22A000160
937	165	CABLE	Created cable with number: 22A000161	CREATE	2016-10-10 11:04:07.712	johannorin	22A000161
938	166	CABLE	Created cable with number: 22A000162	CREATE	2016-10-10 11:04:07.743	johannorin	22A000162
939	167	CABLE	Created cable with number: 22A000163	CREATE	2016-10-10 11:04:07.777	johannorin	22A000163
940	168	CABLE	Created cable with number: 22A000164	CREATE	2016-10-10 11:04:07.813	johannorin	22A000164
941	169	CABLE	Created cable with number: 22A000165	CREATE	2016-10-10 11:04:07.849	johannorin	22A000165
942	170	CABLE	Created cable with number: 22A000166	CREATE	2016-10-10 11:04:07.882	johannorin	22A000166
943	171	CABLE	Created cable with number: 22A000167	CREATE	2016-10-10 11:04:07.914	johannorin	22A000167
944	172	CABLE	Created cable with number: 22A000168	CREATE	2016-10-10 11:04:07.947	johannorin	22A000168
945	173	CABLE	Created cable with number: 22A000169	CREATE	2016-10-10 11:04:07.979	johannorin	22A000169
946	174	CABLE	Created cable with number: 22A000170	CREATE	2016-10-10 11:04:08.01	johannorin	22A000170
947	175	CABLE	Created cable with number: 22A000171	CREATE	2016-10-10 11:04:08.042	johannorin	22A000171
948	176	CABLE	Created cable with number: 22A000172	CREATE	2016-10-10 11:04:08.077	johannorin	22A000172
949	12	CABLE		UPDATE	2016-10-10 11:04:08.128	johannorin	32A000008
950	9	CABLE		UPDATE	2016-10-10 11:04:08.179	johannorin	32A000005
951	11	CABLE		UPDATE	2016-10-10 11:04:08.253	johannorin	32A000007
952	10	CABLE		UPDATE	2016-10-10 11:04:08.291	johannorin	32A000006
953	16	CABLE		UPDATE	2016-10-10 11:04:08.329	johannorin	32A000012
954	13	CABLE		UPDATE	2016-10-10 11:04:08.373	johannorin	32A000009
955	15	CABLE		UPDATE	2016-10-10 11:04:08.41	johannorin	32A000011
956	14	CABLE		UPDATE	2016-10-10 11:04:08.447	johannorin	32A000010
957	21	CABLE		UPDATE	2016-10-10 11:04:08.485	johannorin	32A000017
958	18	CABLE		UPDATE	2016-10-10 11:04:08.523	johannorin	32A000014
959	20	CABLE		UPDATE	2016-10-10 11:04:08.562	johannorin	32A000016
960	19	CABLE		UPDATE	2016-10-10 11:04:08.598	johannorin	32A000015
961	25	CABLE		UPDATE	2016-10-10 11:04:08.637	johannorin	32A000021
962	22	CABLE		UPDATE	2016-10-10 11:04:08.674	johannorin	32A000018
963	24	CABLE		UPDATE	2016-10-10 11:04:08.71	johannorin	32A000020
964	23	CABLE		UPDATE	2016-10-10 11:04:08.758	johannorin	32A000019
965	30	CABLE		UPDATE	2016-10-10 11:04:08.796	johannorin	32A000026
966	27	CABLE		UPDATE	2016-10-10 11:04:08.838	johannorin	32A000023
967	29	CABLE		UPDATE	2016-10-10 11:04:08.894	johannorin	32A000025
968	28	CABLE		UPDATE	2016-10-10 11:04:08.937	johannorin	32A000024
969	177	CABLE	Created cable with number: 32A000173	CREATE	2016-10-10 11:04:08.971	johannorin	32A000173
970	178	CABLE	Created cable with number: 32A000174	CREATE	2016-10-10 11:04:09.004	johannorin	32A000174
971	179	CABLE	Created cable with number: 32A000175	CREATE	2016-10-10 11:04:09.038	johannorin	32A000175
972	180	CABLE	Created cable with number: 32A000176	CREATE	2016-10-10 11:04:09.09	johannorin	32A000176
973	181	CABLE	Created cable with number: 32A000177	CREATE	2016-10-10 11:04:09.146	johannorin	32A000177
974	182	CABLE	Created cable with number: 32A000178	CREATE	2016-10-10 11:04:09.179	johannorin	32A000178
975	183	CABLE	Created cable with number: 32A000179	CREATE	2016-10-10 11:04:09.213	johannorin	32A000179
976	184	CABLE	Created cable with number: 32A000180	CREATE	2016-10-10 11:04:09.256	johannorin	32A000180
977	185	CABLE	Created cable with number: 32A000181	CREATE	2016-10-10 11:04:09.313	johannorin	32A000181
978	186	CABLE	Created cable with number: 32A000182	CREATE	2016-10-10 11:04:09.351	johannorin	32A000182
979	187	CABLE	Created cable with number: 32A000183	CREATE	2016-10-10 11:04:09.419	johannorin	32A000183
980	188	CABLE	Created cable with number: 32A000184	CREATE	2016-10-10 11:04:09.462	johannorin	32A000184
981	189	CABLE	Created cable with number: 32A000185	CREATE	2016-10-10 11:04:09.535	johannorin	32A000185
982	190	CABLE	Created cable with number: 32A000186	CREATE	2016-10-10 11:04:09.568	johannorin	32A000186
983	191	CABLE	Created cable with number: 32A000187	CREATE	2016-10-10 11:04:09.602	johannorin	32A000187
984	192	CABLE	Created cable with number: 32A000188	CREATE	2016-10-10 11:04:09.64	johannorin	32A000188
985	193	CABLE	Created cable with number: 32A000189	CREATE	2016-10-10 11:04:09.675	johannorin	32A000189
986	194	CABLE	Created cable with number: 32A000190	CREATE	2016-10-10 11:04:09.71	johannorin	32A000190
987	195	CABLE	Created cable with number: 32A000191	CREATE	2016-10-10 11:04:09.745	johannorin	32A000191
988	196	CABLE	Created cable with number: 32A000192	CREATE	2016-10-10 11:04:09.779	johannorin	32A000192
989	197	CABLE	Created cable with number: 32A000193	CREATE	2016-10-10 11:04:09.817	johannorin	32A000193
990	198	CABLE	Created cable with number: 32A000194	CREATE	2016-10-10 11:04:09.858	johannorin	32A000194
991	199	CABLE	Created cable with number: 32A000195	CREATE	2016-10-10 11:04:09.894	johannorin	32A000195
992	200	CABLE	Created cable with number: 32A000196	CREATE	2016-10-10 11:04:09.935	johannorin	32A000196
993	201	CABLE	Created cable with number: 32A000197	CREATE	2016-10-10 11:04:09.968	johannorin	32A000197
994	202	CABLE	Created cable with number: 32A000198	CREATE	2016-10-10 11:04:10.004	johannorin	32A000198
995	203	CABLE	Created cable with number: 32A000199	CREATE	2016-10-10 11:04:10.043	johannorin	32A000199
996	204	CABLE	Created cable with number: 32A000200	CREATE	2016-10-10 11:04:10.088	johannorin	32A000200
997	205	CABLE	Created cable with number: 32A000201	CREATE	2016-10-10 11:04:10.128	johannorin	32A000201
998	206	CABLE	Created cable with number: 32A000202	CREATE	2016-10-10 11:04:10.164	johannorin	32A000202
999	207	CABLE	Created cable with number: 32A000203	CREATE	2016-10-10 11:04:10.199	johannorin	32A000203
1000	208	CABLE	Created cable with number: 32A000204	CREATE	2016-10-10 11:04:10.235	johannorin	32A000204
1001	209	CABLE	Created cable with number: 32A000205	CREATE	2016-10-10 11:04:10.273	johannorin	32A000205
1002	210	CABLE	Created cable with number: 32A000206	CREATE	2016-10-10 11:04:10.31	johannorin	32A000206
1003	211	CABLE	Created cable with number: 32A000207	CREATE	2016-10-10 11:04:10.351	johannorin	32A000207
1004	212	CABLE	Created cable with number: 32A000208	CREATE	2016-10-10 11:04:10.394	johannorin	32A000208
1005	213	CABLE	Created cable with number: 32A000209	CREATE	2016-10-10 11:04:10.433	johannorin	32A000209
1006	214	CABLE	Created cable with number: 32A000210	CREATE	2016-10-10 11:04:10.475	johannorin	32A000210
1007	215	CABLE	Created cable with number: 32A000211	CREATE	2016-10-10 11:04:10.515	johannorin	32A000211
1008	216	CABLE	Created cable with number: 32A000212	CREATE	2016-10-10 11:04:10.553	johannorin	32A000212
1009	217	CABLE	Created cable with number: 32A000213	CREATE	2016-10-10 11:04:10.596	johannorin	32A000213
1010	218	CABLE	Created cable with number: 32A000214	CREATE	2016-10-10 11:04:10.634	johannorin	32A000214
1011	219	CABLE	Created cable with number: 32A000215	CREATE	2016-10-10 11:04:10.673	johannorin	32A000215
1012	220	CABLE	Created cable with number: 32A000216	CREATE	2016-10-10 11:04:10.71	johannorin	32A000216
1013	221	CABLE	Created cable with number: 32A000217	CREATE	2016-10-10 11:04:10.746	johannorin	32A000217
1014	222	CABLE	Created cable with number: 32A000218	CREATE	2016-10-10 11:04:10.783	johannorin	32A000218
1015	223	CABLE	Created cable with number: 32A000219	CREATE	2016-10-10 11:04:10.82	johannorin	32A000219
1016	224	CABLE	Created cable with number: 32A000220	CREATE	2016-10-10 11:04:10.858	johannorin	32A000220
1017	225	CABLE	Created cable with number: 32A000221	CREATE	2016-10-10 11:04:10.895	johannorin	32A000221
1018	226	CABLE	Created cable with number: 32A000222	CREATE	2016-10-10 11:04:10.933	johannorin	32A000222
1019	227	CABLE	Created cable with number: 32A000223	CREATE	2016-10-10 11:04:10.985	johannorin	32A000223
1020	228	CABLE	Created cable with number: 32A000224	CREATE	2016-10-10 11:04:11.056	johannorin	32A000224
1021	229	CABLE	Created cable with number: 32A000225	CREATE	2016-10-10 11:04:11.095	johannorin	32A000225
1022	230	CABLE	Created cable with number: 32A000226	CREATE	2016-10-10 11:04:11.136	johannorin	32A000226
1023	231	CABLE	Created cable with number: 32A000227	CREATE	2016-10-10 11:04:11.176	johannorin	32A000227
1024	232	CABLE	Created cable with number: 32A000228	CREATE	2016-10-10 11:04:11.218	johannorin	32A000228
1025	233	CABLE	Created cable with number: 32A000229	CREATE	2016-10-10 11:04:11.257	johannorin	32A000229
1026	234	CABLE	Created cable with number: 32A000230	CREATE	2016-10-10 11:04:11.295	johannorin	32A000230
1027	235	CABLE	Created cable with number: 32A000231	CREATE	2016-10-10 11:04:11.333	johannorin	32A000231
1028	236	CABLE	Created cable with number: 32A000232	CREATE	2016-10-10 11:04:11.374	johannorin	32A000232
1029	237	CABLE	Created cable with number: 32A000233	CREATE	2016-10-10 11:04:11.415	johannorin	32A000233
1030	238	CABLE	Created cable with number: 32A000234	CREATE	2016-10-10 11:04:11.453	johannorin	32A000234
1031	239	CABLE	Created cable with number: 32A000235	CREATE	2016-10-10 11:04:11.511	johannorin	32A000235
1032	240	CABLE	Created cable with number: 32A000236	CREATE	2016-10-10 11:04:11.557	johannorin	32A000236
1033	241	CABLE	Created cable with number: 32A000237	CREATE	2016-10-10 11:04:11.599	johannorin	32A000237
1034	242	CABLE	Created cable with number: 32A000238	CREATE	2016-10-10 11:04:11.636	johannorin	32A000238
1035	243	CABLE	Created cable with number: 32A000239	CREATE	2016-10-10 11:04:11.684	johannorin	32A000239
1036	244	CABLE	Created cable with number: 32A000240	CREATE	2016-10-10 11:04:11.727	johannorin	32A000240
1037	245	CABLE	Created cable with number: 32A000241	CREATE	2016-10-10 11:04:11.765	johannorin	32A000241
1038	246	CABLE	Created cable with number: 32A000242	CREATE	2016-10-10 11:04:11.804	johannorin	32A000242
1039	247	CABLE	Created cable with number: 32A000243	CREATE	2016-10-10 11:04:11.841	johannorin	32A000243
1040	248	CABLE	Created cable with number: 32A000244	CREATE	2016-10-10 11:04:11.879	johannorin	32A000244
1041	249	CABLE	Created cable with number: 32A000245	CREATE	2016-10-10 11:04:11.916	johannorin	32A000245
1042	250	CABLE	Created cable with number: 32A000246	CREATE	2016-10-10 11:04:11.955	johannorin	32A000246
1043	251	CABLE	Created cable with number: 32A000247	CREATE	2016-10-10 11:04:11.993	johannorin	32A000247
1044	252	CABLE	Created cable with number: 32A000248	CREATE	2016-10-10 11:04:12.034	johannorin	32A000248
1045	253	CABLE	Created cable with number: 32A000249	CREATE	2016-10-10 11:04:12.081	johannorin	32A000249
1046	254	CABLE	Created cable with number: 32A000250	CREATE	2016-10-10 11:04:12.12	johannorin	32A000250
1047	255	CABLE	Created cable with number: 32A000251	CREATE	2016-10-10 11:04:12.161	johannorin	32A000251
1048	256	CABLE	Created cable with number: 32A000252	CREATE	2016-10-10 11:04:12.201	johannorin	32A000252
1049	257	CABLE	Created cable with number: 32A000253	CREATE	2016-10-10 11:04:12.253	johannorin	32A000253
1050	258	CABLE	Created cable with number: 32A000254	CREATE	2016-10-10 11:04:12.304	johannorin	32A000254
1051	259	CABLE	Created cable with number: 32A000255	CREATE	2016-10-10 11:04:12.343	johannorin	32A000255
1052	260	CABLE	Created cable with number: 32A000256	CREATE	2016-10-10 11:04:12.42	johannorin	32A000256
1053	261	CABLE	Created cable with number: 32A000257	CREATE	2016-10-10 11:04:12.465	johannorin	32A000257
1054	262	CABLE	Created cable with number: 32A000258	CREATE	2016-10-10 11:04:12.562	johannorin	32A000258
1055	263	CABLE	Created cable with number: 32A000259	CREATE	2016-10-10 11:04:12.615	johannorin	32A000259
1056	264	CABLE	Created cable with number: 32A000260	CREATE	2016-10-10 11:04:12.676	johannorin	32A000260
1057	265	CABLE	Created cable with number: 32A000261	CREATE	2016-10-10 11:04:12.723	johannorin	32A000261
1058	266	CABLE	Created cable with number: 32A000262	CREATE	2016-10-10 11:04:12.77	johannorin	32A000262
1059	267	CABLE	Created cable with number: 32A000263	CREATE	2016-10-10 11:04:12.885	johannorin	32A000263
1060	268	CABLE	Created cable with number: 32A000264	CREATE	2016-10-10 11:04:12.924	johannorin	32A000264
1061	269	CABLE	Created cable with number: 32A000265	CREATE	2016-10-10 11:04:12.989	johannorin	32A000265
1062	270	CABLE	Created cable with number: 32A000266	CREATE	2016-10-10 11:04:13.043	johannorin	32A000266
1063	271	CABLE	Created cable with number: 32A000267	CREATE	2016-10-10 11:04:13.11	johannorin	32A000267
1064	272	CABLE	Created cable with number: 32A000268	CREATE	2016-10-10 11:04:13.162	johannorin	32A000268
1065	273	CABLE	Created cable with number: 32A000269	CREATE	2016-10-10 11:04:13.223	johannorin	32A000269
1066	274	CABLE	Created cable with number: 32A000270	CREATE	2016-10-10 11:04:13.263	johannorin	32A000270
1067	275	CABLE	Created cable with number: 32A000271	CREATE	2016-10-10 11:04:13.302	johannorin	32A000271
1068	276	CABLE	Created cable with number: 32A000272	CREATE	2016-10-10 11:04:13.342	johannorin	32A000272
1069	277	CABLE	Created cable with number: 32A000273	CREATE	2016-10-10 11:04:13.382	johannorin	32A000273
1070	278	CABLE	Created cable with number: 32A000274	CREATE	2016-10-10 11:04:13.42	johannorin	32A000274
1071	279	CABLE	Created cable with number: 32A000275	CREATE	2016-10-10 11:04:13.458	johannorin	32A000275
1072	280	CABLE	Created cable with number: 32A000276	CREATE	2016-10-10 11:04:13.497	johannorin	32A000276
1073	281	CABLE	Created cable with number: 32A000277	CREATE	2016-10-10 11:04:13.535	johannorin	32A000277
1074	282	CABLE	Created cable with number: 32A000278	CREATE	2016-10-10 11:04:13.572	johannorin	32A000278
1075	283	CABLE	Created cable with number: 32A000279	CREATE	2016-10-10 11:04:13.61	johannorin	32A000279
1076	284	CABLE	Created cable with number: 32A000280	CREATE	2016-10-10 11:04:13.648	johannorin	32A000280
1077	285	CABLE	Created cable with number: 32A000281	CREATE	2016-10-10 11:04:13.691	johannorin	32A000281
1078	286	CABLE	Created cable with number: 32A000282	CREATE	2016-10-10 11:04:13.73	johannorin	32A000282
1079	287	CABLE	Created cable with number: 32A000283	CREATE	2016-10-10 11:04:13.768	johannorin	32A000283
1080	288	CABLE	Created cable with number: 32A000284	CREATE	2016-10-10 11:04:13.808	johannorin	32A000284
1081	289	CABLE	Created cable with number: 32A000285	CREATE	2016-10-10 11:04:13.854	johannorin	32A000285
1082	290	CABLE	Created cable with number: 32A000286	CREATE	2016-10-10 11:04:13.894	johannorin	32A000286
1083	291	CABLE	Created cable with number: 32A000287	CREATE	2016-10-10 11:04:13.934	johannorin	32A000287
1084	292	CABLE	Created cable with number: 32A000288	CREATE	2016-10-10 11:04:13.973	johannorin	32A000288
1085	293	CABLE	Created cable with number: 32A000289	CREATE	2016-10-10 11:04:14.012	johannorin	32A000289
1086	294	CABLE	Created cable with number: 32A000290	CREATE	2016-10-10 11:04:14.051	johannorin	32A000290
1087	295	CABLE	Created cable with number: 32A000291	CREATE	2016-10-10 11:04:14.092	johannorin	32A000291
1088	296	CABLE	Created cable with number: 32A000292	CREATE	2016-10-10 11:04:14.132	johannorin	32A000292
1089	297	CABLE	Created cable with number: 32A000293	CREATE	2016-10-10 11:04:14.172	johannorin	32A000293
1090	298	CABLE	Created cable with number: 32A000294	CREATE	2016-10-10 11:04:14.212	johannorin	32A000294
1091	299	CABLE	Created cable with number: 32A000295	CREATE	2016-10-10 11:04:14.251	johannorin	32A000295
1092	300	CABLE	Created cable with number: 32A000296	CREATE	2016-10-10 11:04:14.29	johannorin	32A000296
1093	301	CABLE	Created cable with number: 32A000297	CREATE	2016-10-10 11:04:14.33	johannorin	32A000297
1094	302	CABLE	Created cable with number: 32A000298	CREATE	2016-10-10 11:04:14.372	johannorin	32A000298
1095	303	CABLE	Created cable with number: 32A000299	CREATE	2016-10-10 11:04:14.416	johannorin	32A000299
1096	304	CABLE	Created cable with number: 32A000300	CREATE	2016-10-10 11:04:14.463	johannorin	32A000300
1097	305	CABLE	Created cable with number: 32A000301	CREATE	2016-10-10 11:04:14.539	johannorin	32A000301
1098	306	CABLE	Created cable with number: 32A000302	CREATE	2016-10-10 11:04:14.583	johannorin	32A000302
1099	307	CABLE	Created cable with number: 32A000303	CREATE	2016-10-10 11:04:14.63	johannorin	32A000303
1100	308	CABLE	Created cable with number: 32A000304	CREATE	2016-10-10 11:04:14.673	johannorin	32A000304
1101	309	CABLE	Created cable with number: 32A000305	CREATE	2016-10-10 11:04:14.719	johannorin	32A000305
1102	310	CABLE	Created cable with number: 32A000306	CREATE	2016-10-10 11:04:14.76	johannorin	32A000306
1103	311	CABLE	Created cable with number: 32A000307	CREATE	2016-10-10 11:04:14.801	johannorin	32A000307
1104	312	CABLE	Created cable with number: 32A000308	CREATE	2016-10-10 11:04:14.842	johannorin	32A000308
1105	313	CABLE	Created cable with number: 32A000309	CREATE	2016-10-10 11:04:14.888	johannorin	32A000309
1106	314	CABLE	Created cable with number: 32A000310	CREATE	2016-10-10 11:04:14.963	johannorin	32A000310
1107	315	CABLE	Created cable with number: 32A000311	CREATE	2016-10-10 11:04:15.004	johannorin	32A000311
1108	316	CABLE	Created cable with number: 32A000312	CREATE	2016-10-10 11:04:15.044	johannorin	32A000312
1109	317	CABLE	Created cable with number: 32A000313	CREATE	2016-10-10 11:04:15.084	johannorin	32A000313
1110	318	CABLE	Created cable with number: 32A000314	CREATE	2016-10-10 11:04:15.124	johannorin	32A000314
1111	319	CABLE	Created cable with number: 32A000315	CREATE	2016-10-10 11:04:15.164	johannorin	32A000315
1112	320	CABLE	Created cable with number: 32A000316	CREATE	2016-10-10 11:04:15.203	johannorin	32A000316
1113	321	CABLE	Created cable with number: 32A000317	CREATE	2016-10-10 11:04:15.244	johannorin	32A000317
1114	322	CABLE	Created cable with number: 32A000318	CREATE	2016-10-10 11:04:15.284	johannorin	32A000318
1115	323	CABLE	Created cable with number: 32A000319	CREATE	2016-10-10 11:04:15.324	johannorin	32A000319
1116	324	CABLE	Created cable with number: 32A000320	CREATE	2016-10-10 11:04:15.365	johannorin	32A000320
1117	325	CABLE	Created cable with number: 32A000321	CREATE	2016-10-10 11:04:15.405	johannorin	32A000321
1118	326	CABLE	Created cable with number: 32A000322	CREATE	2016-10-10 11:04:15.444	johannorin	32A000322
1119	327	CABLE	Created cable with number: 32A000323	CREATE	2016-10-10 11:04:15.484	johannorin	32A000323
1120	328	CABLE	Created cable with number: 32A000324	CREATE	2016-10-10 11:04:15.524	johannorin	32A000324
1121	329	CABLE	Created cable with number: 32A000325	CREATE	2016-10-10 11:04:15.565	johannorin	32A000325
1122	330	CABLE	Created cable with number: 32A000326	CREATE	2016-10-10 11:04:15.606	johannorin	32A000326
1123	331	CABLE	Created cable with number: 32A000327	CREATE	2016-10-10 11:04:15.647	johannorin	32A000327
1124	332	CABLE	Created cable with number: 32A000328	CREATE	2016-10-10 11:04:15.688	johannorin	32A000328
1125	333	CABLE	Created cable with number: 42A000329	CREATE	2016-10-10 11:04:15.731	johannorin	42A000329
1126	334	CABLE	Created cable with number: 42A000330	CREATE	2016-10-10 11:04:15.772	johannorin	42A000330
1127	335	CABLE	Created cable with number: 42A000331	CREATE	2016-10-10 11:04:15.813	johannorin	42A000331
1128	336	CABLE	Created cable with number: 42A000332	CREATE	2016-10-10 11:04:15.856	johannorin	42A000332
1129	337	CABLE	Created cable with number: 42A000333	CREATE	2016-10-10 11:04:15.898	johannorin	42A000333
1130	338	CABLE	Created cable with number: 42A000334	CREATE	2016-10-10 11:04:15.941	johannorin	42A000334
1131	339	CABLE	Created cable with number: 42A000335	CREATE	2016-10-10 11:04:15.983	johannorin	42A000335
1132	340	CABLE	Created cable with number: 42A000336	CREATE	2016-10-10 11:04:16.027	johannorin	42A000336
1133	341	CABLE	Created cable with number: 42A000337	CREATE	2016-10-10 11:04:16.119	johannorin	42A000337
1134	342	CABLE	Created cable with number: 42A000338	CREATE	2016-10-10 11:04:16.167	johannorin	42A000338
1135	343	CABLE	Created cable with number: 42A000339	CREATE	2016-10-10 11:04:16.209	johannorin	42A000339
1136	344	CABLE	Created cable with number: 42A000340	CREATE	2016-10-10 11:04:16.252	johannorin	42A000340
1137	345	CABLE	Created cable with number: 42A000341	CREATE	2016-10-10 11:04:16.318	johannorin	42A000341
1138	346	CABLE	Created cable with number: 42A000342	CREATE	2016-10-10 11:04:16.361	johannorin	42A000342
1139	347	CABLE	Created cable with number: 42A000343	CREATE	2016-10-10 11:04:16.405	johannorin	42A000343
1140	348	CABLE	Created cable with number: 42A000344	CREATE	2016-10-10 11:04:16.449	johannorin	42A000344
1141	349	CABLE	Created cable with number: 42A000345	CREATE	2016-10-10 11:04:16.536	johannorin	42A000345
1142	350	CABLE	Created cable with number: 42A000346	CREATE	2016-10-10 11:04:16.579	johannorin	42A000346
1143	351	CABLE	Created cable with number: 42A000347	CREATE	2016-10-10 11:04:16.623	johannorin	42A000347
1144	352	CABLE	Created cable with number: 42A000348	CREATE	2016-10-10 11:04:16.711	johannorin	42A000348
1145	353	CABLE	Created cable with number: 42A000349	CREATE	2016-10-10 11:04:16.778	johannorin	42A000349
1146	354	CABLE	Created cable with number: 42A000350	CREATE	2016-10-10 11:04:16.862	johannorin	42A000350
1147	355	CABLE	Created cable with number: 42A000351	CREATE	2016-10-10 11:04:16.964	johannorin	42A000351
1148	356	CABLE	Created cable with number: 42A000352	CREATE	2016-10-10 11:04:17.006	johannorin	42A000352
1149	357	CABLE	Created cable with number: 42A000353	CREATE	2016-10-10 11:04:17.05	johannorin	42A000353
1150	358	CABLE	Created cable with number: 42A000354	CREATE	2016-10-10 11:04:17.106	johannorin	42A000354
1151	359	CABLE	Created cable with number: 42A000355	CREATE	2016-10-10 11:04:17.172	johannorin	42A000355
1152	360	CABLE	Created cable with number: 42A000356	CREATE	2016-10-10 11:04:17.216	johannorin	42A000356
1153	361	CABLE	Created cable with number: 42A000357	CREATE	2016-10-10 11:04:17.26	johannorin	42A000357
1154	362	CABLE	Created cable with number: 42A000358	CREATE	2016-10-10 11:04:17.303	johannorin	42A000358
1155	363	CABLE	Created cable with number: 42A000359	CREATE	2016-10-10 11:04:17.346	johannorin	42A000359
1156	364	CABLE	Created cable with number: 42A000360	CREATE	2016-10-10 11:04:17.391	johannorin	42A000360
1157	365	CABLE	Created cable with number: 42A000361	CREATE	2016-10-10 11:04:17.434	johannorin	42A000361
1158	366	CABLE	Created cable with number: 42A000362	CREATE	2016-10-10 11:04:17.477	johannorin	42A000362
1159	367	CABLE	Created cable with number: 42A000363	CREATE	2016-10-10 11:04:17.52	johannorin	42A000363
1160	368	CABLE	Created cable with number: 42A000364	CREATE	2016-10-10 11:04:17.563	johannorin	42A000364
1161	369	CABLE	Created cable with number: 42A000365	CREATE	2016-10-10 11:04:17.606	johannorin	42A000365
1162	370	CABLE	Created cable with number: 42A000366	CREATE	2016-10-10 11:04:17.651	johannorin	42A000366
1163	371	CABLE	Created cable with number: 42A000367	CREATE	2016-10-10 11:04:17.696	johannorin	42A000367
1164	372	CABLE	Created cable with number: 42A000368	CREATE	2016-10-10 11:04:17.741	johannorin	42A000368
1165	373	CABLE	Created cable with number: 42A000369	CREATE	2016-10-10 11:04:17.785	johannorin	42A000369
1166	374	CABLE	Created cable with number: 42A000370	CREATE	2016-10-10 11:04:17.831	johannorin	42A000370
1167	375	CABLE	Created cable with number: 42A000371	CREATE	2016-10-10 11:04:17.895	johannorin	42A000371
1168	376	CABLE	Created cable with number: 42A000372	CREATE	2016-10-10 11:04:17.939	johannorin	42A000372
1169	377	CABLE	Created cable with number: 42A000373	CREATE	2016-10-10 11:04:17.983	johannorin	42A000373
1170	378	CABLE	Created cable with number: 42A000374	CREATE	2016-10-10 11:04:18.027	johannorin	42A000374
1171	379	CABLE	Created cable with number: 42A000375	CREATE	2016-10-10 11:04:18.071	johannorin	42A000375
1172	380	CABLE	Created cable with number: 42A000376	CREATE	2016-10-10 11:04:18.116	johannorin	42A000376
1173	381	CABLE	Created cable with number: 42A000377	CREATE	2016-10-10 11:04:18.16	johannorin	42A000377
1174	382	CABLE	Created cable with number: 42A000378	CREATE	2016-10-10 11:04:18.208	johannorin	42A000378
1175	383	CABLE	Created cable with number: 42A000379	CREATE	2016-10-10 11:04:18.259	johannorin	42A000379
1176	384	CABLE	Created cable with number: 42A000380	CREATE	2016-10-10 11:04:18.306	johannorin	42A000380
1177	385	CABLE	Created cable with number: 42A000381	CREATE	2016-10-10 11:04:18.359	johannorin	42A000381
1178	386	CABLE	Created cable with number: 42A000382	CREATE	2016-10-10 11:04:18.406	johannorin	42A000382
1179	387	CABLE	Created cable with number: 42A000383	CREATE	2016-10-10 11:04:18.453	johannorin	42A000383
1180	388	CABLE	Created cable with number: 42A000384	CREATE	2016-10-10 11:04:18.504	johannorin	42A000384
1181	389	CABLE	Created cable with number: 42A000385	CREATE	2016-10-10 11:04:18.55	johannorin	42A000385
1182	390	CABLE	Created cable with number: 42A000386	CREATE	2016-10-10 11:04:18.594	johannorin	42A000386
1183	391	CABLE	Created cable with number: 42A000387	CREATE	2016-10-10 11:04:18.707	johannorin	42A000387
1184	392	CABLE	Created cable with number: 42A000388	CREATE	2016-10-10 11:04:18.753	johannorin	42A000388
1185	393	CABLE	Created cable with number: 42A000389	CREATE	2016-10-10 11:04:18.809	johannorin	42A000389
1186	394	CABLE	Created cable with number: 42A000390	CREATE	2016-10-10 11:04:18.856	johannorin	42A000390
1187	395	CABLE	Created cable with number: 42A000391	CREATE	2016-10-10 11:04:18.907	johannorin	42A000391
1188	396	CABLE	Created cable with number: 42A000392	CREATE	2016-10-10 11:04:18.952	johannorin	42A000392
1189	397	CABLE	Created cable with number: 42A000393	CREATE	2016-10-10 11:04:18.998	johannorin	42A000393
1190	398	CABLE	Created cable with number: 42A000394	CREATE	2016-10-10 11:04:19.044	johannorin	42A000394
1191	399	CABLE	Created cable with number: 42A000395	CREATE	2016-10-10 11:04:19.091	johannorin	42A000395
1192	400	CABLE	Created cable with number: 42A000396	CREATE	2016-10-10 11:04:19.14	johannorin	42A000396
1193	401	CABLE	Created cable with number: 42A000397	CREATE	2016-10-10 11:04:19.187	johannorin	42A000397
1194	402	CABLE	Created cable with number: 42A000398	CREATE	2016-10-10 11:04:19.236	johannorin	42A000398
1195	403	CABLE	Created cable with number: 42A000399	CREATE	2016-10-10 11:04:19.283	johannorin	42A000399
1196	404	CABLE	Created cable with number: 42A000400	CREATE	2016-10-10 11:04:19.332	johannorin	42A000400
1197	405	CABLE	Created cable with number: 42A000401	CREATE	2016-10-10 11:04:19.381	johannorin	42A000401
1198	406	CABLE	Created cable with number: 42A000402	CREATE	2016-10-10 11:04:19.43	johannorin	42A000402
1199	407	CABLE	Created cable with number: 42A000403	CREATE	2016-10-10 11:04:19.495	johannorin	42A000403
1200	408	CABLE	Created cable with number: 42A000404	CREATE	2016-10-10 11:04:19.583	johannorin	42A000404
1201	409	CABLE	Created cable with number: 42A000405	CREATE	2016-10-10 11:04:19.636	johannorin	42A000405
1202	410	CABLE	Created cable with number: 42A000406	CREATE	2016-10-10 11:04:19.683	johannorin	42A000406
1203	411	CABLE	Created cable with number: 42A000407	CREATE	2016-10-10 11:04:19.732	johannorin	42A000407
1204	412	CABLE	Created cable with number: 42A000408	CREATE	2016-10-10 11:04:19.778	johannorin	42A000408
1205	413	CABLE	Created cable with number: 42A000409	CREATE	2016-10-10 11:04:19.825	johannorin	42A000409
1206	414	CABLE	Created cable with number: 42A000410	CREATE	2016-10-10 11:04:19.874	johannorin	42A000410
1207	415	CABLE	Created cable with number: 42A000411	CREATE	2016-10-10 11:04:19.926	johannorin	42A000411
1208	416	CABLE	Created cable with number: 42A000412	CREATE	2016-10-10 11:04:19.973	johannorin	42A000412
1209	417	CABLE	Created cable with number: 42A000413	CREATE	2016-10-10 11:04:20.022	johannorin	42A000413
1210	418	CABLE	Created cable with number: 42A000414	CREATE	2016-10-10 11:04:20.07	johannorin	42A000414
1211	419	CABLE	Created cable with number: 42A000415	CREATE	2016-10-10 11:04:20.12	johannorin	42A000415
1212	420	CABLE	Created cable with number: 42A000416	CREATE	2016-10-10 11:04:20.168	johannorin	42A000416
1213	421	CABLE	Created cable with number: 22A000417	CREATE	2016-10-10 11:04:20.216	johannorin	22A000417
1214	422	CABLE	Created cable with number: 22A000418	CREATE	2016-10-10 11:04:20.263	johannorin	22A000418
1215	423	CABLE	Created cable with number: 22A000419	CREATE	2016-10-10 11:04:20.311	johannorin	22A000419
1216	424	CABLE	Created cable with number: 22A000420	CREATE	2016-10-10 11:04:20.359	johannorin	22A000420
1217	425	CABLE	Created cable with number: 22A000421	CREATE	2016-10-10 11:04:20.406	johannorin	22A000421
1218	426	CABLE	Created cable with number: 22A000422	CREATE	2016-10-10 11:04:20.454	johannorin	22A000422
1219	427	CABLE	Created cable with number: 22A000423	CREATE	2016-10-10 11:04:20.506	johannorin	22A000423
1220	428	CABLE	Created cable with number: 22A000424	CREATE	2016-10-10 11:04:20.552	johannorin	22A000424
1221	429	CABLE	Created cable with number: 22A000425	CREATE	2016-10-10 11:04:20.599	johannorin	22A000425
1222	430	CABLE	Created cable with number: 22A000426	CREATE	2016-10-10 11:04:20.65	johannorin	22A000426
1223	431	CABLE	Created cable with number: 22A000427	CREATE	2016-10-10 11:04:20.7	johannorin	22A000427
1224	17	CABLE		UPDATE	2016-10-10 11:04:20.76	johannorin	32A000013
1225	26	CABLE		UPDATE	2016-10-10 11:04:20.823	johannorin	32A000022
1226	31	CABLE		UPDATE	2016-10-10 11:04:20.888	johannorin	32A000027
1227	432	CABLE	Created cable with number: 32A000428	CREATE	2016-10-10 11:04:20.943	johannorin	32A000428
1228	433	CABLE	Created cable with number: 32A000429	CREATE	2016-10-10 11:04:20.993	johannorin	32A000429
1229	434	CABLE	Created cable with number: 32A000430	CREATE	2016-10-10 11:04:21.044	johannorin	32A000430
1230	435	CABLE	Created cable with number: 32A000431	CREATE	2016-10-10 11:04:21.095	johannorin	32A000431
1231	436	CABLE	Created cable with number: 32A000432	CREATE	2016-10-10 11:04:21.146	johannorin	32A000432
1232	437	CABLE	Created cable with number: 32A000433	CREATE	2016-10-10 11:04:21.195	johannorin	32A000433
1233	438	CABLE	Created cable with number: 32A000434	CREATE	2016-10-10 11:04:21.266	johannorin	32A000434
1234	439	CABLE	Created cable with number: 32A000435	CREATE	2016-10-10 11:04:21.334	johannorin	32A000435
1235	440	CABLE	Created cable with number: 32A000436	CREATE	2016-10-10 11:04:21.399	johannorin	32A000436
1236	441	CABLE	Created cable with number: 32A000437	CREATE	2016-10-10 11:04:21.45	johannorin	32A000437
1237	442	CABLE	Created cable with number: 32A000438	CREATE	2016-10-10 11:04:21.502	johannorin	32A000438
1238	443	CABLE	Created cable with number: 32A000439	CREATE	2016-10-10 11:04:21.563	johannorin	32A000439
1239	444	CABLE	Created cable with number: 32A000440	CREATE	2016-10-10 11:04:21.622	johannorin	32A000440
1240	445	CABLE	Created cable with number: 32A000441	CREATE	2016-10-10 11:04:21.678	johannorin	32A000441
1241	446	CABLE	Created cable with number: 32A000442	CREATE	2016-10-10 11:04:21.729	johannorin	32A000442
1242	447	CABLE	Created cable with number: 32A000443	CREATE	2016-10-10 11:04:21.779	johannorin	32A000443
1243	448	CABLE	Created cable with number: 32A000444	CREATE	2016-10-10 11:04:21.831	johannorin	32A000444
1244	449	CABLE	Created cable with number: 32A000445	CREATE	2016-10-10 11:04:21.884	johannorin	32A000445
1245	450	CABLE	Created cable with number: 32A000446	CREATE	2016-10-10 11:04:21.95	johannorin	32A000446
1246	451	CABLE	Created cable with number: 32A000447	CREATE	2016-10-10 11:04:22.007	johannorin	32A000447
1247	452	CABLE	Created cable with number: 42A000448	CREATE	2016-10-10 11:04:22.057	johannorin	42A000448
1248	453	CABLE	Created cable with number: 42A000449	CREATE	2016-10-10 11:04:22.108	johannorin	42A000449
1249	454	CABLE	Created cable with number: 42A000450	CREATE	2016-10-10 11:04:22.161	johannorin	42A000450
1250	455	CABLE	Created cable with number: 42A000451	CREATE	2016-10-10 11:04:22.211	johannorin	42A000451
1251	456	CABLE	Created cable with number: 42A000452	CREATE	2016-10-10 11:04:22.261	johannorin	42A000452
1252	457	CABLE	Created cable with number: 42A000453	CREATE	2016-10-10 11:04:22.31	johannorin	42A000453
1253	458	CABLE	Created cable with number: 42A000454	CREATE	2016-10-10 11:04:22.361	johannorin	42A000454
1254	459	CABLE	Created cable with number: 42A000455	CREATE	2016-10-10 11:04:22.412	johannorin	42A000455
1255	460	CABLE	Created cable with number: 42A000456	CREATE	2016-10-10 11:04:22.463	johannorin	42A000456
1256	461	CABLE	Created cable with number: 42A000457	CREATE	2016-10-10 11:04:22.515	johannorin	42A000457
1257	9	CABLE		UPDATE	2016-10-10 11:13:26.917	johannorin	32A000005
1258	10	CABLE		UPDATE	2016-10-10 11:13:26.98	johannorin	32A000006
1259	11	CABLE		UPDATE	2016-10-10 11:13:27.039	johannorin	32A000007
1260	12	CABLE		UPDATE	2016-10-10 11:13:27.082	johannorin	32A000008
1261	13	CABLE		UPDATE	2016-10-10 11:13:27.134	johannorin	32A000009
1262	14	CABLE		UPDATE	2016-10-10 11:13:27.168	johannorin	32A000010
1263	15	CABLE		UPDATE	2016-10-10 11:13:27.198	johannorin	32A000011
1264	16	CABLE		UPDATE	2016-10-10 11:13:27.229	johannorin	32A000012
1265	18	CABLE		UPDATE	2016-10-10 11:13:27.261	johannorin	32A000014
1266	19	CABLE		UPDATE	2016-10-10 11:13:27.3	johannorin	32A000015
1267	20	CABLE		UPDATE	2016-10-10 11:13:27.33	johannorin	32A000016
1268	21	CABLE		UPDATE	2016-10-10 11:13:27.36	johannorin	32A000017
1269	22	CABLE		UPDATE	2016-10-10 11:13:27.402	johannorin	32A000018
1270	23	CABLE		UPDATE	2016-10-10 11:13:27.433	johannorin	32A000019
1271	24	CABLE		UPDATE	2016-10-10 11:13:27.464	johannorin	32A000020
1272	25	CABLE		UPDATE	2016-10-10 11:13:27.498	johannorin	32A000021
1273	27	CABLE		UPDATE	2016-10-10 11:13:27.53	johannorin	32A000023
1274	28	CABLE		UPDATE	2016-10-10 11:13:27.562	johannorin	32A000024
1275	29	CABLE		UPDATE	2016-10-10 11:13:27.595	johannorin	32A000025
1276	30	CABLE		UPDATE	2016-10-10 11:13:27.625	johannorin	32A000026
1277	89	CABLE	Validated cable with number: 22A000085.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:39.973	johannorin	22A000085
1278	90	CABLE	Validated cable with number: 22A000086.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:39.981	johannorin	22A000086
1279	91	CABLE	Validated cable with number: 22A000087.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:39.988	johannorin	22A000087
1280	92	CABLE	Validated cable with number: 22A000088.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:39.995	johannorin	22A000088
1281	93	CABLE	Validated cable with number: 22A000089.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.007	johannorin	22A000089
1282	94	CABLE	Validated cable with number: 22A000090.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.015	johannorin	22A000090
1283	95	CABLE	Validated cable with number: 22A000091.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.021	johannorin	22A000091
1284	96	CABLE	Validated cable with number: 22A000092.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.027	johannorin	22A000092
1285	106	CABLE	Validated cable with number: 22A000102.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.037	johannorin	22A000102
1286	107	CABLE	Validated cable with number: 22A000103.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.046	johannorin	22A000103
1287	108	CABLE	Validated cable with number: 22A000104.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.054	johannorin	22A000104
1288	109	CABLE	Validated cable with number: 22A000105.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.061	johannorin	22A000105
1289	110	CABLE	Validated cable with number: 22A000106.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.068	johannorin	22A000106
1290	111	CABLE	Validated cable with number: 22A000107.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.075	johannorin	22A000107
1291	112	CABLE	Validated cable with number: 22A000108.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.082	johannorin	22A000108
1292	113	CABLE	Validated cable with number: 22A000109.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.094	johannorin	22A000109
1293	114	CABLE	Validated cable with number: 22A000110.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.102	johannorin	22A000110
1294	115	CABLE	Validated cable with number: 22A000111.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.109	johannorin	22A000111
1295	116	CABLE	Validated cable with number: 22A000112.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.117	johannorin	22A000112
1296	117	CABLE	Validated cable with number: 22A000113.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.124	johannorin	22A000113
1297	118	CABLE	Validated cable with number: 22A000114.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.131	johannorin	22A000114
1298	119	CABLE	Validated cable with number: 22A000115.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.138	johannorin	22A000115
1299	120	CABLE	Validated cable with number: 22A000116.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.145	johannorin	22A000116
1300	121	CABLE	Validated cable with number: 22A000117.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.152	johannorin	22A000117
1301	97	CABLE	Validated cable with number: 22A000093.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.158	johannorin	22A000093
1302	98	CABLE	Validated cable with number: 22A000094.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.164	johannorin	22A000094
1303	99	CABLE	Validated cable with number: 22A000095.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.171	johannorin	22A000095
1304	100	CABLE	Validated cable with number: 22A000096.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.177	johannorin	22A000096
1305	101	CABLE	Validated cable with number: 22A000097.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.184	johannorin	22A000097
1306	102	CABLE	Validated cable with number: 22A000098.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.192	johannorin	22A000098
1307	103	CABLE	Validated cable with number: 22A000099.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.199	johannorin	22A000099
1308	104	CABLE	Validated cable with number: 22A000100.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.206	johannorin	22A000100
1309	105	CABLE	Validated cable with number: 22A000101.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.212	johannorin	22A000101
1310	122	CABLE	Validated cable with number: 22A000118.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.219	johannorin	22A000118
1311	123	CABLE	Validated cable with number: 22A000119.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.226	johannorin	22A000119
1312	124	CABLE	Validated cable with number: 22A000120.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.231	johannorin	22A000120
1313	125	CABLE	Validated cable with number: 22A000121.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.238	johannorin	22A000121
1314	126	CABLE	Validated cable with number: 22A000122.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.244	johannorin	22A000122
1315	139	CABLE	Validated cable with number: 22A000135.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.372	johannorin	22A000135
1316	140	CABLE	Validated cable with number: 22A000136.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.38	johannorin	22A000136
1317	141	CABLE	Validated cable with number: 22A000137.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.387	johannorin	22A000137
1318	142	CABLE	Validated cable with number: 22A000138.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.394	johannorin	22A000138
1319	143	CABLE	Validated cable with number: 22A000139.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.402	johannorin	22A000139
1320	144	CABLE	Validated cable with number: 22A000140.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.41	johannorin	22A000140
1321	145	CABLE	Validated cable with number: 22A000141.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.418	johannorin	22A000141
1322	146	CABLE	Validated cable with number: 22A000142.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.425	johannorin	22A000142
1323	147	CABLE	Validated cable with number: 22A000143.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.433	johannorin	22A000143
1324	148	CABLE	Validated cable with number: 22A000144.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.442	johannorin	22A000144
1325	149	CABLE	Validated cable with number: 22A000145.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.453	johannorin	22A000145
1326	150	CABLE	Validated cable with number: 22A000146.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.461	johannorin	22A000146
1327	151	CABLE	Validated cable with number: 22A000147.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.47	johannorin	22A000147
1328	152	CABLE	Validated cable with number: 22A000148.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.479	johannorin	22A000148
1329	153	CABLE	Validated cable with number: 22A000149.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.487	johannorin	22A000149
1330	154	CABLE	Validated cable with number: 22A000150.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.494	johannorin	22A000150
1331	127	CABLE	Validated cable with number: 22A000123.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.574	johannorin	22A000123
1332	128	CABLE	Validated cable with number: 22A000124.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.582	johannorin	22A000124
1333	129	CABLE	Validated cable with number: 22A000125.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.589	johannorin	22A000125
1334	130	CABLE	Validated cable with number: 22A000126.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.596	johannorin	22A000126
1335	131	CABLE	Validated cable with number: 22A000127.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.602	johannorin	22A000127
1336	132	CABLE	Validated cable with number: 22A000128.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.607	johannorin	22A000128
1337	133	CABLE	Validated cable with number: 22A000129.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.614	johannorin	22A000129
1338	134	CABLE	Validated cable with number: 22A000130.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.62	johannorin	22A000130
1339	135	CABLE	Validated cable with number: 22A000131.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.626	johannorin	22A000131
1340	136	CABLE	Validated cable with number: 22A000132.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.632	johannorin	22A000132
1341	137	CABLE	Validated cable with number: 22A000133.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.637	johannorin	22A000133
1342	138	CABLE	Validated cable with number: 22A000134.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.644	johannorin	22A000134
1343	155	CABLE	Validated cable with number: 22A000151.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.65	johannorin	22A000151
1344	156	CABLE	Validated cable with number: 22A000152.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.656	johannorin	22A000152
1345	157	CABLE	Validated cable with number: 22A000153.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.663	johannorin	22A000153
1346	158	CABLE	Validated cable with number: 22A000154.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.669	johannorin	22A000154
1347	159	CABLE	Validated cable with number: 22A000155.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.675	johannorin	22A000155
1348	160	CABLE	Validated cable with number: 22A000156.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.681	johannorin	22A000156
1349	161	CABLE	Validated cable with number: 22A000157.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.688	johannorin	22A000157
1350	162	CABLE	Validated cable with number: 22A000158.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.695	johannorin	22A000158
1351	163	CABLE	Validated cable with number: 22A000159.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.702	johannorin	22A000159
1352	164	CABLE	Validated cable with number: 22A000160.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.708	johannorin	22A000160
1353	165	CABLE	Validated cable with number: 22A000161.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.715	johannorin	22A000161
1354	166	CABLE	Validated cable with number: 22A000162.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.721	johannorin	22A000162
1355	167	CABLE	Validated cable with number: 22A000163.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.727	johannorin	22A000163
1356	168	CABLE	Validated cable with number: 22A000164.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.733	johannorin	22A000164
1357	169	CABLE	Validated cable with number: 22A000165.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.74	johannorin	22A000165
1358	170	CABLE	Validated cable with number: 22A000166.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.746	johannorin	22A000166
1359	171	CABLE	Validated cable with number: 22A000167.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.752	johannorin	22A000167
1360	172	CABLE	Validated cable with number: 22A000168.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.758	johannorin	22A000168
1361	173	CABLE	Validated cable with number: 22A000169.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.764	johannorin	22A000169
1362	174	CABLE	Validated cable with number: 22A000170.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.771	johannorin	22A000170
1363	175	CABLE	Validated cable with number: 22A000171.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.777	johannorin	22A000171
1364	176	CABLE	Validated cable with number: 22A000172.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.784	johannorin	22A000172
1365	177	CABLE	Validated cable with number: 32A000173.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.791	johannorin	32A000173
1366	178	CABLE	Validated cable with number: 32A000174.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.798	johannorin	32A000174
1367	179	CABLE	Validated cable with number: 32A000175.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.804	johannorin	32A000175
1368	180	CABLE	Validated cable with number: 32A000176.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.811	johannorin	32A000176
1369	181	CABLE	Validated cable with number: 32A000177.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.876	johannorin	32A000177
1370	182	CABLE	Validated cable with number: 32A000178.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.891	johannorin	32A000178
1371	183	CABLE	Validated cable with number: 32A000179.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.898	johannorin	32A000179
1372	184	CABLE	Validated cable with number: 32A000180.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.916	johannorin	32A000180
1373	185	CABLE	Validated cable with number: 32A000181.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.931	johannorin	32A000181
1374	186	CABLE	Validated cable with number: 32A000182.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.944	johannorin	32A000182
1375	187	CABLE	Validated cable with number: 32A000183.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.954	johannorin	32A000183
1376	188	CABLE	Validated cable with number: 32A000184.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.961	johannorin	32A000184
1377	189	CABLE	Validated cable with number: 32A000185.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.967	johannorin	32A000185
1378	190	CABLE	Validated cable with number: 32A000186.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.973	johannorin	32A000186
1379	191	CABLE	Validated cable with number: 32A000187.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.979	johannorin	32A000187
1380	192	CABLE	Validated cable with number: 32A000188.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.986	johannorin	32A000188
1381	193	CABLE	Validated cable with number: 32A000189.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:40.993	johannorin	32A000189
1382	194	CABLE	Validated cable with number: 32A000190.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41	johannorin	32A000190
1383	195	CABLE	Validated cable with number: 32A000191.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.007	johannorin	32A000191
1384	196	CABLE	Validated cable with number: 32A000192.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.014	johannorin	32A000192
1385	197	CABLE	Validated cable with number: 32A000193.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.021	johannorin	32A000193
1386	198	CABLE	Validated cable with number: 32A000194.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.027	johannorin	32A000194
1387	199	CABLE	Validated cable with number: 32A000195.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.034	johannorin	32A000195
1388	200	CABLE	Validated cable with number: 32A000196.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.04	johannorin	32A000196
1389	201	CABLE	Validated cable with number: 32A000197.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.046	johannorin	32A000197
1390	202	CABLE	Validated cable with number: 32A000198.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.053	johannorin	32A000198
1391	203	CABLE	Validated cable with number: 32A000199.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.059	johannorin	32A000199
1392	204	CABLE	Validated cable with number: 32A000200.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.065	johannorin	32A000200
1393	205	CABLE	Validated cable with number: 32A000201.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.084	johannorin	32A000201
1394	206	CABLE	Validated cable with number: 32A000202.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.101	johannorin	32A000202
1395	207	CABLE	Validated cable with number: 32A000203.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.114	johannorin	32A000203
1396	208	CABLE	Validated cable with number: 32A000204.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.127	johannorin	32A000204
1397	209	CABLE	Validated cable with number: 32A000205.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.136	johannorin	32A000205
1398	210	CABLE	Validated cable with number: 32A000206.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.143	johannorin	32A000206
1399	211	CABLE	Validated cable with number: 32A000207.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.149	johannorin	32A000207
1400	212	CABLE	Validated cable with number: 32A000208.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.155	johannorin	32A000208
1401	213	CABLE	Validated cable with number: 32A000209.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.161	johannorin	32A000209
1402	214	CABLE	Validated cable with number: 32A000210.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.167	johannorin	32A000210
1403	215	CABLE	Validated cable with number: 32A000211.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.173	johannorin	32A000211
1404	216	CABLE	Validated cable with number: 32A000212.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.179	johannorin	32A000212
1405	217	CABLE	Validated cable with number: 32A000213.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.185	johannorin	32A000213
1406	218	CABLE	Validated cable with number: 32A000214.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.191	johannorin	32A000214
1407	219	CABLE	Validated cable with number: 32A000215.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.197	johannorin	32A000215
1408	220	CABLE	Validated cable with number: 32A000216.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.203	johannorin	32A000216
1409	221	CABLE	Validated cable with number: 32A000217.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.208	johannorin	32A000217
1410	222	CABLE	Validated cable with number: 32A000218.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.215	johannorin	32A000218
1411	223	CABLE	Validated cable with number: 32A000219.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.221	johannorin	32A000219
1412	224	CABLE	Validated cable with number: 32A000220.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.227	johannorin	32A000220
1413	225	CABLE	Validated cable with number: 32A000221.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.234	johannorin	32A000221
1414	226	CABLE	Validated cable with number: 32A000222.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.24	johannorin	32A000222
1415	227	CABLE	Validated cable with number: 32A000223.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.247	johannorin	32A000223
1416	228	CABLE	Validated cable with number: 32A000224.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.254	johannorin	32A000224
1417	229	CABLE	Validated cable with number: 32A000225.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.26	johannorin	32A000225
1418	230	CABLE	Validated cable with number: 32A000226.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.265	johannorin	32A000226
1419	231	CABLE	Validated cable with number: 32A000227.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.271	johannorin	32A000227
1420	232	CABLE	Validated cable with number: 32A000228.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.278	johannorin	32A000228
1421	233	CABLE	Validated cable with number: 32A000229.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.284	johannorin	32A000229
1422	234	CABLE	Validated cable with number: 32A000230.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.29	johannorin	32A000230
1423	235	CABLE	Validated cable with number: 32A000231.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.296	johannorin	32A000231
1424	236	CABLE	Validated cable with number: 32A000232.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.303	johannorin	32A000232
1425	237	CABLE	Validated cable with number: 32A000233.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.31	johannorin	32A000233
1426	238	CABLE	Validated cable with number: 32A000234.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.317	johannorin	32A000234
1427	239	CABLE	Validated cable with number: 32A000235.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.323	johannorin	32A000235
1428	240	CABLE	Validated cable with number: 32A000236.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.33	johannorin	32A000236
1429	241	CABLE	Validated cable with number: 32A000237.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.337	johannorin	32A000237
1430	242	CABLE	Validated cable with number: 32A000238.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.345	johannorin	32A000238
1431	243	CABLE	Validated cable with number: 32A000239.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.358	johannorin	32A000239
1432	244	CABLE	Validated cable with number: 32A000240.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.37	johannorin	32A000240
1433	245	CABLE	Validated cable with number: 32A000241.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.385	johannorin	32A000241
1434	246	CABLE	Validated cable with number: 32A000242.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.392	johannorin	32A000242
1435	247	CABLE	Validated cable with number: 32A000243.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.398	johannorin	32A000243
1436	248	CABLE	Validated cable with number: 32A000244.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.404	johannorin	32A000244
1437	249	CABLE	Validated cable with number: 32A000245.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.41	johannorin	32A000245
1438	250	CABLE	Validated cable with number: 32A000246.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.416	johannorin	32A000246
1439	251	CABLE	Validated cable with number: 32A000247.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.422	johannorin	32A000247
1440	252	CABLE	Validated cable with number: 32A000248.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.428	johannorin	32A000248
1441	253	CABLE	Validated cable with number: 32A000249.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.433	johannorin	32A000249
1442	254	CABLE	Validated cable with number: 32A000250.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.439	johannorin	32A000250
1443	255	CABLE	Validated cable with number: 32A000251.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.445	johannorin	32A000251
1444	256	CABLE	Validated cable with number: 32A000252.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.452	johannorin	32A000252
1445	257	CABLE	Validated cable with number: 32A000253.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.458	johannorin	32A000253
1446	258	CABLE	Validated cable with number: 32A000254.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.464	johannorin	32A000254
1447	259	CABLE	Validated cable with number: 32A000255.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.47	johannorin	32A000255
1448	260	CABLE	Validated cable with number: 32A000256.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.476	johannorin	32A000256
1449	261	CABLE	Validated cable with number: 32A000257.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.483	johannorin	32A000257
1450	262	CABLE	Validated cable with number: 32A000258.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.488	johannorin	32A000258
1451	263	CABLE	Validated cable with number: 32A000259.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.495	johannorin	32A000259
1452	264	CABLE	Validated cable with number: 32A000260.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.502	johannorin	32A000260
1453	265	CABLE	Validated cable with number: 32A000261.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.508	johannorin	32A000261
1454	266	CABLE	Validated cable with number: 32A000262.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.519	johannorin	32A000262
1455	267	CABLE	Validated cable with number: 32A000263.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.536	johannorin	32A000263
1456	268	CABLE	Validated cable with number: 32A000264.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.549	johannorin	32A000264
1457	269	CABLE	Validated cable with number: 32A000265.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.556	johannorin	32A000265
1458	270	CABLE	Validated cable with number: 32A000266.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.562	johannorin	32A000266
1459	271	CABLE	Validated cable with number: 32A000267.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.568	johannorin	32A000267
1460	272	CABLE	Validated cable with number: 32A000268.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.575	johannorin	32A000268
1461	273	CABLE	Validated cable with number: 32A000269.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.585	johannorin	32A000269
1462	274	CABLE	Validated cable with number: 32A000270.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.6	johannorin	32A000270
1463	275	CABLE	Validated cable with number: 32A000271.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.609	johannorin	32A000271
1464	276	CABLE	Validated cable with number: 32A000272.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.619	johannorin	32A000272
1465	277	CABLE	Validated cable with number: 32A000273.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.628	johannorin	32A000273
1466	278	CABLE	Validated cable with number: 32A000274.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.636	johannorin	32A000274
1467	279	CABLE	Validated cable with number: 32A000275.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.645	johannorin	32A000275
1468	280	CABLE	Validated cable with number: 32A000276.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.664	johannorin	32A000276
1469	281	CABLE	Validated cable with number: 32A000277.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.673	johannorin	32A000277
1470	282	CABLE	Validated cable with number: 32A000278.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.682	johannorin	32A000278
1471	283	CABLE	Validated cable with number: 32A000279.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.69	johannorin	32A000279
1472	284	CABLE	Validated cable with number: 32A000280.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.696	johannorin	32A000280
1473	285	CABLE	Validated cable with number: 32A000281.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.701	johannorin	32A000281
1474	286	CABLE	Validated cable with number: 32A000282.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.707	johannorin	32A000282
1475	287	CABLE	Validated cable with number: 32A000283.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.713	johannorin	32A000283
1476	288	CABLE	Validated cable with number: 32A000284.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.719	johannorin	32A000284
1477	289	CABLE	Validated cable with number: 32A000285.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.726	johannorin	32A000285
1478	290	CABLE	Validated cable with number: 32A000286.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.732	johannorin	32A000286
1479	291	CABLE	Validated cable with number: 32A000287.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.738	johannorin	32A000287
1480	292	CABLE	Validated cable with number: 32A000288.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.745	johannorin	32A000288
1481	293	CABLE	Validated cable with number: 32A000289.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.751	johannorin	32A000289
1482	294	CABLE	Validated cable with number: 32A000290.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.758	johannorin	32A000290
1483	295	CABLE	Validated cable with number: 32A000291.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.764	johannorin	32A000291
1484	296	CABLE	Validated cable with number: 32A000292.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.77	johannorin	32A000292
1485	297	CABLE	Validated cable with number: 32A000293.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.776	johannorin	32A000293
1486	298	CABLE	Validated cable with number: 32A000294.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.783	johannorin	32A000294
1487	299	CABLE	Validated cable with number: 32A000295.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.789	johannorin	32A000295
1488	300	CABLE	Validated cable with number: 32A000296.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.795	johannorin	32A000296
1489	301	CABLE	Validated cable with number: 32A000297.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.801	johannorin	32A000297
1490	302	CABLE	Validated cable with number: 32A000298.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.808	johannorin	32A000298
1491	303	CABLE	Validated cable with number: 32A000299.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.814	johannorin	32A000299
1492	304	CABLE	Validated cable with number: 32A000300.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.821	johannorin	32A000300
1493	305	CABLE	Validated cable with number: 32A000301.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.834	johannorin	32A000301
1494	306	CABLE	Validated cable with number: 32A000302.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.842	johannorin	32A000302
1495	307	CABLE	Validated cable with number: 32A000303.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.848	johannorin	32A000303
1496	308	CABLE	Validated cable with number: 32A000304.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.854	johannorin	32A000304
1497	309	CABLE	Validated cable with number: 32A000305.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.86	johannorin	32A000305
1498	310	CABLE	Validated cable with number: 32A000306.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.866	johannorin	32A000306
1499	311	CABLE	Validated cable with number: 32A000307.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.873	johannorin	32A000307
1500	312	CABLE	Validated cable with number: 32A000308.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.88	johannorin	32A000308
1501	313	CABLE	Validated cable with number: 32A000309.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.887	johannorin	32A000309
1502	314	CABLE	Validated cable with number: 32A000310.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.893	johannorin	32A000310
1503	315	CABLE	Validated cable with number: 32A000311.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.9	johannorin	32A000311
1504	316	CABLE	Validated cable with number: 32A000312.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.906	johannorin	32A000312
1505	317	CABLE	Validated cable with number: 32A000313.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.913	johannorin	32A000313
1506	318	CABLE	Validated cable with number: 32A000314.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.92	johannorin	32A000314
1507	319	CABLE	Validated cable with number: 32A000315.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.926	johannorin	32A000315
1508	320	CABLE	Validated cable with number: 32A000316.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.932	johannorin	32A000316
1509	321	CABLE	Validated cable with number: 32A000317.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.938	johannorin	32A000317
1510	322	CABLE	Validated cable with number: 32A000318.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.944	johannorin	32A000318
1511	323	CABLE	Validated cable with number: 32A000319.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.952	johannorin	32A000319
1512	324	CABLE	Validated cable with number: 32A000320.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.958	johannorin	32A000320
1513	325	CABLE	Validated cable with number: 32A000321.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.964	johannorin	32A000321
1514	326	CABLE	Validated cable with number: 32A000322.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.971	johannorin	32A000322
1515	327	CABLE	Validated cable with number: 32A000323.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.977	johannorin	32A000323
1516	328	CABLE	Validated cable with number: 32A000324.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.984	johannorin	32A000324
1517	329	CABLE	Validated cable with number: 32A000325.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.989	johannorin	32A000325
1518	330	CABLE	Validated cable with number: 32A000326.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:41.996	johannorin	32A000326
1519	331	CABLE	Validated cable with number: 32A000327.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.003	johannorin	32A000327
1520	332	CABLE	Validated cable with number: 32A000328.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.013	johannorin	32A000328
1521	333	CABLE	Validated cable with number: 42A000329.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.02	johannorin	42A000329
1522	334	CABLE	Validated cable with number: 42A000330.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.027	johannorin	42A000330
1523	335	CABLE	Validated cable with number: 42A000331.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.033	johannorin	42A000331
1524	336	CABLE	Validated cable with number: 42A000332.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.039	johannorin	42A000332
1525	337	CABLE	Validated cable with number: 42A000333.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.046	johannorin	42A000333
1526	338	CABLE	Validated cable with number: 42A000334.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.054	johannorin	42A000334
1527	339	CABLE	Validated cable with number: 42A000335.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.064	johannorin	42A000335
1528	340	CABLE	Validated cable with number: 42A000336.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.07	johannorin	42A000336
1529	341	CABLE	Validated cable with number: 42A000337.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.077	johannorin	42A000337
1530	342	CABLE	Validated cable with number: 42A000338.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.085	johannorin	42A000338
1531	343	CABLE	Validated cable with number: 42A000339.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.092	johannorin	42A000339
1532	344	CABLE	Validated cable with number: 42A000340.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.098	johannorin	42A000340
1533	345	CABLE	Validated cable with number: 42A000341.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.104	johannorin	42A000341
1534	346	CABLE	Validated cable with number: 42A000342.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.11	johannorin	42A000342
1535	347	CABLE	Validated cable with number: 42A000343.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.116	johannorin	42A000343
1536	348	CABLE	Validated cable with number: 42A000344.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.122	johannorin	42A000344
1537	349	CABLE	Validated cable with number: 42A000345.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.128	johannorin	42A000345
1538	350	CABLE	Validated cable with number: 42A000346.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.134	johannorin	42A000346
1539	351	CABLE	Validated cable with number: 42A000347.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.139	johannorin	42A000347
1540	352	CABLE	Validated cable with number: 42A000348.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.154	johannorin	42A000348
1541	353	CABLE	Validated cable with number: 42A000349.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.161	johannorin	42A000349
1542	354	CABLE	Validated cable with number: 42A000350.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.167	johannorin	42A000350
1543	355	CABLE	Validated cable with number: 42A000351.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.173	johannorin	42A000351
1544	356	CABLE	Validated cable with number: 42A000352.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.179	johannorin	42A000352
1545	357	CABLE	Validated cable with number: 42A000353.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.185	johannorin	42A000353
1546	358	CABLE	Validated cable with number: 42A000354.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.191	johannorin	42A000354
1547	359	CABLE	Validated cable with number: 42A000355.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.197	johannorin	42A000355
1548	360	CABLE	Validated cable with number: 42A000356.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.203	johannorin	42A000356
1549	361	CABLE	Validated cable with number: 42A000357.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.209	johannorin	42A000357
1550	362	CABLE	Validated cable with number: 42A000358.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.215	johannorin	42A000358
1551	363	CABLE	Validated cable with number: 42A000359.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.221	johannorin	42A000359
1552	364	CABLE	Validated cable with number: 42A000360.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.227	johannorin	42A000360
1553	365	CABLE	Validated cable with number: 42A000361.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.233	johannorin	42A000361
1554	366	CABLE	Validated cable with number: 42A000362.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.238	johannorin	42A000362
1555	367	CABLE	Validated cable with number: 42A000363.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.244	johannorin	42A000363
1556	368	CABLE	Validated cable with number: 42A000364.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.25	johannorin	42A000364
1557	369	CABLE	Validated cable with number: 42A000365.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.257	johannorin	42A000365
1558	370	CABLE	Validated cable with number: 42A000366.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.263	johannorin	42A000366
1559	371	CABLE	Validated cable with number: 42A000367.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.27	johannorin	42A000367
1560	372	CABLE	Validated cable with number: 42A000368.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.277	johannorin	42A000368
1561	373	CABLE	Validated cable with number: 42A000369.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.284	johannorin	42A000369
1562	374	CABLE	Validated cable with number: 42A000370.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.29	johannorin	42A000370
1563	375	CABLE	Validated cable with number: 42A000371.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.296	johannorin	42A000371
1564	376	CABLE	Validated cable with number: 42A000372.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.304	johannorin	42A000372
1565	377	CABLE	Validated cable with number: 42A000373.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.311	johannorin	42A000373
1566	378	CABLE	Validated cable with number: 42A000374.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.318	johannorin	42A000374
1567	379	CABLE	Validated cable with number: 42A000375.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.325	johannorin	42A000375
1568	380	CABLE	Validated cable with number: 42A000376.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.332	johannorin	42A000376
1569	381	CABLE	Validated cable with number: 42A000377.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.338	johannorin	42A000377
1570	382	CABLE	Validated cable with number: 42A000378.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.344	johannorin	42A000378
1571	383	CABLE	Validated cable with number: 42A000379.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.353	johannorin	42A000379
1572	384	CABLE	Validated cable with number: 42A000380.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.359	johannorin	42A000380
1573	385	CABLE	Validated cable with number: 42A000381.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.365	johannorin	42A000381
1574	386	CABLE	Validated cable with number: 42A000382.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.372	johannorin	42A000382
1575	387	CABLE	Validated cable with number: 42A000383.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.378	johannorin	42A000383
1576	388	CABLE	Validated cable with number: 42A000384.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.384	johannorin	42A000384
1577	389	CABLE	Validated cable with number: 42A000385.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.391	johannorin	42A000385
1578	390	CABLE	Validated cable with number: 42A000386.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.397	johannorin	42A000386
1579	391	CABLE	Validated cable with number: 42A000387.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.404	johannorin	42A000387
1580	392	CABLE	Validated cable with number: 42A000388.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.411	johannorin	42A000388
1581	393	CABLE	Validated cable with number: 42A000389.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.418	johannorin	42A000389
1582	394	CABLE	Validated cable with number: 42A000390.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.423	johannorin	42A000390
1583	395	CABLE	Validated cable with number: 42A000391.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.429	johannorin	42A000391
1584	396	CABLE	Validated cable with number: 42A000392.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.435	johannorin	42A000392
1585	397	CABLE	Validated cable with number: 42A000393.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.44	johannorin	42A000393
1586	398	CABLE	Validated cable with number: 42A000394.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.447	johannorin	42A000394
1587	399	CABLE	Validated cable with number: 42A000395.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.453	johannorin	42A000395
1588	400	CABLE	Validated cable with number: 42A000396.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.459	johannorin	42A000396
1589	401	CABLE	Validated cable with number: 42A000397.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.465	johannorin	42A000397
1590	402	CABLE	Validated cable with number: 42A000398.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.471	johannorin	42A000398
1591	403	CABLE	Validated cable with number: 42A000399.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.476	johannorin	42A000399
1592	404	CABLE	Validated cable with number: 42A000400.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.482	johannorin	42A000400
1593	405	CABLE	Validated cable with number: 42A000401.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.488	johannorin	42A000401
1594	406	CABLE	Validated cable with number: 42A000402.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.494	johannorin	42A000402
1595	407	CABLE	Validated cable with number: 42A000403.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.5	johannorin	42A000403
1596	408	CABLE	Validated cable with number: 42A000404.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.506	johannorin	42A000404
1597	409	CABLE	Validated cable with number: 42A000405.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.512	johannorin	42A000405
1598	410	CABLE	Validated cable with number: 42A000406.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.518	johannorin	42A000406
1599	411	CABLE	Validated cable with number: 42A000407.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.524	johannorin	42A000407
1600	412	CABLE	Validated cable with number: 42A000408.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.531	johannorin	42A000408
1601	413	CABLE	Validated cable with number: 42A000409.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.538	johannorin	42A000409
1602	414	CABLE	Validated cable with number: 42A000410.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.545	johannorin	42A000410
1603	415	CABLE	Validated cable with number: 42A000411.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.552	johannorin	42A000411
1604	416	CABLE	Validated cable with number: 42A000412.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.559	johannorin	42A000412
1605	417	CABLE	Validated cable with number: 42A000413.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.567	johannorin	42A000413
1606	418	CABLE	Validated cable with number: 42A000414.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.573	johannorin	42A000414
1607	419	CABLE	Validated cable with number: 42A000415.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.58	johannorin	42A000415
1608	420	CABLE	Validated cable with number: 42A000416.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.588	johannorin	42A000416
1609	421	CABLE	Validated cable with number: 22A000417.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.595	johannorin	22A000417
1610	422	CABLE	Validated cable with number: 22A000418.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.602	johannorin	22A000418
1611	423	CABLE	Validated cable with number: 22A000419.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.608	johannorin	22A000419
1612	424	CABLE	Validated cable with number: 22A000420.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.614	johannorin	22A000420
1613	425	CABLE	Validated cable with number: 22A000421.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.62	johannorin	22A000421
1614	426	CABLE	Validated cable with number: 22A000422.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.625	johannorin	22A000422
1615	427	CABLE	Validated cable with number: 22A000423.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.632	johannorin	22A000423
1616	428	CABLE	Validated cable with number: 22A000424.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.639	johannorin	22A000424
1617	429	CABLE	Validated cable with number: 22A000425.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.645	johannorin	22A000425
1618	430	CABLE	Validated cable with number: 22A000426.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.652	johannorin	22A000426
1619	431	CABLE	Validated cable with number: 22A000427.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.664	johannorin	22A000427
1620	432	CABLE	Validated cable with number: 32A000428.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.678	johannorin	32A000428
1621	433	CABLE	Validated cable with number: 32A000429.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.694	johannorin	32A000429
1622	434	CABLE	Validated cable with number: 32A000430.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.701	johannorin	32A000430
1623	435	CABLE	Validated cable with number: 32A000431.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.707	johannorin	32A000431
1624	436	CABLE	Validated cable with number: 32A000432.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.714	johannorin	32A000432
1625	437	CABLE	Validated cable with number: 32A000433.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.721	johannorin	32A000433
1626	438	CABLE	Validated cable with number: 32A000434.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.726	johannorin	32A000434
1627	439	CABLE	Validated cable with number: 32A000435.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.733	johannorin	32A000435
1628	440	CABLE	Validated cable with number: 32A000436.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.739	johannorin	32A000436
1629	441	CABLE	Validated cable with number: 32A000437.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.745	johannorin	32A000437
1630	442	CABLE	Validated cable with number: 32A000438.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.752	johannorin	32A000438
1631	443	CABLE	Validated cable with number: 32A000439.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.757	johannorin	32A000439
1632	444	CABLE	Validated cable with number: 32A000440.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.763	johannorin	32A000440
1633	445	CABLE	Validated cable with number: 32A000441.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.77	johannorin	32A000441
1634	446	CABLE	Validated cable with number: 32A000442.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.78	johannorin	32A000442
1635	447	CABLE	Validated cable with number: 32A000443.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.787	johannorin	32A000443
1636	448	CABLE	Validated cable with number: 32A000444.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.794	johannorin	32A000444
1637	449	CABLE	Validated cable with number: 32A000445.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.801	johannorin	32A000445
1638	450	CABLE	Validated cable with number: 32A000446.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.808	johannorin	32A000446
1639	451	CABLE	Validated cable with number: 32A000447.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.815	johannorin	32A000447
1640	452	CABLE	Validated cable with number: 42A000448.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.822	johannorin	42A000448
1641	453	CABLE	Validated cable with number: 42A000449.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.828	johannorin	42A000449
1642	454	CABLE	Validated cable with number: 42A000450.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.835	johannorin	42A000450
1643	455	CABLE	Validated cable with number: 42A000451.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.844	johannorin	42A000451
1644	456	CABLE	Validated cable with number: 42A000452.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.853	johannorin	42A000452
1645	457	CABLE	Validated cable with number: 42A000453.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.86	johannorin	42A000453
1646	458	CABLE	Validated cable with number: 42A000454.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.866	johannorin	42A000454
1647	459	CABLE	Validated cable with number: 42A000455.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.872	johannorin	42A000455
1648	460	CABLE	Validated cable with number: 42A000456.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.879	johannorin	42A000456
1649	461	CABLE	Validated cable with number: 42A000457.\nCable name is: DANGLING (was VALID)\nCable endpoint A name is: VALID (was VALID)\nCable endpoint B name is: VALID (was VALID)\n	VALIDATE	2016-10-10 13:40:42.885	johannorin	42A000457
1650	462	CABLE	Created cable with number: 42A000458	CREATE	2016-10-10 15:15:29.242	johannorin	42A000458
1651	463	CABLE	Created cable with number: 42A000459	CREATE	2016-10-10 15:15:29.249	johannorin	42A000459
1652	464	CABLE	Created cable with number: 42A000460	CREATE	2016-10-10 15:15:29.255	johannorin	42A000460
1653	465	CABLE	Created cable with number: 42A000461	CREATE	2016-10-10 15:15:29.262	johannorin	42A000461
1654	466	CABLE	Created cable with number: 42A000462	CREATE	2016-10-10 15:15:29.268	johannorin	42A000462
1655	467	CABLE	Created cable with number: 42A000463	CREATE	2016-10-10 15:15:29.275	johannorin	42A000463
1656	468	CABLE	Created cable with number: 42A000464	CREATE	2016-10-10 15:15:29.283	johannorin	42A000464
1657	469	CABLE	Created cable with number: 42A000465	CREATE	2016-10-10 15:15:29.29	johannorin	42A000465
1658	470	CABLE	Created cable with number: 42A000466	CREATE	2016-10-10 15:15:29.297	johannorin	42A000466
1659	471	CABLE	Created cable with number: 42A000467	CREATE	2016-10-10 15:15:29.304	johannorin	42A000467
1660	472	CABLE	Created cable with number: 42A000468	CREATE	2016-10-10 15:15:29.318	johannorin	42A000468
1661	473	CABLE	Created cable with number: 42A000469	CREATE	2016-10-10 15:15:29.326	johannorin	42A000469
1662	474	CABLE	Created cable with number: 42A000470	CREATE	2016-10-10 15:15:29.334	johannorin	42A000470
1663	475	CABLE	Created cable with number: 42A000471	CREATE	2016-10-10 15:15:29.341	johannorin	42A000471
1664	476	CABLE	Created cable with number: 42A000472	CREATE	2016-10-10 15:15:29.349	johannorin	42A000472
1665	477	CABLE	Created cable with number: 42A000473	CREATE	2016-10-10 15:15:29.361	johannorin	42A000473
1666	478	CABLE	Created cable with number: 42A000474	CREATE	2016-10-10 15:15:29.373	johannorin	42A000474
1667	479	CABLE	Created cable with number: 42A000475	CREATE	2016-10-10 15:15:29.383	johannorin	42A000475
1668	480	CABLE	Created cable with number: 42A000476	CREATE	2016-10-10 15:15:29.393	johannorin	42A000476
1669	481	CABLE	Created cable with number: 42A000477	CREATE	2016-10-10 15:15:29.403	johannorin	42A000477
1670	482	CABLE	Created cable with number: 42A000478	CREATE	2016-10-10 15:15:29.412	johannorin	42A000478
1671	483	CABLE	Created cable with number: 42A000479	CREATE	2016-10-10 15:15:29.42	johannorin	42A000479
1672	484	CABLE	Created cable with number: 42A000480	CREATE	2016-10-10 15:15:29.429	johannorin	42A000480
1673	485	CABLE	Created cable with number: 42A000481	CREATE	2016-10-10 15:15:29.437	johannorin	42A000481
1674	486	CABLE	Created cable with number: 42A000482	CREATE	2016-10-10 15:15:29.447	johannorin	42A000482
1675	487	CABLE	Created cable with number: 42A000483	CREATE	2016-10-10 15:15:29.455	johannorin	42A000483
1676	488	CABLE	Created cable with number: 42A000484	CREATE	2016-10-10 15:15:29.465	johannorin	42A000484
1677	489	CABLE	Created cable with number: 42A000485	CREATE	2016-10-10 15:15:29.477	johannorin	42A000485
1678	490	CABLE	Created cable with number: 42A000486	CREATE	2016-10-10 15:15:29.489	johannorin	42A000486
1679	491	CABLE	Created cable with number: 42A000487	CREATE	2016-10-10 15:15:29.499	johannorin	42A000487
1680	492	CABLE	Created cable with number: 42A000488	CREATE	2016-10-10 15:15:29.507	johannorin	42A000488
1681	493	CABLE	Created cable with number: 42A000489	CREATE	2016-10-10 15:15:29.517	johannorin	42A000489
1682	494	CABLE	Created cable with number: 42A000490	CREATE	2016-10-10 15:15:29.526	johannorin	42A000490
1683	495	CABLE	Created cable with number: 42A000491	CREATE	2016-10-10 15:15:29.535	johannorin	42A000491
1684	496	CABLE	Created cable with number: 42A000492	CREATE	2016-10-10 15:15:29.543	johannorin	42A000492
1685	497	CABLE	Created cable with number: 42A000493	CREATE	2016-10-10 15:15:29.552	johannorin	42A000493
1686	498	CABLE	Created cable with number: 42A000494	CREATE	2016-10-10 15:15:29.56	johannorin	42A000494
1687	499	CABLE	Created cable with number: 42A000495	CREATE	2016-10-10 15:15:29.569	johannorin	42A000495
1688	500	CABLE	Created cable with number: 42A000496	CREATE	2016-10-10 15:15:29.578	johannorin	42A000496
1689	501	CABLE	Created cable with number: 42A000497	CREATE	2016-10-10 15:15:29.587	johannorin	42A000497
1690	502	CABLE	Created cable with number: 42A000498	CREATE	2016-10-10 15:15:29.596	johannorin	42A000498
1691	503	CABLE	Created cable with number: 42A000499	CREATE	2016-10-10 15:15:29.605	johannorin	42A000499
1692	504	CABLE	Created cable with number: 42A000500	CREATE	2016-10-10 15:15:29.613	johannorin	42A000500
1693	505	CABLE	Created cable with number: 42A000501	CREATE	2016-10-10 15:15:29.622	johannorin	42A000501
1694	506	CABLE	Created cable with number: 42A000502	CREATE	2016-10-10 15:15:29.632	johannorin	42A000502
1695	507	CABLE	Created cable with number: 42A000503	CREATE	2016-10-10 15:15:29.641	johannorin	42A000503
1696	508	CABLE	Created cable with number: 42A000504	CREATE	2016-10-10 15:15:29.65	johannorin	42A000504
1697	509	CABLE	Created cable with number: 42A000505	CREATE	2016-10-10 15:15:29.66	johannorin	42A000505
1698	510	CABLE	Created cable with number: 42A000506	CREATE	2016-10-10 15:15:29.669	johannorin	42A000506
\.


--
-- Name: history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('history_id_seq', 1698, true);


--
-- Data for Name: manufacturer; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY manufacturer (id, address, country, created, email, modified, name, phonenumber, status) FROM stdin;
\.


--
-- Name: manufacturer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('manufacturer_id_seq', 1, false);


--
-- Data for Name: query; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY query (id, created, description, executed, owner, entitytype) FROM stdin;
9	2016-05-12 09:31:10.96	test	\N	evangeliavaena	CABLE_TYPE
15	2016-09-12 07:41:52.442	Test	\N	sunilsah	CABLE
16	2016-09-12 12:11:42.485	test	\N	ricardofernandes	CABLE
17	2016-09-14 09:50:36.787	test	\N	ricardofernandes	CABLE_TYPE
18	2016-09-20 12:55:34.893	new query	\N	evangeliavaena	CABLE
\.


--
-- Name: query_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('query_id_seq', 19, true);


--
-- Data for Name: query_querycondition; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY query_querycondition (query_id, conditions_id) FROM stdin;
15	22
16	23
17	24
18	25
9	15
\.


--
-- Data for Name: querycondition; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY querycondition (id, booleanoperator, comparisonoperator, field, parenthesisclose, parenthesisopen, "position", value, query) FROM stdin;
15	NONE	EQUAL	name	NONE	NONE	0		\N
22	NONE	EQUAL	Type	NONE	NONE	0		\N
23	NONE	EQUAL	Type	NONE	NONE	0		\N
24	NONE	EQUAL	Name	NONE	NONE	0		\N
25	NONE	EQUAL	Type	NONE	NONE	0		\N
\.


--
-- Name: querycondition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cabledb_dev
--

SELECT pg_catalog.setval('querycondition_id_seq', 26, true);


--
-- Data for Name: schema_version; Type: TABLE DATA; Schema: public; Owner: cabledb_dev
--

COPY schema_version (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	1	<< Flyway Baseline >>	BASELINE	<< Flyway Baseline >>	\N	cabledb_dev	2016-09-09 07:42:07.189542	0	t
2	2	Manufacturer details	SQL	V2__Manufacturer_details.sql	-751662879	cabledb_dev	2016-09-09 12:46:41.610395	697	t
3	3	Conatainer	SQL	V3__Conatainer.sql	980218601	cabledb_dev	2016-09-09 12:46:42.869383	240	t
4	4	CableTypeArtifact	SQL	V4__CableTypeArtifact.sql	570312134	cabledb_dev	2016-09-09 12:46:43.667002	825	t
5	5	Connector details	SQL	V5__Connector_details.sql	-1219987428	cabledb_dev	2016-09-09 12:46:45.045172	744	t
\.


--
-- Name: artifact_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY artifact
    ADD CONSTRAINT artifact_pkey PRIMARY KEY (id);


--
-- Name: cable_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY cable
    ADD CONSTRAINT cable_pkey PRIMARY KEY (id);


--
-- Name: cabletype_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY cabletype
    ADD CONSTRAINT cabletype_pkey PRIMARY KEY (id);


--
-- Name: connector_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY connector
    ADD CONSTRAINT connector_pkey PRIMARY KEY (id);


--
-- Name: displayview_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY displayview
    ADD CONSTRAINT displayview_pkey PRIMARY KEY (id);


--
-- Name: displayviewcolumn_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY displayviewcolumn
    ADD CONSTRAINT displayviewcolumn_pkey PRIMARY KEY (id);


--
-- Name: endpoint_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY endpoint
    ADD CONSTRAINT endpoint_pkey PRIMARY KEY (id);


--
-- Name: history_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY history
    ADD CONSTRAINT history_pkey PRIMARY KEY (id);


--
-- Name: manufacturer_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY manufacturer
    ADD CONSTRAINT manufacturer_pkey PRIMARY KEY (id);


--
-- Name: query_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY query
    ADD CONSTRAINT query_pkey PRIMARY KEY (id);


--
-- Name: querycondition_pkey; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY querycondition
    ADD CONSTRAINT querycondition_pkey PRIMARY KEY (id);


--
-- Name: schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank);


--
-- Name: uk_fxfvd2s6mbhp1gav7l22bsroh; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY displayview_displayviewcolumn
    ADD CONSTRAINT uk_fxfvd2s6mbhp1gav7l22bsroh UNIQUE (columns_id);


--
-- Name: uk_lay8xo3i37yd6rph7c2kbw3kd; Type: CONSTRAINT; Schema: public; Owner: cabledb_dev; Tablespace: 
--

ALTER TABLE ONLY query_querycondition
    ADD CONSTRAINT uk_lay8xo3i37yd6rph7c2kbw3kd UNIQUE (conditions_id);


--
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: cabledb_dev; Tablespace: 
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- Name: fk_4e5wsfbmdm8hdaifhtrucon45; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY cable
    ADD CONSTRAINT fk_4e5wsfbmdm8hdaifhtrucon45 FOREIGN KEY (endpointb_id) REFERENCES endpoint(id);


--
-- Name: fk_4od3w30oade3x21p9emvxfbwa; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY cable
    ADD CONSTRAINT fk_4od3w30oade3x21p9emvxfbwa FOREIGN KEY (endpointa_id) REFERENCES endpoint(id);


--
-- Name: fk_51txnixcivulqx0q1rbdfqboo; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY cable
    ADD CONSTRAINT fk_51txnixcivulqx0q1rbdfqboo FOREIGN KEY (cabletype_id) REFERENCES cabletype(id);


--
-- Name: fk_column_displayview; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY displayviewcolumn
    ADD CONSTRAINT fk_column_displayview FOREIGN KEY (displayview) REFERENCES displayview(id);


--
-- Name: fk_condition_query; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY querycondition
    ADD CONSTRAINT fk_condition_query FOREIGN KEY (query) REFERENCES query(id);


--
-- Name: fk_dyqpqgm1v70d7nq077b3enlf3; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY query_querycondition
    ADD CONSTRAINT fk_dyqpqgm1v70d7nq077b3enlf3 FOREIGN KEY (query_id) REFERENCES query(id);


--
-- Name: fk_e5t5yfslr9ur0lgtgqbmln45e; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY cabletype
    ADD CONSTRAINT fk_e5t5yfslr9ur0lgtgqbmln45e FOREIGN KEY (datasheet_id) REFERENCES artifact(id);


--
-- Name: fk_fxfvd2s6mbhp1gav7l22bsroh; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY displayview_displayviewcolumn
    ADD CONSTRAINT fk_fxfvd2s6mbhp1gav7l22bsroh FOREIGN KEY (columns_id) REFERENCES displayviewcolumn(id);


--
-- Name: fk_g246e21ge4icztmsug63rv35z; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY endpoint
    ADD CONSTRAINT fk_g246e21ge4icztmsug63rv35z FOREIGN KEY (connector_id) REFERENCES connector(id);


--
-- Name: fk_kaka4eyd48g1yu2kry8km5acq; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY displayview_displayviewcolumn
    ADD CONSTRAINT fk_kaka4eyd48g1yu2kry8km5acq FOREIGN KEY (displayview_id) REFERENCES displayview(id);


--
-- Name: fk_lay8xo3i37yd6rph7c2kbw3kd; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY query_querycondition
    ADD CONSTRAINT fk_lay8xo3i37yd6rph7c2kbw3kd FOREIGN KEY (conditions_id) REFERENCES querycondition(id);


--
-- Name: fk_r079j78rx6cvmklhmt31jt75i; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY cabletype
    ADD CONSTRAINT fk_r079j78rx6cvmklhmt31jt75i FOREIGN KEY (manufacturer_id) REFERENCES manufacturer(id);


--
-- Name: fk_z2t1gfetj3tg6rtsgsharb00d; Type: FK CONSTRAINT; Schema: public; Owner: cabledb_dev
--

ALTER TABLE ONLY cable
    ADD CONSTRAINT fk_z2t1gfetj3tg6rtsgsharb00d FOREIGN KEY (qualityreport_id) REFERENCES artifact(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

